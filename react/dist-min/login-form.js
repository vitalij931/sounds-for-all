/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 52);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = React;

/***/ }),

/***/ 52:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(53);


/***/ }),

/***/ 53:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.LoginForm = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LoginForm = exports.LoginForm = function (_React$Component) {
	_inherits(LoginForm, _React$Component);

	function LoginForm(props) {
		_classCallCheck(this, LoginForm);

		var _this = _possibleConstructorReturn(this, (LoginForm.__proto__ || Object.getPrototypeOf(LoginForm)).call(this, props));

		_this.state = {
			error_message: window.login_error_message,
			is_loading: false,
			is_login: true
		};

		_this.onPageChange = _this.onPageChange.bind(_this);
		_this.onRegisterClick = _this.onRegisterClick.bind(_this);
		return _this;
	}

	_createClass(LoginForm, [{
		key: 'onRegisterClick',
		value: function onRegisterClick() {
			this.setState({
				is_login: false,
				password: '',
				error_message: null
			});
		}
	}, {
		key: 'onPageChange',
		value: function onPageChange() {
			this.setState({
				is_login: !this.state.is_login
			});
		}
	}, {
		key: 'render',
		value: function render() {
			if (this.state.is_login) {
				return _react2.default.createElement(LoginContent, { onPageChange: this.onPageChange });
			} else {
				return _react2.default.createElement(RegisterContent, { onPageChange: this.onPageChange });
			}
		}
	}]);

	return LoginForm;
}(_react2.default.Component);

var LoginContent = function (_React$Component2) {
	_inherits(LoginContent, _React$Component2);

	function LoginContent(props) {
		_classCallCheck(this, LoginContent);

		var _this2 = _possibleConstructorReturn(this, (LoginContent.__proto__ || Object.getPrototypeOf(LoginContent)).call(this, props));

		_this2.state = {
			password: '',
			username: '',
			remember_me: true
		};

		_this2.submit_request = null;

		_this2.onSubmit = _this2.onSubmit.bind(_this2);
		_this2.handleKeyPress = _this2.handleKeyPress.bind(_this2);
		_this2.onPasswordChange = _this2.onPasswordChange.bind(_this2);
		_this2.onUsernameChange = _this2.onUsernameChange.bind(_this2);
		_this2.onRememberMeChange = _this2.onRememberMeChange.bind(_this2);
		return _this2;
	}

	_createClass(LoginContent, [{
		key: 'onSubmit',
		value: function onSubmit() {
			var _this3 = this;

			if (this.submit_request) {
				return;
			}

			var data = {
				username: this.state.username,
				password: this.state.password,
				ajax: 1
			};

			this.submit_request = $.post('/account/submit', data, function (response) {
				if (response.success) {
					console.log(response);
					console.log('Done!');
					location.href = '/account';
				} else {
					alertify.error(response.message);
				}
			}, 'json').always(function () {
				_this3.submit_request = null;
				_this3.forceUpdate();
			});

			this.forceUpdate();
		}
	}, {
		key: 'handleKeyPress',
		value: function handleKeyPress(e) {
			if (e.key === 'Enter') {
				this.onSubmit();
			}
		}
	}, {
		key: 'onPasswordChange',
		value: function onPasswordChange(e) {
			this.setState({
				password: e.target.value
			});
		}
	}, {
		key: 'onUsernameChange',
		value: function onUsernameChange(e) {
			this.setState({
				username: e.target.value
			});
		}
	}, {
		key: 'onRememberMeChange',
		value: function onRememberMeChange(e) {
			this.setState({
				remember_me: e.target.checked
			});
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			if (this.submit_request) {
				this.submit_request.abort();
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var _React$createElement;

			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(
					'a',
					{ href: '/' },
					_react2.default.createElement('img', { src: '/img/logo.png', className: 'login-logo img-responsive center-block', 'data-no-retina': '' })
				),
				_react2.default.createElement(
					'div',
					{ className: 'padding-top form-small' },
					_react2.default.createElement(
						'div',
						{ className: 'login-error-message' },
						this.state.error_message
					),
					_react2.default.createElement('input', (_React$createElement = { className: 'full-width input-sm', value: this.state.username, onChange: this.onUsernameChange, name: 'username', type: 'text' }, _defineProperty(_React$createElement, 'name', 'login'), _defineProperty(_React$createElement, 'placeholder', 'Email/Username'), _defineProperty(_React$createElement, 'id', ''), _defineProperty(_React$createElement, 'autoFocus', ''), _React$createElement)),
					_react2.default.createElement('br', null),
					_react2.default.createElement('input', { className: 'full-width input-sm', value: this.state.password, onKeyPress: this.handleKeyPress, onChange: this.onPasswordChange, style: { marginTop: "10px" }, type: 'password', placeholder: 'Password', name: 'password' }),
					_react2.default.createElement(
						'div',
						{ className: 'checkbox', id: 'login_forgotten' },
						_react2.default.createElement(
							'label',
							{ htmlFor: 'remember_me' },
							_react2.default.createElement('input', { onChange: this.onRememberMeChange, name: 'remember_me', value: 'remember_me', type: 'checkbox', checked: this.state.remember_me }),
							_react2.default.createElement(
								'label',
								{ htmlFor: 'remember_me' },
								'Stay logged in'
							)
						)
					),
					_react2.default.createElement(
						'button',
						{ onClick: this.onSubmit, disabled: !!this.submit_request, className: 'btn-primary full-width' },
						!!this.submit_request ? "Logining in..." : "Login"
					),
					_react2.default.createElement('hr', { className: 'hr-half' })
				),
				_react2.default.createElement(
					'p',
					null,
					'Don\'t have an account? ',
					_react2.default.createElement(
						'a',
						{ href: 'javascript:;', onClick: this.props.onPageChange },
						'Register'
					)
				),
				_react2.default.createElement(
					'a',
					{ 'data-target': '#forgotPasswordEmailModal', 'data-toggle': 'modal' },
					'Forgotten your password?'
				),
				_react2.default.createElement('br', null),
				_react2.default.createElement(
					'a',
					{ 'data-target': '#rev', 'data-toggle': 'modal' },
					'Resend email verification'
				)
			);
		}
	}]);

	return LoginContent;
}(_react2.default.Component);

var RegisterContent = function (_React$Component3) {
	_inherits(RegisterContent, _React$Component3);

	function RegisterContent(props) {
		_classCallCheck(this, RegisterContent);

		var _this4 = _possibleConstructorReturn(this, (RegisterContent.__proto__ || Object.getPrototypeOf(RegisterContent)).call(this, props));

		_this4.state = {
			email: '',
			password: '',
			username: '',
			remember_me: true
		};

		_this4.submit_request = null;

		_this4.onSubmit = _this4.onSubmit.bind(_this4);
		_this4.onEmailChange = _this4.onEmailChange.bind(_this4);
		_this4.onPasswordChange = _this4.onPasswordChange.bind(_this4);
		_this4.onUsernameChange = _this4.onUsernameChange.bind(_this4);
		return _this4;
	}

	_createClass(RegisterContent, [{
		key: 'onSubmit',
		value: function onSubmit() {
			var _this5 = this;

			if (this.submit_request) {
				return;
			}

			var data = {
				username: this.state.username,
				password: this.state.password,
				email: this.state.email,
				ajax: 1
			};

			this.submit_request = $.post('/account/register', data, function (response) {
				if (response.success) {
					console.log(response);
					window.alertify.success('Registration successful!');
					_this5.props.onPageChange && _this5.props.onPageChange();
				} else {
					window.alertify.error(response.message);
				}
			}, 'json').always(function () {
				_this5.submit_request = null;
				_this5.forceUpdate();
			});
			this.forceUpdate();
		}
	}, {
		key: 'onPasswordChange',
		value: function onPasswordChange(e) {
			this.setState({
				password: e.target.value
			});
		}
	}, {
		key: 'onUsernameChange',
		value: function onUsernameChange(e) {
			this.setState({
				username: e.target.value
			});
		}
	}, {
		key: 'onEmailChange',
		value: function onEmailChange(e) {
			this.setState({
				email: e.target.value
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _React$createElement2, _React$createElement3;

			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(
					'a',
					{ href: '/' },
					_react2.default.createElement('img', { src: '/img/logo.png', className: 'login-logo img-responsive center-block', 'data-no-retina': '' })
				),
				_react2.default.createElement(
					'div',
					{ className: 'padding-top form-small' },
					_react2.default.createElement(
						'div',
						{ className: 'login-error-message' },
						this.state.error_message
					),
					_react2.default.createElement('input', (_React$createElement2 = { className: 'full-width input-sm', value: this.state.username, onChange: this.onUsernameChange, name: 'username', type: 'text' }, _defineProperty(_React$createElement2, 'name', 'login'), _defineProperty(_React$createElement2, 'placeholder', 'Username'), _defineProperty(_React$createElement2, 'autoFocus', true), _React$createElement2)),
					_react2.default.createElement('br', null),
					_react2.default.createElement('input', (_React$createElement3 = { className: 'full-width input-sm', value: this.state.email, onChange: this.onEmailChange, style: { marginTop: "10px" }, name: 'email', type: 'text' }, _defineProperty(_React$createElement3, 'name', 'login'), _defineProperty(_React$createElement3, 'placeholder', 'Email'), _React$createElement3)),
					_react2.default.createElement('br', null),
					_react2.default.createElement('input', { className: 'full-width input-sm', value: this.state.password, onChange: this.onPasswordChange, style: { marginTop: "10px" }, type: 'password', placeholder: 'Password', name: 'password' }),
					_react2.default.createElement(
						'button',
						{ onClick: this.onSubmit, disabled: !!this.submit_request, style: { marginTop: "10px" }, className: 'btn-primary full-width' },
						!!this.submit_request ? "Registering..." : "Register"
					),
					_react2.default.createElement('hr', { className: 'hr-half' })
				),
				_react2.default.createElement(
					'p',
					null,
					'Already have an account? ',
					_react2.default.createElement(
						'a',
						{ href: 'javascript:;', onClick: this.props.onPageChange },
						'Login'
					)
				),
				_react2.default.createElement(
					'a',
					{ 'data-target': '#rev', 'data-toggle': 'modal' },
					'Resend email verification'
				)
			);
		}
	}]);

	return RegisterContent;
}(_react2.default.Component);

ReactDOM.render(_react2.default.createElement(LoginForm, null), document.getElementById('react-login'));

/***/ })

/******/ });