/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 50);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports) {

module.exports = React;

/***/ }),

/***/ 16:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.IconAudioPlayer = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var safety_styles = _react2.default.createElement(
	'style',
	null,
	'\n\t.safety-bar {\n\t\tanimation: sound 0ms -800ms linear infinite alternate;\n\t}\n'
);

var IconAudioPlayer = exports.IconAudioPlayer = function (_React$Component) {
	_inherits(IconAudioPlayer, _React$Component);

	function IconAudioPlayer(props) {
		_classCallCheck(this, IconAudioPlayer);

		var _this = _possibleConstructorReturn(this, (IconAudioPlayer.__proto__ || Object.getPrototypeOf(IconAudioPlayer)).call(this, props));

		_this.play = _this.play.bind(_this);
		_this.onClick = _this.onClick.bind(_this);
		_this.renderFrame = _this.renderFrame.bind(_this);
		_this.initializeAudio = _this.initializeAudio.bind(_this);

		_this.state = {
			use_safety_styles: !window.AudioContext || /Mobi/.test(navigator.userAgent),
			is_buffering: false
		};
		return _this;
	}

	_createClass(IconAudioPlayer, [{
		key: 'initializeAudio',
		value: function initializeAudio(source) {
			var _this2 = this;

			if (!source) source = this.props.source;

			this.audio = (typeof source === 'undefined' ? 'undefined' : _typeof(source)) === 'object' ? source : new Audio(source);
			this.audio.onended = function (e) {
				_this2.forceUpdate();
				_this2.props.onEnded && _this2.props.onEnded(e, _this2);
			};

			this.props.durationchange && this.audio.addEventListener('durationchange', this.props.durationchange);
			this.props.progress && this.audio.addEventListener('progress', this.props.progress);
			this.props.timeupdate && this.audio.addEventListener('timeupdate', this.props.timeupdate);

			if (!this.props.ignore_analyser) {
				try {
					var ctx = new AudioContext();
					this.audioSrc = ctx.createMediaElementSource(this.audio);
					this.analyser = ctx.createAnalyser();
					// we have to connect the MediaElementSource with the analyser
					this.audioSrc.connect(this.analyser);
					this.audioSrc.connect(ctx.destination);
					// we could configure the analyser: e.g. analyser.fftSize (for further infos read the spec)

					// frequencyBinCount tells you how many values you'll receive from the analyser
					this.frequencyData = new Uint8Array(this.analyser.frequencyBinCount);
				} catch (ignored) {}
			}
		}
	}, {
		key: 'play',
		value: function play() {
			this.audio && this.audio.play() && this.forceUpdate();
		}
	}, {
		key: 'componentWillReceiveProps',
		value: function componentWillReceiveProps(nextProps) {
			if (nextProps.source && nextProps.source != this.props.source) {
				var was_paused = this.audio.paused;
				this.audio && this.audio.pause();
				this.initializeAudio(nextProps.source);

				if (!was_paused) {
					this.audio.play();
				}
			}
		}
	}, {
		key: 'renderFrame',
		value: function renderFrame() {
			if (this.state.use_safety_styles) {
				return;
			}

			var length = 400;
			var barCount = 10;
			var medium = Math.floor(length / barCount);

			if (!this.audio || this.audio.paused) {
				for (var i = 0; i < barCount; i++) {
					var el = this.frequencyData[i * medium + 50];
					var dom = $('#bars .bar').get(i);

					$(dom).css({ height: '1vh', opacity: 1 });
				}
				return;
			}

			this.analyser.getByteFrequencyData(this.frequencyData);

			for (var i = 0; i < barCount; i++) {
				var el = this.frequencyData[i * medium + 50];
				var dom = $('#bars .bar').get(i);
				var floatVal = el / 255;

				$(dom).css({ height: floatVal * 30 + 'vh', opacity: floatVal * 0.5 + 0.5 });
			}

			requestAnimationFrame(this.renderFrame);
		}
	}, {
		key: 'onClick',
		value: function onClick(e) {
			if (!this.audio) {
				this.initializeAudio(this.props.source);
			}

			if (this.audio.paused) {
				this.audio.play();

				if (!this.props.ignore_analyser) requestAnimationFrame(this.renderFrame);
			} else {
				this.audio.pause();
			}

			this.forceUpdate();
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			if (this.audio) {
				this.props.durationchange && this.audio.removeEventListener('durationchange', this.props.durationchange);
				this.props.progress && this.audio.removeEventListener('progress', this.props.progress);
				this.props.timeupdate && this.audio.removeEventListener('timeupdate', this.props.timeupdate);
				this.audio.src = '';
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var is_paused = this.audio ? this.audio.paused : true;

			return _react2.default.createElement(
				'div',
				{ className: 'icon-audio-player', style: this.props.style },
				this.state.use_safety_styles && !is_paused ? safety_styles : null,
				_react2.default.createElement('i', { onClick: this.onClick, className: 'fa ' + (is_paused ? 'fa-play' : 'fa-pause'), 'aria-hidden': 'true' })
			);
		}
	}]);

	return IconAudioPlayer;
}(_react2.default.Component);

/***/ }),

/***/ 50:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(51);


/***/ }),

/***/ 51:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _iconAudioPlayer = __webpack_require__(16);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DefaultIconAudioPlayer = function (_React$Component) {
	_inherits(DefaultIconAudioPlayer, _React$Component);

	function DefaultIconAudioPlayer(props) {
		_classCallCheck(this, DefaultIconAudioPlayer);

		var _this = _possibleConstructorReturn(this, (DefaultIconAudioPlayer.__proto__ || Object.getPrototypeOf(DefaultIconAudioPlayer)).call(this, props));

		_this.onPlay = _this.onPlay.bind(_this);
		_this.onEnded = _this.onEnded.bind(_this);
		_this.onForward = _this.onForward.bind(_this);
		_this.onBackward = _this.onBackward.bind(_this);

		_this.state = {
			currentIndex: 0
		};
		return _this;
	}

	_createClass(DefaultIconAudioPlayer, [{
		key: 'onPlay',
		value: function onPlay() {}
	}, {
		key: 'onEnded',
		value: function onEnded(e, ref) {
			this.setState({
				currentIndex: (this.state.currentIndex + 1) % 4
			}, function () {
				ref.play();
			});
		}
	}, {
		key: 'onForward',
		value: function onForward() {
			this.setState({
				currentIndex: (this.state.currentIndex + 1) % 4
			});
		}
	}, {
		key: 'onBackward',
		value: function onBackward() {
			this.setState({
				currentIndex: (this.state.currentIndex == 0 ? 3 : this.state.currentIndex - 1) % 4
			});
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(
					'div',
					{ className: 'default-icon-audio-player' },
					_react2.default.createElement('i', { onClick: this.onBackward, className: 'fa fa-backward', 'aria-hidden': 'true' }),
					_react2.default.createElement(_iconAudioPlayer.IconAudioPlayer, { onEnded: this.onEnded, source: 'assets/2-' + (this.state.currentIndex + 1) + '.mp3' }),
					_react2.default.createElement('i', { onClick: this.onForward, className: 'fa fa-forward', 'aria-hidden': 'true' })
				)
			);
		}
	}]);

	return DefaultIconAudioPlayer;
}(_react2.default.Component);

ReactDOM.render(_react2.default.createElement(DefaultIconAudioPlayer, null), document.getElementById('react-music'));

/***/ })

/******/ });