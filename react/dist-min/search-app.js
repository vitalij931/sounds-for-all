/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 54);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = React;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global = module.exports = typeof window != 'undefined' && window.Math == Math ? window : typeof self != 'undefined' && self.Math == Math ? self
// eslint-disable-next-line no-new-func
: Function('return this')();
if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var core = module.exports = { version: '2.5.1' };
if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var hasOwnProperty = {}.hasOwnProperty;
module.exports = function (it, key) {
  return hasOwnProperty.call(it, key);
};

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var anObject = __webpack_require__(10);
var IE8_DOM_DEFINE = __webpack_require__(37);
var toPrimitive = __webpack_require__(20);
var dP = Object.defineProperty;

exports.f = __webpack_require__(5) ? Object.defineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (IE8_DOM_DEFINE) try {
    return dP(O, P, Attributes);
  } catch (e) {/* empty */}
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// Thank's IE8 for his funny defineProperty
module.exports = !__webpack_require__(12)(function () {
  return Object.defineProperty({}, 'a', { get: function get() {
      return 7;
    } }).a != 7;
});

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var global = __webpack_require__(1);
var core = __webpack_require__(2);
var ctx = __webpack_require__(36);
var hide = __webpack_require__(7);
var PROTOTYPE = 'prototype';

var $export = function $export(type, name, source) {
  var IS_FORCED = type & $export.F;
  var IS_GLOBAL = type & $export.G;
  var IS_STATIC = type & $export.S;
  var IS_PROTO = type & $export.P;
  var IS_BIND = type & $export.B;
  var IS_WRAP = type & $export.W;
  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});
  var expProto = exports[PROTOTYPE];
  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];
  var key, own, out;
  if (IS_GLOBAL) source = name;
  for (key in source) {
    // contains in native
    own = !IS_FORCED && target && target[key] !== undefined;
    if (own && key in exports) continue;
    // export native or passed
    out = own ? target[key] : source[key];
    // prevent global pollution for namespaces
    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]
    // bind timers to global for call from export context
    : IS_BIND && own ? ctx(out, global)
    // wrap global constructors for prevent change them in library
    : IS_WRAP && target[key] == out ? function (C) {
      var F = function F(a, b, c) {
        if (this instanceof C) {
          switch (arguments.length) {
            case 0:
              return new C();
            case 1:
              return new C(a);
            case 2:
              return new C(a, b);
          }return new C(a, b, c);
        }return C.apply(this, arguments);
      };
      F[PROTOTYPE] = C[PROTOTYPE];
      return F;
      // make static versions for prototype methods
    }(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;
    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%
    if (IS_PROTO) {
      (exports.virtual || (exports.virtual = {}))[key] = out;
      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%
      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);
    }
  }
};
// type bitmap
$export.F = 1; // forced
$export.G = 2; // global
$export.S = 4; // static
$export.P = 8; // proto
$export.B = 16; // bind
$export.W = 32; // wrap
$export.U = 64; // safe
$export.R = 128; // real proto method for `library`
module.exports = $export;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var dP = __webpack_require__(4);
var createDesc = __webpack_require__(15);
module.exports = __webpack_require__(5) ? function (object, key, value) {
  return dP.f(object, key, createDesc(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// to indexed object, toObject with fallback for non-array-like ES3 strings
var IObject = __webpack_require__(75);
var defined = __webpack_require__(17);
module.exports = function (it) {
  return IObject(defined(it));
};

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var store = __webpack_require__(19)('wks');
var uid = __webpack_require__(14);
var _Symbol = __webpack_require__(1).Symbol;
var USE_SYMBOL = typeof _Symbol == 'function';

var $exports = module.exports = function (name) {
  return store[name] || (store[name] = USE_SYMBOL && _Symbol[name] || (USE_SYMBOL ? _Symbol : uid)('Symbol.' + name));
};

$exports.store = store;

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isObject = __webpack_require__(11);
module.exports = function (it) {
  if (!isObject(it)) throw TypeError(it + ' is not an object!');
  return it;
};

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

module.exports = function (it) {
  return (typeof it === 'undefined' ? 'undefined' : _typeof(it)) === 'object' ? it !== null : typeof it === 'function';
};

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (exec) {
  try {
    return !!exec();
  } catch (e) {
    return true;
  }
};

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout() {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
})();
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch (e) {
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch (e) {
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }
}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e) {
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e) {
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }
}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while (len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) {
    return [];
};

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () {
    return '/';
};
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function () {
    return 0;
};

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var id = 0;
var px = Math.random();
module.exports = function (key) {
  return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
};

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.IconAudioPlayer = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var safety_styles = _react2.default.createElement(
	'style',
	null,
	'\n\t.safety-bar {\n\t\tanimation: sound 0ms -800ms linear infinite alternate;\n\t}\n'
);

var IconAudioPlayer = exports.IconAudioPlayer = function (_React$Component) {
	_inherits(IconAudioPlayer, _React$Component);

	function IconAudioPlayer(props) {
		_classCallCheck(this, IconAudioPlayer);

		var _this = _possibleConstructorReturn(this, (IconAudioPlayer.__proto__ || Object.getPrototypeOf(IconAudioPlayer)).call(this, props));

		_this.play = _this.play.bind(_this);
		_this.onClick = _this.onClick.bind(_this);
		_this.renderFrame = _this.renderFrame.bind(_this);
		_this.initializeAudio = _this.initializeAudio.bind(_this);

		_this.state = {
			use_safety_styles: !window.AudioContext || /Mobi/.test(navigator.userAgent),
			is_buffering: false
		};
		return _this;
	}

	_createClass(IconAudioPlayer, [{
		key: 'initializeAudio',
		value: function initializeAudio(source) {
			var _this2 = this;

			if (!source) source = this.props.source;

			this.audio = (typeof source === 'undefined' ? 'undefined' : _typeof(source)) === 'object' ? source : new Audio(source);
			this.audio.onended = function (e) {
				_this2.forceUpdate();
				_this2.props.onEnded && _this2.props.onEnded(e, _this2);
			};

			this.props.durationchange && this.audio.addEventListener('durationchange', this.props.durationchange);
			this.props.progress && this.audio.addEventListener('progress', this.props.progress);
			this.props.timeupdate && this.audio.addEventListener('timeupdate', this.props.timeupdate);

			if (!this.props.ignore_analyser) {
				try {
					var ctx = new AudioContext();
					this.audioSrc = ctx.createMediaElementSource(this.audio);
					this.analyser = ctx.createAnalyser();
					// we have to connect the MediaElementSource with the analyser
					this.audioSrc.connect(this.analyser);
					this.audioSrc.connect(ctx.destination);
					// we could configure the analyser: e.g. analyser.fftSize (for further infos read the spec)

					// frequencyBinCount tells you how many values you'll receive from the analyser
					this.frequencyData = new Uint8Array(this.analyser.frequencyBinCount);
				} catch (ignored) {}
			}
		}
	}, {
		key: 'play',
		value: function play() {
			this.audio && this.audio.play() && this.forceUpdate();
		}
	}, {
		key: 'componentWillReceiveProps',
		value: function componentWillReceiveProps(nextProps) {
			if (nextProps.source && nextProps.source != this.props.source) {
				var was_paused = this.audio.paused;
				this.audio && this.audio.pause();
				this.initializeAudio(nextProps.source);

				if (!was_paused) {
					this.audio.play();
				}
			}
		}
	}, {
		key: 'renderFrame',
		value: function renderFrame() {
			if (this.state.use_safety_styles) {
				return;
			}

			var length = 400;
			var barCount = 10;
			var medium = Math.floor(length / barCount);

			if (!this.audio || this.audio.paused) {
				for (var i = 0; i < barCount; i++) {
					var el = this.frequencyData[i * medium + 50];
					var dom = $('#bars .bar').get(i);

					$(dom).css({ height: '1vh', opacity: 1 });
				}
				return;
			}

			this.analyser.getByteFrequencyData(this.frequencyData);

			for (var i = 0; i < barCount; i++) {
				var el = this.frequencyData[i * medium + 50];
				var dom = $('#bars .bar').get(i);
				var floatVal = el / 255;

				$(dom).css({ height: floatVal * 30 + 'vh', opacity: floatVal * 0.5 + 0.5 });
			}

			requestAnimationFrame(this.renderFrame);
		}
	}, {
		key: 'onClick',
		value: function onClick(e) {
			if (!this.audio) {
				this.initializeAudio(this.props.source);
			}

			if (this.audio.paused) {
				this.audio.play();

				if (!this.props.ignore_analyser) requestAnimationFrame(this.renderFrame);
			} else {
				this.audio.pause();
			}

			this.forceUpdate();
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			if (this.audio) {
				this.props.durationchange && this.audio.removeEventListener('durationchange', this.props.durationchange);
				this.props.progress && this.audio.removeEventListener('progress', this.props.progress);
				this.props.timeupdate && this.audio.removeEventListener('timeupdate', this.props.timeupdate);
				this.audio.src = '';
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var is_paused = this.audio ? this.audio.paused : true;

			return _react2.default.createElement(
				'div',
				{ className: 'icon-audio-player', style: this.props.style },
				this.state.use_safety_styles && !is_paused ? safety_styles : null,
				_react2.default.createElement('i', { onClick: this.onClick, className: 'fa ' + (is_paused ? 'fa-play' : 'fa-pause'), 'aria-hidden': 'true' })
			);
		}
	}]);

	return IconAudioPlayer;
}(_react2.default.Component);

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// 7.2.1 RequireObjectCoercible(argument)
module.exports = function (it) {
  if (it == undefined) throw TypeError("Can't call method on  " + it);
  return it;
};

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var shared = __webpack_require__(19)('keys');
var uid = __webpack_require__(14);
module.exports = function (key) {
  return shared[key] || (shared[key] = uid(key));
};

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var global = __webpack_require__(1);
var SHARED = '__core-js_shared__';
var store = global[SHARED] || (global[SHARED] = {});
module.exports = function (key) {
  return store[key] || (store[key] = {});
};

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// 7.1.1 ToPrimitive(input [, PreferredType])
var isObject = __webpack_require__(11);
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
module.exports = function (it, S) {
  if (!isObject(it)) return it;
  var fn, val;
  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;
  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;
  throw TypeError("Can't convert object to primitive value");
};

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// 7.1.4 ToInteger
var ceil = Math.ceil;
var floor = Math.floor;
module.exports = function (it) {
  return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
};

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = true;

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = {};

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
var anObject = __webpack_require__(10);
var dPs = __webpack_require__(74);
var enumBugKeys = __webpack_require__(26);
var IE_PROTO = __webpack_require__(18)('IE_PROTO');
var Empty = function Empty() {/* empty */};
var PROTOTYPE = 'prototype';

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var _createDict = function createDict() {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = __webpack_require__(38)('iframe');
  var i = enumBugKeys.length;
  var lt = '<';
  var gt = '>';
  var iframeDocument;
  iframe.style.display = 'none';
  __webpack_require__(79).appendChild(iframe);
  iframe.src = 'javascript:'; // eslint-disable-line no-script-url
  // createDict = iframe.contentWindow.Object;
  // html.removeChild(iframe);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
  iframeDocument.close();
  _createDict = iframeDocument.F;
  while (i--) {
    delete _createDict[PROTOTYPE][enumBugKeys[i]];
  }return _createDict();
};

module.exports = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    Empty[PROTOTYPE] = anObject(O);
    result = new Empty();
    Empty[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO] = O;
  } else result = _createDict();
  return Properties === undefined ? result : dPs(result, Properties);
};

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// 19.1.2.14 / 15.2.3.14 Object.keys(O)
var $keys = __webpack_require__(42);
var enumBugKeys = __webpack_require__(26);

module.exports = Object.keys || function keys(O) {
  return $keys(O, enumBugKeys);
};

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// IE 8- don't enum bug keys
module.exports = 'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'.split(',');

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var def = __webpack_require__(4).f;
var has = __webpack_require__(3);
var TAG = __webpack_require__(9)('toStringTag');

module.exports = function (it, tag, stat) {
  if (it && !has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
};

/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.f = __webpack_require__(9);

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var global = __webpack_require__(1);
var core = __webpack_require__(2);
var LIBRARY = __webpack_require__(22);
var wksExt = __webpack_require__(28);
var defineProperty = __webpack_require__(4).f;
module.exports = function (name) {
  var $Symbol = core.Symbol || (core.Symbol = LIBRARY ? {} : global.Symbol || {});
  if (name.charAt(0) != '_' && !(name in $Symbol)) defineProperty($Symbol, name, { value: wksExt.f(name) });
};

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.f = {}.propertyIsEnumerable;

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * 
 */

function makeEmptyFunction(arg) {
  return function () {
    return arg;
  };
}

/**
 * This function accepts and discards inputs; it has no side effects. This is
 * primarily useful idiomatically for overridable function endpoints which
 * always need to be callable, since JS lacks a null-call idiom ala Cocoa.
 */
var emptyFunction = function emptyFunction() {};

emptyFunction.thatReturns = makeEmptyFunction;
emptyFunction.thatReturnsFalse = makeEmptyFunction(false);
emptyFunction.thatReturnsTrue = makeEmptyFunction(true);
emptyFunction.thatReturnsNull = makeEmptyFunction(null);
emptyFunction.thatReturnsThis = function () {
  return this;
};
emptyFunction.thatReturnsArgument = function (arg) {
  return arg;
};

module.exports = emptyFunction;

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */



/**
 * Use invariant() to assert state which your program assumes to be true.
 *
 * Provide sprintf-style format (only %s is supported) and arguments
 * to provide information about what broke and what you were
 * expecting.
 *
 * The invariant message will be stripped in production, but the invariant
 * will remain to ensure logic does not differ in production.
 */

var validateFormat = function validateFormat(format) {};

if (process.env.NODE_ENV !== 'production') {
  validateFormat = function validateFormat(format) {
    if (format === undefined) {
      throw new Error('invariant requires an error message argument');
    }
  };
}

function invariant(condition, format, a, b, c, d, e, f) {
  validateFormat(format);

  if (!condition) {
    var error;
    if (format === undefined) {
      error = new Error('Minified exception occurred; use the non-minified dev environment ' + 'for the full error message and additional helpful warnings.');
    } else {
      var args = [a, b, c, d, e, f];
      var argIndex = 0;
      error = new Error(format.replace(/%s/g, function () {
        return args[argIndex++];
      }));
      error.name = 'Invariant Violation';
    }

    error.framesToPop = 1; // we don't care about invariant's own frame
    throw error;
  }
}

module.exports = invariant;
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(13)))

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var ReactPropTypesSecret = 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED';

module.exports = ReactPropTypesSecret;

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// 7.1.13 ToObject(argument)
var defined = __webpack_require__(17);
module.exports = function (it) {
  return Object(defined(it));
};

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)
var has = __webpack_require__(3);
var toObject = __webpack_require__(34);
var IE_PROTO = __webpack_require__(18)('IE_PROTO');
var ObjectProto = Object.prototype;

module.exports = Object.getPrototypeOf || function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  }return O instanceof Object ? ObjectProto : null;
};

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// optional / simple context binding
var aFunction = __webpack_require__(62);
module.exports = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 1:
      return function (a) {
        return fn.call(that, a);
      };
    case 2:
      return function (a, b) {
        return fn.call(that, a, b);
      };
    case 3:
      return function (a, b, c) {
        return fn.call(that, a, b, c);
      };
  }
  return function () /* ...args */{
    return fn.apply(that, arguments);
  };
};

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = !__webpack_require__(5) && !__webpack_require__(12)(function () {
  return Object.defineProperty(__webpack_require__(38)('div'), 'a', { get: function get() {
      return 7;
    } }).a != 7;
});

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isObject = __webpack_require__(11);
var document = __webpack_require__(1).document;
// typeof document.createElement is 'object' in old IE
var is = isObject(document) && isObject(document.createElement);
module.exports = function (it) {
  return is ? document.createElement(it) : {};
};

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports.__esModule = true;

var _iterator = __webpack_require__(69);

var _iterator2 = _interopRequireDefault(_iterator);

var _symbol = __webpack_require__(84);

var _symbol2 = _interopRequireDefault(_symbol);

var _typeof = typeof _symbol2.default === "function" && _typeof2(_iterator2.default) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
} : function (obj) {
  return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
};

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

exports.default = typeof _symbol2.default === "function" && _typeof(_iterator2.default) === "symbol" ? function (obj) {
  return typeof obj === "undefined" ? "undefined" : _typeof(obj);
} : function (obj) {
  return obj && typeof _symbol2.default === "function" && obj.constructor === _symbol2.default && obj !== _symbol2.default.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof(obj);
};

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var LIBRARY = __webpack_require__(22);
var $export = __webpack_require__(6);
var redefine = __webpack_require__(41);
var hide = __webpack_require__(7);
var has = __webpack_require__(3);
var Iterators = __webpack_require__(23);
var $iterCreate = __webpack_require__(73);
var setToStringTag = __webpack_require__(27);
var getPrototypeOf = __webpack_require__(35);
var ITERATOR = __webpack_require__(9)('iterator');
var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
var FF_ITERATOR = '@@iterator';
var KEYS = 'keys';
var VALUES = 'values';

var returnThis = function returnThis() {
  return this;
};

module.exports = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
  $iterCreate(Constructor, NAME, next);
  var getMethod = function getMethod(kind) {
    if (!BUGGY && kind in proto) return proto[kind];
    switch (kind) {
      case KEYS:
        return function keys() {
          return new Constructor(this, kind);
        };
      case VALUES:
        return function values() {
          return new Constructor(this, kind);
        };
    }return function entries() {
      return new Constructor(this, kind);
    };
  };
  var TAG = NAME + ' Iterator';
  var DEF_VALUES = DEFAULT == VALUES;
  var VALUES_BUG = false;
  var proto = Base.prototype;
  var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
  var $default = $native || getMethod(DEFAULT);
  var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
  var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
  var methods, key, IteratorPrototype;
  // Fix native
  if ($anyNative) {
    IteratorPrototype = getPrototypeOf($anyNative.call(new Base()));
    if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(IteratorPrototype, TAG, true);
      // fix for some old engines
      if (!LIBRARY && !has(IteratorPrototype, ITERATOR)) hide(IteratorPrototype, ITERATOR, returnThis);
    }
  }
  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEF_VALUES && $native && $native.name !== VALUES) {
    VALUES_BUG = true;
    $default = function values() {
      return $native.call(this);
    };
  }
  // Define iterator
  if ((!LIBRARY || FORCED) && (BUGGY || VALUES_BUG || !proto[ITERATOR])) {
    hide(proto, ITERATOR, $default);
  }
  // Plug for library
  Iterators[NAME] = $default;
  Iterators[TAG] = returnThis;
  if (DEFAULT) {
    methods = {
      values: DEF_VALUES ? $default : getMethod(VALUES),
      keys: IS_SET ? $default : getMethod(KEYS),
      entries: $entries
    };
    if (FORCED) for (key in methods) {
      if (!(key in proto)) redefine(proto, key, methods[key]);
    } else $export($export.P + $export.F * (BUGGY || VALUES_BUG), NAME, methods);
  }
  return methods;
};

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = __webpack_require__(7);

/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var has = __webpack_require__(3);
var toIObject = __webpack_require__(8);
var arrayIndexOf = __webpack_require__(76)(false);
var IE_PROTO = __webpack_require__(18)('IE_PROTO');

module.exports = function (object, names) {
  var O = toIObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) {
    if (key != IE_PROTO) has(O, key) && result.push(key);
  } // Don't enum bug & hidden keys
  while (names.length > i) {
    if (has(O, key = names[i++])) {
      ~arrayIndexOf(result, key) || result.push(key);
    }
  }return result;
};

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var toString = {}.toString;

module.exports = function (it) {
  return toString.call(it).slice(8, -1);
};

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.f = Object.getOwnPropertySymbols;

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// 19.1.2.7 / 15.2.3.4 Object.getOwnPropertyNames(O)
var $keys = __webpack_require__(42);
var hiddenKeys = __webpack_require__(26).concat('length', 'prototype');

exports.f = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return $keys(O, hiddenKeys);
};

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var pIE = __webpack_require__(30);
var createDesc = __webpack_require__(15);
var toIObject = __webpack_require__(8);
var toPrimitive = __webpack_require__(20);
var has = __webpack_require__(3);
var IE8_DOM_DEFINE = __webpack_require__(37);
var gOPD = Object.getOwnPropertyDescriptor;

exports.f = __webpack_require__(5) ? gOPD : function getOwnPropertyDescriptor(O, P) {
  O = toIObject(O);
  P = toPrimitive(P, true);
  if (IE8_DOM_DEFINE) try {
    return gOPD(O, P);
  } catch (e) {/* empty */}
  if (has(O, P)) return createDesc(!pIE.f.call(O, P), O[P]);
};

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */



var emptyFunction = __webpack_require__(31);

/**
 * Similar to invariant but only logs a warning if the condition is not met.
 * This can be used to log issues in development environments in critical
 * paths. Removing the logging code for production environments will keep the
 * same logic and follow the same code paths.
 */

var warning = emptyFunction;

if (process.env.NODE_ENV !== 'production') {
  var printWarning = function printWarning(format) {
    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    var argIndex = 0;
    var message = 'Warning: ' + format.replace(/%s/g, function () {
      return args[argIndex++];
    });
    if (typeof console !== 'undefined') {
      console.error(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };

  warning = function warning(condition, format) {
    if (format === undefined) {
      throw new Error('`warning(condition, format, ...args)` requires a warning ' + 'message argument');
    }

    if (format.indexOf('Failed Composite propType: ') === 0) {
      return; // Ignore CompositeComponent proptype check.
    }

    if (!condition) {
      for (var _len2 = arguments.length, args = Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
        args[_key2 - 2] = arguments[_key2];
      }

      printWarning.apply(undefined, [format].concat(args));
    }
  };
}

module.exports = warning;
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(13)))

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*! Kefir.js v3.8.0
 *  https://github.com/rpominov/kefir
 */

(function (global, factory) {
  ( false ? 'undefined' : _typeof(exports)) === 'object' && typeof module !== 'undefined' ? factory(exports) :  true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [exports], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : factory(global.Kefir = global.Kefir || {});
})(undefined, function (exports) {
  'use strict';

  function createObj(proto) {
    var F = function F() {};
    F.prototype = proto;
    return new F();
  }

  function extend(target /*, mixin1, mixin2...*/) {
    var length = arguments.length,
        i = void 0,
        prop = void 0;
    for (i = 1; i < length; i++) {
      for (prop in arguments[i]) {
        target[prop] = arguments[i][prop];
      }
    }
    return target;
  }

  function inherit(Child, Parent /*, mixin1, mixin2...*/) {
    var length = arguments.length,
        i = void 0;
    Child.prototype = createObj(Parent.prototype);
    Child.prototype.constructor = Child;
    for (i = 2; i < length; i++) {
      extend(Child.prototype, arguments[i]);
    }
    return Child;
  }

  var NOTHING = ['<nothing>'];
  var END = 'end';
  var VALUE = 'value';
  var ERROR = 'error';
  var ANY = 'any';

  function concat(a, b) {
    var result = void 0,
        length = void 0,
        i = void 0,
        j = void 0;
    if (a.length === 0) {
      return b;
    }
    if (b.length === 0) {
      return a;
    }
    j = 0;
    result = new Array(a.length + b.length);
    length = a.length;
    for (i = 0; i < length; i++, j++) {
      result[j] = a[i];
    }
    length = b.length;
    for (i = 0; i < length; i++, j++) {
      result[j] = b[i];
    }
    return result;
  }

  function find(arr, value) {
    var length = arr.length,
        i = void 0;
    for (i = 0; i < length; i++) {
      if (arr[i] === value) {
        return i;
      }
    }
    return -1;
  }

  function findByPred(arr, pred) {
    var length = arr.length,
        i = void 0;
    for (i = 0; i < length; i++) {
      if (pred(arr[i])) {
        return i;
      }
    }
    return -1;
  }

  function cloneArray(input) {
    var length = input.length,
        result = new Array(length),
        i = void 0;
    for (i = 0; i < length; i++) {
      result[i] = input[i];
    }
    return result;
  }

  function _remove(input, index) {
    var length = input.length,
        result = void 0,
        i = void 0,
        j = void 0;
    if (index >= 0 && index < length) {
      if (length === 1) {
        return [];
      } else {
        result = new Array(length - 1);
        for (i = 0, j = 0; i < length; i++) {
          if (i !== index) {
            result[j] = input[i];
            j++;
          }
        }
        return result;
      }
    } else {
      return input;
    }
  }

  function map(input, fn) {
    var length = input.length,
        result = new Array(length),
        i = void 0;
    for (i = 0; i < length; i++) {
      result[i] = fn(input[i]);
    }
    return result;
  }

  function forEach(arr, fn) {
    var length = arr.length,
        i = void 0;
    for (i = 0; i < length; i++) {
      fn(arr[i]);
    }
  }

  function fillArray(arr, value) {
    var length = arr.length,
        i = void 0;
    for (i = 0; i < length; i++) {
      arr[i] = value;
    }
  }

  function contains(arr, value) {
    return find(arr, value) !== -1;
  }

  function slide(cur, next, max) {
    var length = Math.min(max, cur.length + 1),
        offset = cur.length - length + 1,
        result = new Array(length),
        i = void 0;
    for (i = offset; i < length; i++) {
      result[i - offset] = cur[i];
    }
    result[length - 1] = next;
    return result;
  }

  function callSubscriber(type, fn, event) {
    if (type === ANY) {
      fn(event);
    } else if (type === event.type) {
      if (type === VALUE || type === ERROR) {
        fn(event.value);
      } else {
        fn();
      }
    }
  }

  function Dispatcher() {
    this._items = [];
    this._spies = [];
    this._inLoop = 0;
    this._removedItems = null;
  }

  extend(Dispatcher.prototype, {
    add: function add(type, fn) {
      this._items = concat(this._items, [{ type: type, fn: fn }]);
      return this._items.length;
    },
    remove: function remove(type, fn) {
      var index = findByPred(this._items, function (x) {
        return x.type === type && x.fn === fn;
      });

      // if we're currently in a notification loop,
      // remember this subscriber was removed
      if (this._inLoop !== 0 && index !== -1) {
        if (this._removedItems === null) {
          this._removedItems = [];
        }
        this._removedItems.push(this._items[index]);
      }

      this._items = _remove(this._items, index);
      return this._items.length;
    },
    addSpy: function addSpy(fn) {
      this._spies = concat(this._spies, [fn]);
      return this._spies.length;
    },

    // Because spies are only ever a function that perform logging as
    // their only side effect, we don't need the same complicated
    // removal logic like in remove()
    removeSpy: function removeSpy(fn) {
      this._spies = _remove(this._spies, this._spies.indexOf(fn));
      return this._spies.length;
    },
    dispatch: function dispatch(event) {
      this._inLoop++;
      for (var i = 0, spies = this._spies; this._spies !== null && i < spies.length; i++) {
        spies[i](event);
      }

      for (var _i = 0, items = this._items; _i < items.length; _i++) {
        // cleanup was called
        if (this._items === null) {
          break;
        }

        // this subscriber was removed
        if (this._removedItems !== null && contains(this._removedItems, items[_i])) {
          continue;
        }

        callSubscriber(items[_i].type, items[_i].fn, event);
      }
      this._inLoop--;
      if (this._inLoop === 0) {
        this._removedItems = null;
      }
    },
    cleanup: function cleanup() {
      this._items = null;
      this._spies = null;
    }
  });

  function Observable() {
    this._dispatcher = new Dispatcher();
    this._active = false;
    this._alive = true;
    this._activating = false;
    this._logHandlers = null;
    this._spyHandlers = null;
  }

  extend(Observable.prototype, {
    _name: 'observable',

    _onActivation: function _onActivation() {},
    _onDeactivation: function _onDeactivation() {},
    _setActive: function _setActive(active) {
      if (this._active !== active) {
        this._active = active;
        if (active) {
          this._activating = true;
          this._onActivation();
          this._activating = false;
        } else {
          this._onDeactivation();
        }
      }
    },
    _clear: function _clear() {
      this._setActive(false);
      this._dispatcher.cleanup();
      this._dispatcher = null;
      this._logHandlers = null;
    },
    _emit: function _emit(type, x) {
      switch (type) {
        case VALUE:
          return this._emitValue(x);
        case ERROR:
          return this._emitError(x);
        case END:
          return this._emitEnd();
      }
    },
    _emitValue: function _emitValue(value) {
      if (this._alive) {
        this._dispatcher.dispatch({ type: VALUE, value: value });
      }
    },
    _emitError: function _emitError(value) {
      if (this._alive) {
        this._dispatcher.dispatch({ type: ERROR, value: value });
      }
    },
    _emitEnd: function _emitEnd() {
      if (this._alive) {
        this._alive = false;
        this._dispatcher.dispatch({ type: END });
        this._clear();
      }
    },
    _on: function _on(type, fn) {
      if (this._alive) {
        this._dispatcher.add(type, fn);
        this._setActive(true);
      } else {
        callSubscriber(type, fn, { type: END });
      }
      return this;
    },
    _off: function _off(type, fn) {
      if (this._alive) {
        var count = this._dispatcher.remove(type, fn);
        if (count === 0) {
          this._setActive(false);
        }
      }
      return this;
    },
    onValue: function onValue(fn) {
      return this._on(VALUE, fn);
    },
    onError: function onError(fn) {
      return this._on(ERROR, fn);
    },
    onEnd: function onEnd(fn) {
      return this._on(END, fn);
    },
    onAny: function onAny(fn) {
      return this._on(ANY, fn);
    },
    offValue: function offValue(fn) {
      return this._off(VALUE, fn);
    },
    offError: function offError(fn) {
      return this._off(ERROR, fn);
    },
    offEnd: function offEnd(fn) {
      return this._off(END, fn);
    },
    offAny: function offAny(fn) {
      return this._off(ANY, fn);
    },
    observe: function observe(observerOrOnValue, onError, onEnd) {
      var _this = this;
      var closed = false;

      var observer = !observerOrOnValue || typeof observerOrOnValue === 'function' ? { value: observerOrOnValue, error: onError, end: onEnd } : observerOrOnValue;

      var handler = function handler(event) {
        if (event.type === END) {
          closed = true;
        }
        if (event.type === VALUE && observer.value) {
          observer.value(event.value);
        } else if (event.type === ERROR && observer.error) {
          observer.error(event.value);
        } else if (event.type === END && observer.end) {
          observer.end(event.value);
        }
      };

      this.onAny(handler);

      return {
        unsubscribe: function unsubscribe() {
          if (!closed) {
            _this.offAny(handler);
            closed = true;
          }
        },

        get closed() {
          return closed;
        }
      };
    },

    // A and B must be subclasses of Stream and Property (order doesn't matter)
    _ofSameType: function _ofSameType(A, B) {
      return A.prototype.getType() === this.getType() ? A : B;
    },
    setName: function setName(sourceObs /* optional */, selfName) {
      this._name = selfName ? sourceObs._name + '.' + selfName : sourceObs;
      return this;
    },
    log: function log() {
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.toString();

      var isCurrent = void 0;
      var handler = function handler(event) {
        var type = '<' + event.type + (isCurrent ? ':current' : '') + '>';
        if (event.type === END) {
          console.log(name, type);
        } else {
          console.log(name, type, event.value);
        }
      };

      if (this._alive) {
        if (!this._logHandlers) {
          this._logHandlers = [];
        }
        this._logHandlers.push({ name: name, handler: handler });
      }

      isCurrent = true;
      this.onAny(handler);
      isCurrent = false;

      return this;
    },
    offLog: function offLog() {
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.toString();

      if (this._logHandlers) {
        var handlerIndex = findByPred(this._logHandlers, function (obj) {
          return obj.name === name;
        });
        if (handlerIndex !== -1) {
          this.offAny(this._logHandlers[handlerIndex].handler);
          this._logHandlers.splice(handlerIndex, 1);
        }
      }

      return this;
    },
    spy: function spy() {
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.toString();

      var handler = function handler(event) {
        var type = '<' + event.type + '>';
        if (event.type === END) {
          console.log(name, type);
        } else {
          console.log(name, type, event.value);
        }
      };
      if (this._alive) {
        if (!this._spyHandlers) {
          this._spyHandlers = [];
        }
        this._spyHandlers.push({ name: name, handler: handler });
        this._dispatcher.addSpy(handler);
      }
      return this;
    },
    offSpy: function offSpy() {
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.toString();

      if (this._spyHandlers) {
        var handlerIndex = findByPred(this._spyHandlers, function (obj) {
          return obj.name === name;
        });
        if (handlerIndex !== -1) {
          this._dispatcher.removeSpy(this._spyHandlers[handlerIndex].handler);
          this._spyHandlers.splice(handlerIndex, 1);
        }
      }
      return this;
    }
  });

  // extend() can't handle `toString` in IE8
  Observable.prototype.toString = function () {
    return '[' + this._name + ']';
  };

  function Stream() {
    Observable.call(this);
  }

  inherit(Stream, Observable, {
    _name: 'stream',

    getType: function getType() {
      return 'stream';
    }
  });

  function Property() {
    Observable.call(this);
    this._currentEvent = null;
  }

  inherit(Property, Observable, {
    _name: 'property',

    _emitValue: function _emitValue(value) {
      if (this._alive) {
        this._currentEvent = { type: VALUE, value: value };
        if (!this._activating) {
          this._dispatcher.dispatch({ type: VALUE, value: value });
        }
      }
    },
    _emitError: function _emitError(value) {
      if (this._alive) {
        this._currentEvent = { type: ERROR, value: value };
        if (!this._activating) {
          this._dispatcher.dispatch({ type: ERROR, value: value });
        }
      }
    },
    _emitEnd: function _emitEnd() {
      if (this._alive) {
        this._alive = false;
        if (!this._activating) {
          this._dispatcher.dispatch({ type: END });
        }
        this._clear();
      }
    },
    _on: function _on(type, fn) {
      if (this._alive) {
        this._dispatcher.add(type, fn);
        this._setActive(true);
      }
      if (this._currentEvent !== null) {
        callSubscriber(type, fn, this._currentEvent);
      }
      if (!this._alive) {
        callSubscriber(type, fn, { type: END });
      }
      return this;
    },
    getType: function getType() {
      return 'property';
    }
  });

  var neverS = new Stream();
  neverS._emitEnd();
  neverS._name = 'never';

  function never() {
    return neverS;
  }

  function timeBased(mixin) {
    function AnonymousStream(wait, options) {
      var _this = this;

      Stream.call(this);
      this._wait = wait;
      this._intervalId = null;
      this._$onTick = function () {
        return _this._onTick();
      };
      this._init(options);
    }

    inherit(AnonymousStream, Stream, {
      _init: function _init() {},
      _free: function _free() {},
      _onTick: function _onTick() {},
      _onActivation: function _onActivation() {
        this._intervalId = setInterval(this._$onTick, this._wait);
      },
      _onDeactivation: function _onDeactivation() {
        if (this._intervalId !== null) {
          clearInterval(this._intervalId);
          this._intervalId = null;
        }
      },
      _clear: function _clear() {
        Stream.prototype._clear.call(this);
        this._$onTick = null;
        this._free();
      }
    }, mixin);

    return AnonymousStream;
  }

  var S = timeBased({
    _name: 'later',

    _init: function _init(_ref) {
      var x = _ref.x;

      this._x = x;
    },
    _free: function _free() {
      this._x = null;
    },
    _onTick: function _onTick() {
      this._emitValue(this._x);
      this._emitEnd();
    }
  });

  function later(wait, x) {
    return new S(wait, { x: x });
  }

  var S$1 = timeBased({
    _name: 'interval',

    _init: function _init(_ref) {
      var x = _ref.x;

      this._x = x;
    },
    _free: function _free() {
      this._x = null;
    },
    _onTick: function _onTick() {
      this._emitValue(this._x);
    }
  });

  function interval(wait, x) {
    return new S$1(wait, { x: x });
  }

  var S$2 = timeBased({
    _name: 'sequentially',

    _init: function _init(_ref) {
      var xs = _ref.xs;

      this._xs = cloneArray(xs);
    },
    _free: function _free() {
      this._xs = null;
    },
    _onTick: function _onTick() {
      if (this._xs.length === 1) {
        this._emitValue(this._xs[0]);
        this._emitEnd();
      } else {
        this._emitValue(this._xs.shift());
      }
    }
  });

  function sequentially(wait, xs) {
    return xs.length === 0 ? never() : new S$2(wait, { xs: xs });
  }

  var S$3 = timeBased({
    _name: 'fromPoll',

    _init: function _init(_ref) {
      var fn = _ref.fn;

      this._fn = fn;
    },
    _free: function _free() {
      this._fn = null;
    },
    _onTick: function _onTick() {
      var fn = this._fn;
      this._emitValue(fn());
    }
  });

  function fromPoll(wait, fn) {
    return new S$3(wait, { fn: fn });
  }

  function emitter(obs) {
    function value(x) {
      obs._emitValue(x);
      return obs._active;
    }

    function error(x) {
      obs._emitError(x);
      return obs._active;
    }

    function end() {
      obs._emitEnd();
      return obs._active;
    }

    function event(e) {
      obs._emit(e.type, e.value);
      return obs._active;
    }

    return {
      value: value,
      error: error,
      end: end,
      event: event,

      // legacy
      emit: value,
      emitEvent: event
    };
  }

  var S$4 = timeBased({
    _name: 'withInterval',

    _init: function _init(_ref) {
      var fn = _ref.fn;

      this._fn = fn;
      this._emitter = emitter(this);
    },
    _free: function _free() {
      this._fn = null;
      this._emitter = null;
    },
    _onTick: function _onTick() {
      var fn = this._fn;
      fn(this._emitter);
    }
  });

  function withInterval(wait, fn) {
    return new S$4(wait, { fn: fn });
  }

  function S$5(fn) {
    Stream.call(this);
    this._fn = fn;
    this._unsubscribe = null;
  }

  inherit(S$5, Stream, {
    _name: 'stream',

    _onActivation: function _onActivation() {
      var fn = this._fn;
      var unsubscribe = fn(emitter(this));
      this._unsubscribe = typeof unsubscribe === 'function' ? unsubscribe : null;

      // fix https://github.com/rpominov/kefir/issues/35
      if (!this._active) {
        this._callUnsubscribe();
      }
    },
    _callUnsubscribe: function _callUnsubscribe() {
      if (this._unsubscribe !== null) {
        this._unsubscribe();
        this._unsubscribe = null;
      }
    },
    _onDeactivation: function _onDeactivation() {
      this._callUnsubscribe();
    },
    _clear: function _clear() {
      Stream.prototype._clear.call(this);
      this._fn = null;
    }
  });

  function stream(fn) {
    return new S$5(fn);
  }

  function fromCallback(callbackConsumer) {
    var called = false;

    return stream(function (emitter) {
      if (!called) {
        callbackConsumer(function (x) {
          emitter.emit(x);
          emitter.end();
        });
        called = true;
      }
    }).setName('fromCallback');
  }

  function fromNodeCallback(callbackConsumer) {
    var called = false;

    return stream(function (emitter) {
      if (!called) {
        callbackConsumer(function (error, x) {
          if (error) {
            emitter.error(error);
          } else {
            emitter.emit(x);
          }
          emitter.end();
        });
        called = true;
      }
    }).setName('fromNodeCallback');
  }

  function spread(fn, length) {
    switch (length) {
      case 0:
        return function () {
          return fn();
        };
      case 1:
        return function (a) {
          return fn(a[0]);
        };
      case 2:
        return function (a) {
          return fn(a[0], a[1]);
        };
      case 3:
        return function (a) {
          return fn(a[0], a[1], a[2]);
        };
      case 4:
        return function (a) {
          return fn(a[0], a[1], a[2], a[3]);
        };
      default:
        return function (a) {
          return fn.apply(null, a);
        };
    }
  }

  function apply(fn, c, a) {
    var aLength = a ? a.length : 0;
    if (c == null) {
      switch (aLength) {
        case 0:
          return fn();
        case 1:
          return fn(a[0]);
        case 2:
          return fn(a[0], a[1]);
        case 3:
          return fn(a[0], a[1], a[2]);
        case 4:
          return fn(a[0], a[1], a[2], a[3]);
        default:
          return fn.apply(null, a);
      }
    } else {
      switch (aLength) {
        case 0:
          return fn.call(c);
        default:
          return fn.apply(c, a);
      }
    }
  }

  function fromSubUnsub(sub, unsub, transformer /* Function | falsey */) {
    return stream(function (emitter) {
      var handler = transformer ? function () {
        emitter.emit(apply(transformer, this, arguments));
      } : function (x) {
        emitter.emit(x);
      };

      sub(handler);
      return function () {
        return unsub(handler);
      };
    }).setName('fromSubUnsub');
  }

  var pairs = [['addEventListener', 'removeEventListener'], ['addListener', 'removeListener'], ['on', 'off']];

  function fromEvents(target, eventName, transformer) {
    var sub = void 0,
        unsub = void 0;

    for (var i = 0; i < pairs.length; i++) {
      if (typeof target[pairs[i][0]] === 'function' && typeof target[pairs[i][1]] === 'function') {
        sub = pairs[i][0];
        unsub = pairs[i][1];
        break;
      }
    }

    if (sub === undefined) {
      throw new Error("target don't support any of " + 'addEventListener/removeEventListener, addListener/removeListener, on/off method pair');
    }

    return fromSubUnsub(function (handler) {
      return target[sub](eventName, handler);
    }, function (handler) {
      return target[unsub](eventName, handler);
    }, transformer).setName('fromEvents');
  }

  // HACK:
  //   We don't call parent Class constructor, but instead putting all necessary
  //   properties into prototype to simulate ended Property
  //   (see Propperty and Observable classes).

  function P(value) {
    this._currentEvent = { type: 'value', value: value, current: true };
  }

  inherit(P, Property, {
    _name: 'constant',
    _active: false,
    _activating: false,
    _alive: false,
    _dispatcher: null,
    _logHandlers: null
  });

  function constant(x) {
    return new P(x);
  }

  // HACK:
  //   We don't call parent Class constructor, but instead putting all necessary
  //   properties into prototype to simulate ended Property
  //   (see Propperty and Observable classes).

  function P$1(value) {
    this._currentEvent = { type: 'error', value: value, current: true };
  }

  inherit(P$1, Property, {
    _name: 'constantError',
    _active: false,
    _activating: false,
    _alive: false,
    _dispatcher: null,
    _logHandlers: null
  });

  function constantError(x) {
    return new P$1(x);
  }

  function createConstructor(BaseClass, name) {
    return function AnonymousObservable(source, options) {
      var _this = this;

      BaseClass.call(this);
      this._source = source;
      this._name = source._name + '.' + name;
      this._init(options);
      this._$handleAny = function (event) {
        return _this._handleAny(event);
      };
    };
  }

  function createClassMethods(BaseClass) {
    return {
      _init: function _init() {},
      _free: function _free() {},
      _handleValue: function _handleValue(x) {
        this._emitValue(x);
      },
      _handleError: function _handleError(x) {
        this._emitError(x);
      },
      _handleEnd: function _handleEnd() {
        this._emitEnd();
      },
      _handleAny: function _handleAny(event) {
        switch (event.type) {
          case VALUE:
            return this._handleValue(event.value);
          case ERROR:
            return this._handleError(event.value);
          case END:
            return this._handleEnd();
        }
      },
      _onActivation: function _onActivation() {
        this._source.onAny(this._$handleAny);
      },
      _onDeactivation: function _onDeactivation() {
        this._source.offAny(this._$handleAny);
      },
      _clear: function _clear() {
        BaseClass.prototype._clear.call(this);
        this._source = null;
        this._$handleAny = null;
        this._free();
      }
    };
  }

  function createStream(name, mixin) {
    var S = createConstructor(Stream, name);
    inherit(S, Stream, createClassMethods(Stream), mixin);
    return S;
  }

  function createProperty(name, mixin) {
    var P = createConstructor(Property, name);
    inherit(P, Property, createClassMethods(Property), mixin);
    return P;
  }

  var P$2 = createProperty('toProperty', {
    _init: function _init(_ref) {
      var fn = _ref.fn;

      this._getInitialCurrent = fn;
    },
    _onActivation: function _onActivation() {
      if (this._getInitialCurrent !== null) {
        var getInitial = this._getInitialCurrent;
        this._emitValue(getInitial());
      }
      this._source.onAny(this._$handleAny); // copied from patterns/one-source
    }
  });

  function toProperty(obs) {
    var fn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

    if (fn !== null && typeof fn !== 'function') {
      throw new Error('You should call toProperty() with a function or no arguments.');
    }
    return new P$2(obs, { fn: fn });
  }

  var S$6 = createStream('changes', {
    _handleValue: function _handleValue(x) {
      if (!this._activating) {
        this._emitValue(x);
      }
    },
    _handleError: function _handleError(x) {
      if (!this._activating) {
        this._emitError(x);
      }
    }
  });

  function changes(obs) {
    return new S$6(obs);
  }

  function fromPromise(promise) {
    var called = false;

    var result = stream(function (emitter) {
      if (!called) {
        var onValue = function onValue(x) {
          emitter.emit(x);
          emitter.end();
        };
        var onError = function onError(x) {
          emitter.error(x);
          emitter.end();
        };
        var _promise = promise.then(onValue, onError);

        // prevent libraries like 'Q' or 'when' from swallowing exceptions
        if (_promise && typeof _promise.done === 'function') {
          _promise.done();
        }

        called = true;
      }
    });

    return toProperty(result, null).setName('fromPromise');
  }

  function getGlodalPromise() {
    if (typeof Promise === 'function') {
      return Promise;
    } else {
      throw new Error("There isn't default Promise, use shim or parameter");
    }
  }

  var toPromise = function toPromise(obs) {
    var Promise = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : getGlodalPromise();

    var last = null;
    return new Promise(function (resolve, reject) {
      obs.onAny(function (event) {
        if (event.type === END && last !== null) {
          (last.type === VALUE ? resolve : reject)(last.value);
          last = null;
        } else {
          last = event;
        }
      });
    });
  };

  var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

  function createCommonjsModule(fn, module) {
    return module = { exports: {} }, fn(module, module.exports), module.exports;
  }

  var ponyfill = createCommonjsModule(function (module, exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports['default'] = symbolObservablePonyfill;
    function symbolObservablePonyfill(root) {
      var result;
      var _Symbol = root.Symbol;

      if (typeof _Symbol === 'function') {
        if (_Symbol.observable) {
          result = _Symbol.observable;
        } else {
          result = _Symbol('observable');
          _Symbol.observable = result;
        }
      } else {
        result = '@@observable';
      }

      return result;
    }
  });

  var index$1 = createCommonjsModule(function (module, exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var _ponyfill2 = _interopRequireDefault(ponyfill);

    function _interopRequireDefault(obj) {
      return obj && obj.__esModule ? obj : { 'default': obj };
    }

    var root; /* global window */

    if (typeof self !== 'undefined') {
      root = self;
    } else if (typeof window !== 'undefined') {
      root = window;
    } else if (typeof commonjsGlobal !== 'undefined') {
      root = commonjsGlobal;
    } else {
      root = module;
    }

    var result = (0, _ponyfill2['default'])(root);
    exports['default'] = result;
  });

  var index = index$1;

  // this file contains some hot JS modules systems stuff

  var $$observable = index.default ? index.default : index;

  function fromESObservable(_observable) {
    var observable = _observable[$$observable] ? _observable[$$observable]() : _observable;
    return stream(function (emitter) {
      var unsub = observable.subscribe({
        error: function error(_error) {
          emitter.error(_error);
          emitter.end();
        },
        next: function next(value) {
          emitter.emit(value);
        },
        complete: function complete() {
          emitter.end();
        }
      });

      if (unsub.unsubscribe) {
        return function () {
          unsub.unsubscribe();
        };
      } else {
        return unsub;
      }
    }).setName('fromESObservable');
  }

  function ESObservable(observable) {
    this._observable = observable.takeErrors(1);
  }

  extend(ESObservable.prototype, {
    subscribe: function subscribe(observerOrOnNext, onError, onComplete) {
      var _this = this;

      var observer = typeof observerOrOnNext === 'function' ? { next: observerOrOnNext, error: onError, complete: onComplete } : observerOrOnNext;

      var fn = function fn(event) {
        if (event.type === END) {
          closed = true;
        }

        if (event.type === VALUE && observer.next) {
          observer.next(event.value);
        } else if (event.type === ERROR && observer.error) {
          observer.error(event.value);
        } else if (event.type === END && observer.complete) {
          observer.complete(event.value);
        }
      };

      this._observable.onAny(fn);
      var closed = false;

      var subscription = {
        unsubscribe: function unsubscribe() {
          closed = true;
          _this._observable.offAny(fn);
        },
        get closed() {
          return closed;
        }
      };
      return subscription;
    }
  });

  // Need to assign directly b/c Symbols aren't enumerable.
  ESObservable.prototype[$$observable] = function () {
    return this;
  };

  function toESObservable() {
    return new ESObservable(this);
  }

  function collect(source, keys, values) {
    for (var prop in source) {
      if (source.hasOwnProperty(prop)) {
        keys.push(prop);
        values.push(source[prop]);
      }
    }
  }

  function defaultErrorsCombinator(errors) {
    var latestError = void 0;
    for (var i = 0; i < errors.length; i++) {
      if (errors[i] !== undefined) {
        if (latestError === undefined || latestError.index < errors[i].index) {
          latestError = errors[i];
        }
      }
    }
    return latestError.error;
  }

  function Combine(active, passive, combinator) {
    var _this = this;

    Stream.call(this);
    this._activeCount = active.length;
    this._sources = concat(active, passive);
    this._combinator = combinator;
    this._aliveCount = 0;
    this._latestValues = new Array(this._sources.length);
    this._latestErrors = new Array(this._sources.length);
    fillArray(this._latestValues, NOTHING);
    this._emitAfterActivation = false;
    this._endAfterActivation = false;
    this._latestErrorIndex = 0;

    this._$handlers = [];

    var _loop = function _loop(i) {
      _this._$handlers.push(function (event) {
        return _this._handleAny(i, event);
      });
    };

    for (var i = 0; i < this._sources.length; i++) {
      _loop(i);
    }
  }

  inherit(Combine, Stream, {
    _name: 'combine',

    _onActivation: function _onActivation() {
      this._aliveCount = this._activeCount;

      // we need to suscribe to _passive_ sources before _active_
      // (see https://github.com/rpominov/kefir/issues/98)
      for (var i = this._activeCount; i < this._sources.length; i++) {
        this._sources[i].onAny(this._$handlers[i]);
      }
      for (var _i = 0; _i < this._activeCount; _i++) {
        this._sources[_i].onAny(this._$handlers[_i]);
      }

      if (this._emitAfterActivation) {
        this._emitAfterActivation = false;
        this._emitIfFull();
      }
      if (this._endAfterActivation) {
        this._emitEnd();
      }
    },
    _onDeactivation: function _onDeactivation() {
      var length = this._sources.length,
          i = void 0;
      for (i = 0; i < length; i++) {
        this._sources[i].offAny(this._$handlers[i]);
      }
    },
    _emitIfFull: function _emitIfFull() {
      var hasAllValues = true;
      var hasErrors = false;
      var length = this._latestValues.length;
      var valuesCopy = new Array(length);
      var errorsCopy = new Array(length);

      for (var i = 0; i < length; i++) {
        valuesCopy[i] = this._latestValues[i];
        errorsCopy[i] = this._latestErrors[i];

        if (valuesCopy[i] === NOTHING) {
          hasAllValues = false;
        }

        if (errorsCopy[i] !== undefined) {
          hasErrors = true;
        }
      }

      if (hasAllValues) {
        var combinator = this._combinator;
        this._emitValue(combinator(valuesCopy));
      }
      if (hasErrors) {
        this._emitError(defaultErrorsCombinator(errorsCopy));
      }
    },
    _handleAny: function _handleAny(i, event) {
      if (event.type === VALUE || event.type === ERROR) {
        if (event.type === VALUE) {
          this._latestValues[i] = event.value;
          this._latestErrors[i] = undefined;
        }
        if (event.type === ERROR) {
          this._latestValues[i] = NOTHING;
          this._latestErrors[i] = {
            index: this._latestErrorIndex++,
            error: event.value
          };
        }

        if (i < this._activeCount) {
          if (this._activating) {
            this._emitAfterActivation = true;
          } else {
            this._emitIfFull();
          }
        }
      } else {
        // END

        if (i < this._activeCount) {
          this._aliveCount--;
          if (this._aliveCount === 0) {
            if (this._activating) {
              this._endAfterActivation = true;
            } else {
              this._emitEnd();
            }
          }
        }
      }
    },
    _clear: function _clear() {
      Stream.prototype._clear.call(this);
      this._sources = null;
      this._latestValues = null;
      this._latestErrors = null;
      this._combinator = null;
      this._$handlers = null;
    }
  });

  function combineAsArray(active) {
    var passive = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    var combinator = arguments[2];

    if (!Array.isArray(passive)) {
      throw new Error('Combine can only combine active and passive collections of the same type.');
    }

    combinator = combinator ? spread(combinator, active.length + passive.length) : function (x) {
      return x;
    };
    return active.length === 0 ? never() : new Combine(active, passive, combinator);
  }

  function combineAsObject(active) {
    var passive = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var combinator = arguments[2];

    if ((typeof passive === 'undefined' ? 'undefined' : _typeof(passive)) !== 'object' || Array.isArray(passive)) {
      throw new Error('Combine can only combine active and passive collections of the same type.');
    }

    var keys = [],
        activeObservables = [],
        passiveObservables = [];

    collect(active, keys, activeObservables);
    collect(passive, keys, passiveObservables);

    var objectify = function objectify(values) {
      var event = {};
      for (var i = values.length - 1; 0 <= i; i--) {
        event[keys[i]] = values[i];
      }
      return combinator ? combinator(event) : event;
    };

    return activeObservables.length === 0 ? never() : new Combine(activeObservables, passiveObservables, objectify);
  }

  function combine(active, passive, combinator) {
    if (typeof passive === 'function') {
      combinator = passive;
      passive = undefined;
    }

    return Array.isArray(active) ? combineAsArray(active, passive, combinator) : combineAsObject(active, passive, combinator);
  }

  var Observable$2 = {
    empty: function empty() {
      return never();
    },

    // Monoid based on merge() seems more useful than one based on concat().
    concat: function concat(a, b) {
      return a.merge(b);
    },
    of: function of(x) {
      return constant(x);
    },
    map: function map(fn, obs) {
      return obs.map(fn);
    },
    bimap: function bimap(fnErr, fnVal, obs) {
      return obs.mapErrors(fnErr).map(fnVal);
    },

    // This ap strictly speaking incompatible with chain. If we derive ap from chain we get
    // different (not very useful) behavior. But spec requires that if method can be derived
    // it must have the same behavior as hand-written method. We intentionally violate the spec
    // in hope that it won't cause many troubles in practice. And in return we have more useful type.
    ap: function ap(obsFn, obsVal) {
      return combine([obsFn, obsVal], function (fn, val) {
        return fn(val);
      });
    },
    chain: function chain(fn, obs) {
      return obs.flatMap(fn);
    }
  };

  var staticLand = Object.freeze({
    Observable: Observable$2
  });

  var mixin = {
    _init: function _init(_ref) {
      var fn = _ref.fn;

      this._fn = fn;
    },
    _free: function _free() {
      this._fn = null;
    },
    _handleValue: function _handleValue(x) {
      var fn = this._fn;
      this._emitValue(fn(x));
    }
  };

  var S$7 = createStream('map', mixin);
  var P$3 = createProperty('map', mixin);

  var id = function id(x) {
    return x;
  };

  function map$1(obs) {
    var fn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : id;

    return new (obs._ofSameType(S$7, P$3))(obs, { fn: fn });
  }

  var mixin$1 = {
    _init: function _init(_ref) {
      var fn = _ref.fn;

      this._fn = fn;
    },
    _free: function _free() {
      this._fn = null;
    },
    _handleValue: function _handleValue(x) {
      var fn = this._fn;
      if (fn(x)) {
        this._emitValue(x);
      }
    }
  };

  var S$8 = createStream('filter', mixin$1);
  var P$4 = createProperty('filter', mixin$1);

  var id$1 = function id$1(x) {
    return x;
  };

  function filter(obs) {
    var fn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : id$1;

    return new (obs._ofSameType(S$8, P$4))(obs, { fn: fn });
  }

  var mixin$2 = {
    _init: function _init(_ref) {
      var n = _ref.n;

      this._n = n;
      if (n <= 0) {
        this._emitEnd();
      }
    },
    _handleValue: function _handleValue(x) {
      if (this._n === 0) {
        return;
      }
      this._n--;
      this._emitValue(x);
      if (this._n === 0) {
        this._emitEnd();
      }
    }
  };

  var S$9 = createStream('take', mixin$2);
  var P$5 = createProperty('take', mixin$2);

  function take(obs, n) {
    return new (obs._ofSameType(S$9, P$5))(obs, { n: n });
  }

  var mixin$3 = {
    _init: function _init(_ref) {
      var n = _ref.n;

      this._n = n;
      if (n <= 0) {
        this._emitEnd();
      }
    },
    _handleError: function _handleError(x) {
      if (this._n === 0) {
        return;
      }
      this._n--;
      this._emitError(x);
      if (this._n === 0) {
        this._emitEnd();
      }
    }
  };

  var S$10 = createStream('takeErrors', mixin$3);
  var P$6 = createProperty('takeErrors', mixin$3);

  function takeErrors(obs, n) {
    return new (obs._ofSameType(S$10, P$6))(obs, { n: n });
  }

  var mixin$4 = {
    _init: function _init(_ref) {
      var fn = _ref.fn;

      this._fn = fn;
    },
    _free: function _free() {
      this._fn = null;
    },
    _handleValue: function _handleValue(x) {
      var fn = this._fn;
      if (fn(x)) {
        this._emitValue(x);
      } else {
        this._emitEnd();
      }
    }
  };

  var S$11 = createStream('takeWhile', mixin$4);
  var P$7 = createProperty('takeWhile', mixin$4);

  var id$2 = function id$2(x) {
    return x;
  };

  function takeWhile(obs) {
    var fn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : id$2;

    return new (obs._ofSameType(S$11, P$7))(obs, { fn: fn });
  }

  var mixin$5 = {
    _init: function _init() {
      this._lastValue = NOTHING;
    },
    _free: function _free() {
      this._lastValue = null;
    },
    _handleValue: function _handleValue(x) {
      this._lastValue = x;
    },
    _handleEnd: function _handleEnd() {
      if (this._lastValue !== NOTHING) {
        this._emitValue(this._lastValue);
      }
      this._emitEnd();
    }
  };

  var S$12 = createStream('last', mixin$5);
  var P$8 = createProperty('last', mixin$5);

  function last(obs) {
    return new (obs._ofSameType(S$12, P$8))(obs);
  }

  var mixin$6 = {
    _init: function _init(_ref) {
      var n = _ref.n;

      this._n = Math.max(0, n);
    },
    _handleValue: function _handleValue(x) {
      if (this._n === 0) {
        this._emitValue(x);
      } else {
        this._n--;
      }
    }
  };

  var S$13 = createStream('skip', mixin$6);
  var P$9 = createProperty('skip', mixin$6);

  function skip(obs, n) {
    return new (obs._ofSameType(S$13, P$9))(obs, { n: n });
  }

  var mixin$7 = {
    _init: function _init(_ref) {
      var fn = _ref.fn;

      this._fn = fn;
    },
    _free: function _free() {
      this._fn = null;
    },
    _handleValue: function _handleValue(x) {
      var fn = this._fn;
      if (this._fn !== null && !fn(x)) {
        this._fn = null;
      }
      if (this._fn === null) {
        this._emitValue(x);
      }
    }
  };

  var S$14 = createStream('skipWhile', mixin$7);
  var P$10 = createProperty('skipWhile', mixin$7);

  var id$3 = function id$3(x) {
    return x;
  };

  function skipWhile(obs) {
    var fn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : id$3;

    return new (obs._ofSameType(S$14, P$10))(obs, { fn: fn });
  }

  var mixin$8 = {
    _init: function _init(_ref) {
      var fn = _ref.fn;

      this._fn = fn;
      this._prev = NOTHING;
    },
    _free: function _free() {
      this._fn = null;
      this._prev = null;
    },
    _handleValue: function _handleValue(x) {
      var fn = this._fn;
      if (this._prev === NOTHING || !fn(this._prev, x)) {
        this._prev = x;
        this._emitValue(x);
      }
    }
  };

  var S$15 = createStream('skipDuplicates', mixin$8);
  var P$11 = createProperty('skipDuplicates', mixin$8);

  var eq = function eq(a, b) {
    return a === b;
  };

  function skipDuplicates(obs) {
    var fn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : eq;

    return new (obs._ofSameType(S$15, P$11))(obs, { fn: fn });
  }

  var mixin$9 = {
    _init: function _init(_ref) {
      var fn = _ref.fn,
          seed = _ref.seed;

      this._fn = fn;
      this._prev = seed;
    },
    _free: function _free() {
      this._prev = null;
      this._fn = null;
    },
    _handleValue: function _handleValue(x) {
      if (this._prev !== NOTHING) {
        var fn = this._fn;
        this._emitValue(fn(this._prev, x));
      }
      this._prev = x;
    }
  };

  var S$16 = createStream('diff', mixin$9);
  var P$12 = createProperty('diff', mixin$9);

  function defaultFn(a, b) {
    return [a, b];
  }

  function diff(obs, fn) {
    var seed = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : NOTHING;

    return new (obs._ofSameType(S$16, P$12))(obs, { fn: fn || defaultFn, seed: seed });
  }

  var P$13 = createProperty('scan', {
    _init: function _init(_ref) {
      var fn = _ref.fn,
          seed = _ref.seed;

      this._fn = fn;
      this._seed = seed;
      if (seed !== NOTHING) {
        this._emitValue(seed);
      }
    },
    _free: function _free() {
      this._fn = null;
      this._seed = null;
    },
    _handleValue: function _handleValue(x) {
      var fn = this._fn;
      if (this._currentEvent === null || this._currentEvent.type === ERROR) {
        this._emitValue(this._seed === NOTHING ? x : fn(this._seed, x));
      } else {
        this._emitValue(fn(this._currentEvent.value, x));
      }
    }
  });

  function scan(obs, fn) {
    var seed = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : NOTHING;

    return new P$13(obs, { fn: fn, seed: seed });
  }

  var mixin$10 = {
    _init: function _init(_ref) {
      var fn = _ref.fn;

      this._fn = fn;
    },
    _free: function _free() {
      this._fn = null;
    },
    _handleValue: function _handleValue(x) {
      var fn = this._fn;
      var xs = fn(x);
      for (var i = 0; i < xs.length; i++) {
        this._emitValue(xs[i]);
      }
    }
  };

  var S$17 = createStream('flatten', mixin$10);

  var id$4 = function id$4(x) {
    return x;
  };

  function flatten(obs) {
    var fn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : id$4;

    return new S$17(obs, { fn: fn });
  }

  var END_MARKER = {};

  var mixin$11 = {
    _init: function _init(_ref) {
      var _this = this;

      var wait = _ref.wait;

      this._wait = Math.max(0, wait);
      this._buff = [];
      this._$shiftBuff = function () {
        var value = _this._buff.shift();
        if (value === END_MARKER) {
          _this._emitEnd();
        } else {
          _this._emitValue(value);
        }
      };
    },
    _free: function _free() {
      this._buff = null;
      this._$shiftBuff = null;
    },
    _handleValue: function _handleValue(x) {
      if (this._activating) {
        this._emitValue(x);
      } else {
        this._buff.push(x);
        setTimeout(this._$shiftBuff, this._wait);
      }
    },
    _handleEnd: function _handleEnd() {
      if (this._activating) {
        this._emitEnd();
      } else {
        this._buff.push(END_MARKER);
        setTimeout(this._$shiftBuff, this._wait);
      }
    }
  };

  var S$18 = createStream('delay', mixin$11);
  var P$14 = createProperty('delay', mixin$11);

  function delay(obs, wait) {
    return new (obs._ofSameType(S$18, P$14))(obs, { wait: wait });
  }

  var now = Date.now ? function () {
    return Date.now();
  } : function () {
    return new Date().getTime();
  };

  var mixin$12 = {
    _init: function _init(_ref) {
      var _this = this;

      var wait = _ref.wait,
          leading = _ref.leading,
          trailing = _ref.trailing;

      this._wait = Math.max(0, wait);
      this._leading = leading;
      this._trailing = trailing;
      this._trailingValue = null;
      this._timeoutId = null;
      this._endLater = false;
      this._lastCallTime = 0;
      this._$trailingCall = function () {
        return _this._trailingCall();
      };
    },
    _free: function _free() {
      this._trailingValue = null;
      this._$trailingCall = null;
    },
    _handleValue: function _handleValue(x) {
      if (this._activating) {
        this._emitValue(x);
      } else {
        var curTime = now();
        if (this._lastCallTime === 0 && !this._leading) {
          this._lastCallTime = curTime;
        }
        var remaining = this._wait - (curTime - this._lastCallTime);
        if (remaining <= 0) {
          this._cancelTrailing();
          this._lastCallTime = curTime;
          this._emitValue(x);
        } else if (this._trailing) {
          this._cancelTrailing();
          this._trailingValue = x;
          this._timeoutId = setTimeout(this._$trailingCall, remaining);
        }
      }
    },
    _handleEnd: function _handleEnd() {
      if (this._activating) {
        this._emitEnd();
      } else {
        if (this._timeoutId) {
          this._endLater = true;
        } else {
          this._emitEnd();
        }
      }
    },
    _cancelTrailing: function _cancelTrailing() {
      if (this._timeoutId !== null) {
        clearTimeout(this._timeoutId);
        this._timeoutId = null;
      }
    },
    _trailingCall: function _trailingCall() {
      this._emitValue(this._trailingValue);
      this._timeoutId = null;
      this._trailingValue = null;
      this._lastCallTime = !this._leading ? 0 : now();
      if (this._endLater) {
        this._emitEnd();
      }
    }
  };

  var S$19 = createStream('throttle', mixin$12);
  var P$15 = createProperty('throttle', mixin$12);

  function throttle(obs, wait) {
    var _ref2 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
        _ref2$leading = _ref2.leading,
        leading = _ref2$leading === undefined ? true : _ref2$leading,
        _ref2$trailing = _ref2.trailing,
        trailing = _ref2$trailing === undefined ? true : _ref2$trailing;

    return new (obs._ofSameType(S$19, P$15))(obs, { wait: wait, leading: leading, trailing: trailing });
  }

  var mixin$13 = {
    _init: function _init(_ref) {
      var _this = this;

      var wait = _ref.wait,
          immediate = _ref.immediate;

      this._wait = Math.max(0, wait);
      this._immediate = immediate;
      this._lastAttempt = 0;
      this._timeoutId = null;
      this._laterValue = null;
      this._endLater = false;
      this._$later = function () {
        return _this._later();
      };
    },
    _free: function _free() {
      this._laterValue = null;
      this._$later = null;
    },
    _handleValue: function _handleValue(x) {
      if (this._activating) {
        this._emitValue(x);
      } else {
        this._lastAttempt = now();
        if (this._immediate && !this._timeoutId) {
          this._emitValue(x);
        }
        if (!this._timeoutId) {
          this._timeoutId = setTimeout(this._$later, this._wait);
        }
        if (!this._immediate) {
          this._laterValue = x;
        }
      }
    },
    _handleEnd: function _handleEnd() {
      if (this._activating) {
        this._emitEnd();
      } else {
        if (this._timeoutId && !this._immediate) {
          this._endLater = true;
        } else {
          this._emitEnd();
        }
      }
    },
    _later: function _later() {
      var last = now() - this._lastAttempt;
      if (last < this._wait && last >= 0) {
        this._timeoutId = setTimeout(this._$later, this._wait - last);
      } else {
        this._timeoutId = null;
        if (!this._immediate) {
          this._emitValue(this._laterValue);
          this._laterValue = null;
        }
        if (this._endLater) {
          this._emitEnd();
        }
      }
    }
  };

  var S$20 = createStream('debounce', mixin$13);
  var P$16 = createProperty('debounce', mixin$13);

  function debounce(obs, wait) {
    var _ref2 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
        _ref2$immediate = _ref2.immediate,
        immediate = _ref2$immediate === undefined ? false : _ref2$immediate;

    return new (obs._ofSameType(S$20, P$16))(obs, { wait: wait, immediate: immediate });
  }

  var mixin$14 = {
    _init: function _init(_ref) {
      var fn = _ref.fn;

      this._fn = fn;
    },
    _free: function _free() {
      this._fn = null;
    },
    _handleError: function _handleError(x) {
      var fn = this._fn;
      this._emitError(fn(x));
    }
  };

  var S$21 = createStream('mapErrors', mixin$14);
  var P$17 = createProperty('mapErrors', mixin$14);

  var id$5 = function id$5(x) {
    return x;
  };

  function mapErrors(obs) {
    var fn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : id$5;

    return new (obs._ofSameType(S$21, P$17))(obs, { fn: fn });
  }

  var mixin$15 = {
    _init: function _init(_ref) {
      var fn = _ref.fn;

      this._fn = fn;
    },
    _free: function _free() {
      this._fn = null;
    },
    _handleError: function _handleError(x) {
      var fn = this._fn;
      if (fn(x)) {
        this._emitError(x);
      }
    }
  };

  var S$22 = createStream('filterErrors', mixin$15);
  var P$18 = createProperty('filterErrors', mixin$15);

  var id$6 = function id$6(x) {
    return x;
  };

  function filterErrors(obs) {
    var fn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : id$6;

    return new (obs._ofSameType(S$22, P$18))(obs, { fn: fn });
  }

  var mixin$16 = {
    _handleValue: function _handleValue() {}
  };

  var S$23 = createStream('ignoreValues', mixin$16);
  var P$19 = createProperty('ignoreValues', mixin$16);

  function ignoreValues(obs) {
    return new (obs._ofSameType(S$23, P$19))(obs);
  }

  var mixin$17 = {
    _handleError: function _handleError() {}
  };

  var S$24 = createStream('ignoreErrors', mixin$17);
  var P$20 = createProperty('ignoreErrors', mixin$17);

  function ignoreErrors(obs) {
    return new (obs._ofSameType(S$24, P$20))(obs);
  }

  var mixin$18 = {
    _handleEnd: function _handleEnd() {}
  };

  var S$25 = createStream('ignoreEnd', mixin$18);
  var P$21 = createProperty('ignoreEnd', mixin$18);

  function ignoreEnd(obs) {
    return new (obs._ofSameType(S$25, P$21))(obs);
  }

  var mixin$19 = {
    _init: function _init(_ref) {
      var fn = _ref.fn;

      this._fn = fn;
    },
    _free: function _free() {
      this._fn = null;
    },
    _handleEnd: function _handleEnd() {
      var fn = this._fn;
      this._emitValue(fn());
      this._emitEnd();
    }
  };

  var S$26 = createStream('beforeEnd', mixin$19);
  var P$22 = createProperty('beforeEnd', mixin$19);

  function beforeEnd(obs, fn) {
    return new (obs._ofSameType(S$26, P$22))(obs, { fn: fn });
  }

  var mixin$20 = {
    _init: function _init(_ref) {
      var min = _ref.min,
          max = _ref.max;

      this._max = max;
      this._min = min;
      this._buff = [];
    },
    _free: function _free() {
      this._buff = null;
    },
    _handleValue: function _handleValue(x) {
      this._buff = slide(this._buff, x, this._max);
      if (this._buff.length >= this._min) {
        this._emitValue(this._buff);
      }
    }
  };

  var S$27 = createStream('slidingWindow', mixin$20);
  var P$23 = createProperty('slidingWindow', mixin$20);

  function slidingWindow(obs, max) {
    var min = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

    return new (obs._ofSameType(S$27, P$23))(obs, { min: min, max: max });
  }

  var mixin$21 = {
    _init: function _init(_ref) {
      var fn = _ref.fn,
          flushOnEnd = _ref.flushOnEnd;

      this._fn = fn;
      this._flushOnEnd = flushOnEnd;
      this._buff = [];
    },
    _free: function _free() {
      this._buff = null;
    },
    _flush: function _flush() {
      if (this._buff !== null && this._buff.length !== 0) {
        this._emitValue(this._buff);
        this._buff = [];
      }
    },
    _handleValue: function _handleValue(x) {
      this._buff.push(x);
      var fn = this._fn;
      if (!fn(x)) {
        this._flush();
      }
    },
    _handleEnd: function _handleEnd() {
      if (this._flushOnEnd) {
        this._flush();
      }
      this._emitEnd();
    }
  };

  var S$28 = createStream('bufferWhile', mixin$21);
  var P$24 = createProperty('bufferWhile', mixin$21);

  var id$7 = function id$7(x) {
    return x;
  };

  function bufferWhile(obs, fn) {
    var _ref2 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
        _ref2$flushOnEnd = _ref2.flushOnEnd,
        flushOnEnd = _ref2$flushOnEnd === undefined ? true : _ref2$flushOnEnd;

    return new (obs._ofSameType(S$28, P$24))(obs, { fn: fn || id$7, flushOnEnd: flushOnEnd });
  }

  var mixin$22 = {
    _init: function _init(_ref) {
      var count = _ref.count,
          flushOnEnd = _ref.flushOnEnd;

      this._count = count;
      this._flushOnEnd = flushOnEnd;
      this._buff = [];
    },
    _free: function _free() {
      this._buff = null;
    },
    _flush: function _flush() {
      if (this._buff !== null && this._buff.length !== 0) {
        this._emitValue(this._buff);
        this._buff = [];
      }
    },
    _handleValue: function _handleValue(x) {
      this._buff.push(x);
      if (this._buff.length >= this._count) {
        this._flush();
      }
    },
    _handleEnd: function _handleEnd() {
      if (this._flushOnEnd) {
        this._flush();
      }
      this._emitEnd();
    }
  };

  var S$29 = createStream('bufferWithCount', mixin$22);
  var P$25 = createProperty('bufferWithCount', mixin$22);

  function bufferWhile$1(obs, count) {
    var _ref2 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
        _ref2$flushOnEnd = _ref2.flushOnEnd,
        flushOnEnd = _ref2$flushOnEnd === undefined ? true : _ref2$flushOnEnd;

    return new (obs._ofSameType(S$29, P$25))(obs, { count: count, flushOnEnd: flushOnEnd });
  }

  var mixin$23 = {
    _init: function _init(_ref) {
      var _this = this;

      var wait = _ref.wait,
          count = _ref.count,
          flushOnEnd = _ref.flushOnEnd;

      this._wait = wait;
      this._count = count;
      this._flushOnEnd = flushOnEnd;
      this._intervalId = null;
      this._$onTick = function () {
        return _this._flush();
      };
      this._buff = [];
    },
    _free: function _free() {
      this._$onTick = null;
      this._buff = null;
    },
    _flush: function _flush() {
      if (this._buff !== null) {
        this._emitValue(this._buff);
        this._buff = [];
      }
    },
    _handleValue: function _handleValue(x) {
      this._buff.push(x);
      if (this._buff.length >= this._count) {
        clearInterval(this._intervalId);
        this._flush();
        this._intervalId = setInterval(this._$onTick, this._wait);
      }
    },
    _handleEnd: function _handleEnd() {
      if (this._flushOnEnd && this._buff.length !== 0) {
        this._flush();
      }
      this._emitEnd();
    },
    _onActivation: function _onActivation() {
      this._intervalId = setInterval(this._$onTick, this._wait);
      this._source.onAny(this._$handleAny); // copied from patterns/one-source
    },
    _onDeactivation: function _onDeactivation() {
      if (this._intervalId !== null) {
        clearInterval(this._intervalId);
        this._intervalId = null;
      }
      this._source.offAny(this._$handleAny); // copied from patterns/one-source
    }
  };

  var S$30 = createStream('bufferWithTimeOrCount', mixin$23);
  var P$26 = createProperty('bufferWithTimeOrCount', mixin$23);

  function bufferWithTimeOrCount(obs, wait, count) {
    var _ref2 = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {},
        _ref2$flushOnEnd = _ref2.flushOnEnd,
        flushOnEnd = _ref2$flushOnEnd === undefined ? true : _ref2$flushOnEnd;

    return new (obs._ofSameType(S$30, P$26))(obs, { wait: wait, count: count, flushOnEnd: flushOnEnd });
  }

  function xformForObs(obs) {
    return {
      '@@transducer/step': function transducerStep(res, input) {
        obs._emitValue(input);
        return null;
      },
      '@@transducer/result': function transducerResult() {
        obs._emitEnd();
        return null;
      }
    };
  }

  var mixin$24 = {
    _init: function _init(_ref) {
      var transducer = _ref.transducer;

      this._xform = transducer(xformForObs(this));
    },
    _free: function _free() {
      this._xform = null;
    },
    _handleValue: function _handleValue(x) {
      if (this._xform['@@transducer/step'](null, x) !== null) {
        this._xform['@@transducer/result'](null);
      }
    },
    _handleEnd: function _handleEnd() {
      this._xform['@@transducer/result'](null);
    }
  };

  var S$31 = createStream('transduce', mixin$24);
  var P$27 = createProperty('transduce', mixin$24);

  function transduce(obs, transducer) {
    return new (obs._ofSameType(S$31, P$27))(obs, { transducer: transducer });
  }

  var mixin$25 = {
    _init: function _init(_ref) {
      var fn = _ref.fn;

      this._handler = fn;
      this._emitter = emitter(this);
    },
    _free: function _free() {
      this._handler = null;
      this._emitter = null;
    },
    _handleAny: function _handleAny(event) {
      this._handler(this._emitter, event);
    }
  };

  var S$32 = createStream('withHandler', mixin$25);
  var P$28 = createProperty('withHandler', mixin$25);

  function withHandler(obs, fn) {
    return new (obs._ofSameType(S$32, P$28))(obs, { fn: fn });
  }

  var isArray = Array.isArray || function (xs) {
    return Object.prototype.toString.call(xs) === '[object Array]';
  };

  function Zip(sources, combinator) {
    var _this = this;

    Stream.call(this);

    this._buffers = map(sources, function (source) {
      return isArray(source) ? cloneArray(source) : [];
    });
    this._sources = map(sources, function (source) {
      return isArray(source) ? never() : source;
    });

    this._combinator = combinator ? spread(combinator, this._sources.length) : function (x) {
      return x;
    };
    this._aliveCount = 0;

    this._$handlers = [];

    var _loop = function _loop(i) {
      _this._$handlers.push(function (event) {
        return _this._handleAny(i, event);
      });
    };

    for (var i = 0; i < this._sources.length; i++) {
      _loop(i);
    }
  }

  inherit(Zip, Stream, {
    _name: 'zip',

    _onActivation: function _onActivation() {
      // if all sources are arrays
      while (this._isFull()) {
        this._emit();
      }

      var length = this._sources.length;
      this._aliveCount = length;
      for (var i = 0; i < length && this._active; i++) {
        this._sources[i].onAny(this._$handlers[i]);
      }
    },
    _onDeactivation: function _onDeactivation() {
      for (var i = 0; i < this._sources.length; i++) {
        this._sources[i].offAny(this._$handlers[i]);
      }
    },
    _emit: function _emit() {
      var values = new Array(this._buffers.length);
      for (var i = 0; i < this._buffers.length; i++) {
        values[i] = this._buffers[i].shift();
      }
      var combinator = this._combinator;
      this._emitValue(combinator(values));
    },
    _isFull: function _isFull() {
      for (var i = 0; i < this._buffers.length; i++) {
        if (this._buffers[i].length === 0) {
          return false;
        }
      }
      return true;
    },
    _handleAny: function _handleAny(i, event) {
      if (event.type === VALUE) {
        this._buffers[i].push(event.value);
        if (this._isFull()) {
          this._emit();
        }
      }
      if (event.type === ERROR) {
        this._emitError(event.value);
      }
      if (event.type === END) {
        this._aliveCount--;
        if (this._aliveCount === 0) {
          this._emitEnd();
        }
      }
    },
    _clear: function _clear() {
      Stream.prototype._clear.call(this);
      this._sources = null;
      this._buffers = null;
      this._combinator = null;
      this._$handlers = null;
    }
  });

  function zip(observables, combinator /* Function | falsey */) {
    return observables.length === 0 ? never() : new Zip(observables, combinator);
  }

  var id$8 = function id$8(x) {
    return x;
  };

  function AbstractPool() {
    var _this = this;

    var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
        _ref$queueLim = _ref.queueLim,
        queueLim = _ref$queueLim === undefined ? 0 : _ref$queueLim,
        _ref$concurLim = _ref.concurLim,
        concurLim = _ref$concurLim === undefined ? -1 : _ref$concurLim,
        _ref$drop = _ref.drop,
        drop = _ref$drop === undefined ? 'new' : _ref$drop;

    Stream.call(this);

    this._queueLim = queueLim < 0 ? -1 : queueLim;
    this._concurLim = concurLim < 0 ? -1 : concurLim;
    this._drop = drop;
    this._queue = [];
    this._curSources = [];
    this._$handleSubAny = function (event) {
      return _this._handleSubAny(event);
    };
    this._$endHandlers = [];
    this._currentlyAdding = null;

    if (this._concurLim === 0) {
      this._emitEnd();
    }
  }

  inherit(AbstractPool, Stream, {
    _name: 'abstractPool',

    _add: function _add(obj, toObs /* Function | falsey */) {
      toObs = toObs || id$8;
      if (this._concurLim === -1 || this._curSources.length < this._concurLim) {
        this._addToCur(toObs(obj));
      } else {
        if (this._queueLim === -1 || this._queue.length < this._queueLim) {
          this._addToQueue(toObs(obj));
        } else if (this._drop === 'old') {
          this._removeOldest();
          this._add(obj, toObs);
        }
      }
    },
    _addAll: function _addAll(obss) {
      var _this2 = this;

      forEach(obss, function (obs) {
        return _this2._add(obs);
      });
    },
    _remove: function _remove(obs) {
      if (this._removeCur(obs) === -1) {
        this._removeQueue(obs);
      }
    },
    _addToQueue: function _addToQueue(obs) {
      this._queue = concat(this._queue, [obs]);
    },
    _addToCur: function _addToCur(obs) {
      if (this._active) {
        // HACK:
        //
        // We have two optimizations for cases when `obs` is ended. We don't want
        // to add such observable to the list, but only want to emit events
        // from it (if it has some).
        //
        // Instead of this hacks, we could just did following,
        // but it would be 5-8 times slower:
        //
        //     this._curSources = concat(this._curSources, [obs]);
        //     this._subscribe(obs);
        //

        // #1
        // This one for cases when `obs` already ended
        // e.g., Kefir.constant() or Kefir.never()
        if (!obs._alive) {
          if (obs._currentEvent) {
            this._emit(obs._currentEvent.type, obs._currentEvent.value);
          }
          return;
        }

        // #2
        // This one is for cases when `obs` going to end synchronously on
        // first subscriber e.g., Kefir.stream(em => {em.emit(1); em.end()})
        this._currentlyAdding = obs;
        obs.onAny(this._$handleSubAny);
        this._currentlyAdding = null;
        if (obs._alive) {
          this._curSources = concat(this._curSources, [obs]);
          if (this._active) {
            this._subToEnd(obs);
          }
        }
      } else {
        this._curSources = concat(this._curSources, [obs]);
      }
    },
    _subToEnd: function _subToEnd(obs) {
      var _this3 = this;

      var onEnd = function onEnd() {
        return _this3._removeCur(obs);
      };
      this._$endHandlers.push({ obs: obs, handler: onEnd });
      obs.onEnd(onEnd);
    },
    _subscribe: function _subscribe(obs) {
      obs.onAny(this._$handleSubAny);

      // it can become inactive in responce of subscribing to `obs.onAny` above
      if (this._active) {
        this._subToEnd(obs);
      }
    },
    _unsubscribe: function _unsubscribe(obs) {
      obs.offAny(this._$handleSubAny);

      var onEndI = findByPred(this._$endHandlers, function (obj) {
        return obj.obs === obs;
      });
      if (onEndI !== -1) {
        obs.offEnd(this._$endHandlers[onEndI].handler);
        this._$endHandlers.splice(onEndI, 1);
      }
    },
    _handleSubAny: function _handleSubAny(event) {
      if (event.type === VALUE) {
        this._emitValue(event.value);
      } else if (event.type === ERROR) {
        this._emitError(event.value);
      }
    },
    _removeQueue: function _removeQueue(obs) {
      var index = find(this._queue, obs);
      this._queue = _remove(this._queue, index);
      return index;
    },
    _removeCur: function _removeCur(obs) {
      if (this._active) {
        this._unsubscribe(obs);
      }
      var index = find(this._curSources, obs);
      this._curSources = _remove(this._curSources, index);
      if (index !== -1) {
        if (this._queue.length !== 0) {
          this._pullQueue();
        } else if (this._curSources.length === 0) {
          this._onEmpty();
        }
      }
      return index;
    },
    _removeOldest: function _removeOldest() {
      this._removeCur(this._curSources[0]);
    },
    _pullQueue: function _pullQueue() {
      if (this._queue.length !== 0) {
        this._queue = cloneArray(this._queue);
        this._addToCur(this._queue.shift());
      }
    },
    _onActivation: function _onActivation() {
      for (var i = 0, sources = this._curSources; i < sources.length && this._active; i++) {
        this._subscribe(sources[i]);
      }
    },
    _onDeactivation: function _onDeactivation() {
      for (var i = 0, sources = this._curSources; i < sources.length; i++) {
        this._unsubscribe(sources[i]);
      }
      if (this._currentlyAdding !== null) {
        this._unsubscribe(this._currentlyAdding);
      }
    },
    _isEmpty: function _isEmpty() {
      return this._curSources.length === 0;
    },
    _onEmpty: function _onEmpty() {},
    _clear: function _clear() {
      Stream.prototype._clear.call(this);
      this._queue = null;
      this._curSources = null;
      this._$handleSubAny = null;
      this._$endHandlers = null;
    }
  });

  function Merge(sources) {
    AbstractPool.call(this);
    this._addAll(sources);
    this._initialised = true;
  }

  inherit(Merge, AbstractPool, {
    _name: 'merge',

    _onEmpty: function _onEmpty() {
      if (this._initialised) {
        this._emitEnd();
      }
    }
  });

  function merge(observables) {
    return observables.length === 0 ? never() : new Merge(observables);
  }

  function S$33(generator) {
    var _this = this;

    Stream.call(this);
    this._generator = generator;
    this._source = null;
    this._inLoop = false;
    this._iteration = 0;
    this._$handleAny = function (event) {
      return _this._handleAny(event);
    };
  }

  inherit(S$33, Stream, {
    _name: 'repeat',

    _handleAny: function _handleAny(event) {
      if (event.type === END) {
        this._source = null;
        this._getSource();
      } else {
        this._emit(event.type, event.value);
      }
    },
    _getSource: function _getSource() {
      if (!this._inLoop) {
        this._inLoop = true;
        var generator = this._generator;
        while (this._source === null && this._alive && this._active) {
          this._source = generator(this._iteration++);
          if (this._source) {
            this._source.onAny(this._$handleAny);
          } else {
            this._emitEnd();
          }
        }
        this._inLoop = false;
      }
    },
    _onActivation: function _onActivation() {
      if (this._source) {
        this._source.onAny(this._$handleAny);
      } else {
        this._getSource();
      }
    },
    _onDeactivation: function _onDeactivation() {
      if (this._source) {
        this._source.offAny(this._$handleAny);
      }
    },
    _clear: function _clear() {
      Stream.prototype._clear.call(this);
      this._generator = null;
      this._source = null;
      this._$handleAny = null;
    }
  });

  var repeat = function repeat(generator) {
    return new S$33(generator);
  };

  function concat$1(observables) {
    return repeat(function (index) {
      return observables.length > index ? observables[index] : false;
    }).setName('concat');
  }

  function Pool() {
    AbstractPool.call(this);
  }

  inherit(Pool, AbstractPool, {
    _name: 'pool',

    plug: function plug(obs) {
      this._add(obs);
      return this;
    },
    unplug: function unplug(obs) {
      this._remove(obs);
      return this;
    }
  });

  function FlatMap(source, fn, options) {
    var _this = this;

    AbstractPool.call(this, options);
    this._source = source;
    this._fn = fn;
    this._mainEnded = false;
    this._lastCurrent = null;
    this._$handleMain = function (event) {
      return _this._handleMain(event);
    };
  }

  inherit(FlatMap, AbstractPool, {
    _onActivation: function _onActivation() {
      AbstractPool.prototype._onActivation.call(this);
      if (this._active) {
        this._source.onAny(this._$handleMain);
      }
    },
    _onDeactivation: function _onDeactivation() {
      AbstractPool.prototype._onDeactivation.call(this);
      this._source.offAny(this._$handleMain);
      this._hadNoEvSinceDeact = true;
    },
    _handleMain: function _handleMain(event) {
      if (event.type === VALUE) {
        // Is latest value before deactivation survived, and now is 'current' on this activation?
        // We don't want to handle such values, to prevent to constantly add
        // same observale on each activation/deactivation when our main source
        // is a `Kefir.conatant()` for example.
        var sameCurr = this._activating && this._hadNoEvSinceDeact && this._lastCurrent === event.value;
        if (!sameCurr) {
          this._add(event.value, this._fn);
        }
        this._lastCurrent = event.value;
        this._hadNoEvSinceDeact = false;
      }

      if (event.type === ERROR) {
        this._emitError(event.value);
      }

      if (event.type === END) {
        if (this._isEmpty()) {
          this._emitEnd();
        } else {
          this._mainEnded = true;
        }
      }
    },
    _onEmpty: function _onEmpty() {
      if (this._mainEnded) {
        this._emitEnd();
      }
    },
    _clear: function _clear() {
      AbstractPool.prototype._clear.call(this);
      this._source = null;
      this._lastCurrent = null;
      this._$handleMain = null;
    }
  });

  function FlatMapErrors(source, fn) {
    FlatMap.call(this, source, fn);
  }

  inherit(FlatMapErrors, FlatMap, {
    // Same as in FlatMap, only VALUE/ERROR flipped
    _handleMain: function _handleMain(event) {
      if (event.type === ERROR) {
        var sameCurr = this._activating && this._hadNoEvSinceDeact && this._lastCurrent === event.value;
        if (!sameCurr) {
          this._add(event.value, this._fn);
        }
        this._lastCurrent = event.value;
        this._hadNoEvSinceDeact = false;
      }

      if (event.type === VALUE) {
        this._emitValue(event.value);
      }

      if (event.type === END) {
        if (this._isEmpty()) {
          this._emitEnd();
        } else {
          this._mainEnded = true;
        }
      }
    }
  });

  function createConstructor$1(BaseClass, name) {
    return function AnonymousObservable(primary, secondary, options) {
      var _this = this;

      BaseClass.call(this);
      this._primary = primary;
      this._secondary = secondary;
      this._name = primary._name + '.' + name;
      this._lastSecondary = NOTHING;
      this._$handleSecondaryAny = function (event) {
        return _this._handleSecondaryAny(event);
      };
      this._$handlePrimaryAny = function (event) {
        return _this._handlePrimaryAny(event);
      };
      this._init(options);
    };
  }

  function createClassMethods$1(BaseClass) {
    return {
      _init: function _init() {},
      _free: function _free() {},
      _handlePrimaryValue: function _handlePrimaryValue(x) {
        this._emitValue(x);
      },
      _handlePrimaryError: function _handlePrimaryError(x) {
        this._emitError(x);
      },
      _handlePrimaryEnd: function _handlePrimaryEnd() {
        this._emitEnd();
      },
      _handleSecondaryValue: function _handleSecondaryValue(x) {
        this._lastSecondary = x;
      },
      _handleSecondaryError: function _handleSecondaryError(x) {
        this._emitError(x);
      },
      _handleSecondaryEnd: function _handleSecondaryEnd() {},
      _handlePrimaryAny: function _handlePrimaryAny(event) {
        switch (event.type) {
          case VALUE:
            return this._handlePrimaryValue(event.value);
          case ERROR:
            return this._handlePrimaryError(event.value);
          case END:
            return this._handlePrimaryEnd(event.value);
        }
      },
      _handleSecondaryAny: function _handleSecondaryAny(event) {
        switch (event.type) {
          case VALUE:
            return this._handleSecondaryValue(event.value);
          case ERROR:
            return this._handleSecondaryError(event.value);
          case END:
            this._handleSecondaryEnd(event.value);
            this._removeSecondary();
        }
      },
      _removeSecondary: function _removeSecondary() {
        if (this._secondary !== null) {
          this._secondary.offAny(this._$handleSecondaryAny);
          this._$handleSecondaryAny = null;
          this._secondary = null;
        }
      },
      _onActivation: function _onActivation() {
        if (this._secondary !== null) {
          this._secondary.onAny(this._$handleSecondaryAny);
        }
        if (this._active) {
          this._primary.onAny(this._$handlePrimaryAny);
        }
      },
      _onDeactivation: function _onDeactivation() {
        if (this._secondary !== null) {
          this._secondary.offAny(this._$handleSecondaryAny);
        }
        this._primary.offAny(this._$handlePrimaryAny);
      },
      _clear: function _clear() {
        BaseClass.prototype._clear.call(this);
        this._primary = null;
        this._secondary = null;
        this._lastSecondary = null;
        this._$handleSecondaryAny = null;
        this._$handlePrimaryAny = null;
        this._free();
      }
    };
  }

  function createStream$1(name, mixin) {
    var S = createConstructor$1(Stream, name);
    inherit(S, Stream, createClassMethods$1(Stream), mixin);
    return S;
  }

  function createProperty$1(name, mixin) {
    var P = createConstructor$1(Property, name);
    inherit(P, Property, createClassMethods$1(Property), mixin);
    return P;
  }

  var mixin$26 = {
    _handlePrimaryValue: function _handlePrimaryValue(x) {
      if (this._lastSecondary !== NOTHING && this._lastSecondary) {
        this._emitValue(x);
      }
    },
    _handleSecondaryEnd: function _handleSecondaryEnd() {
      if (this._lastSecondary === NOTHING || !this._lastSecondary) {
        this._emitEnd();
      }
    }
  };

  var S$34 = createStream$1('filterBy', mixin$26);
  var P$29 = createProperty$1('filterBy', mixin$26);

  function filterBy(primary, secondary) {
    return new (primary._ofSameType(S$34, P$29))(primary, secondary);
  }

  var id2 = function id2(_, x) {
    return x;
  };

  function sampledBy(passive, active, combinator) {
    var _combinator = combinator ? function (a, b) {
      return combinator(b, a);
    } : id2;
    return combine([active], [passive], _combinator).setName(passive, 'sampledBy');
  }

  var mixin$27 = {
    _handlePrimaryValue: function _handlePrimaryValue(x) {
      if (this._lastSecondary !== NOTHING) {
        this._emitValue(x);
      }
    },
    _handleSecondaryEnd: function _handleSecondaryEnd() {
      if (this._lastSecondary === NOTHING) {
        this._emitEnd();
      }
    }
  };

  var S$35 = createStream$1('skipUntilBy', mixin$27);
  var P$30 = createProperty$1('skipUntilBy', mixin$27);

  function skipUntilBy(primary, secondary) {
    return new (primary._ofSameType(S$35, P$30))(primary, secondary);
  }

  var mixin$28 = {
    _handleSecondaryValue: function _handleSecondaryValue() {
      this._emitEnd();
    }
  };

  var S$36 = createStream$1('takeUntilBy', mixin$28);
  var P$31 = createProperty$1('takeUntilBy', mixin$28);

  function takeUntilBy(primary, secondary) {
    return new (primary._ofSameType(S$36, P$31))(primary, secondary);
  }

  var mixin$29 = {
    _init: function _init() {
      var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
          _ref$flushOnEnd = _ref.flushOnEnd,
          flushOnEnd = _ref$flushOnEnd === undefined ? true : _ref$flushOnEnd;

      this._buff = [];
      this._flushOnEnd = flushOnEnd;
    },
    _free: function _free() {
      this._buff = null;
    },
    _flush: function _flush() {
      if (this._buff !== null) {
        this._emitValue(this._buff);
        this._buff = [];
      }
    },
    _handlePrimaryEnd: function _handlePrimaryEnd() {
      if (this._flushOnEnd) {
        this._flush();
      }
      this._emitEnd();
    },
    _onActivation: function _onActivation() {
      this._primary.onAny(this._$handlePrimaryAny);
      if (this._alive && this._secondary !== null) {
        this._secondary.onAny(this._$handleSecondaryAny);
      }
    },
    _handlePrimaryValue: function _handlePrimaryValue(x) {
      this._buff.push(x);
    },
    _handleSecondaryValue: function _handleSecondaryValue() {
      this._flush();
    },
    _handleSecondaryEnd: function _handleSecondaryEnd() {
      if (!this._flushOnEnd) {
        this._emitEnd();
      }
    }
  };

  var S$37 = createStream$1('bufferBy', mixin$29);
  var P$32 = createProperty$1('bufferBy', mixin$29);

  function bufferBy(primary, secondary, options /* optional */) {
    return new (primary._ofSameType(S$37, P$32))(primary, secondary, options);
  }

  var mixin$30 = {
    _init: function _init() {
      var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
          _ref$flushOnEnd = _ref.flushOnEnd,
          flushOnEnd = _ref$flushOnEnd === undefined ? true : _ref$flushOnEnd,
          _ref$flushOnChange = _ref.flushOnChange,
          flushOnChange = _ref$flushOnChange === undefined ? false : _ref$flushOnChange;

      this._buff = [];
      this._flushOnEnd = flushOnEnd;
      this._flushOnChange = flushOnChange;
    },
    _free: function _free() {
      this._buff = null;
    },
    _flush: function _flush() {
      if (this._buff !== null) {
        this._emitValue(this._buff);
        this._buff = [];
      }
    },
    _handlePrimaryEnd: function _handlePrimaryEnd() {
      if (this._flushOnEnd) {
        this._flush();
      }
      this._emitEnd();
    },
    _handlePrimaryValue: function _handlePrimaryValue(x) {
      this._buff.push(x);
      if (this._lastSecondary !== NOTHING && !this._lastSecondary) {
        this._flush();
      }
    },
    _handleSecondaryEnd: function _handleSecondaryEnd() {
      if (!this._flushOnEnd && (this._lastSecondary === NOTHING || this._lastSecondary)) {
        this._emitEnd();
      }
    },
    _handleSecondaryValue: function _handleSecondaryValue(x) {
      if (this._flushOnChange && !x) {
        this._flush();
      }

      // from default _handleSecondaryValue
      this._lastSecondary = x;
    }
  };

  var S$38 = createStream$1('bufferWhileBy', mixin$30);
  var P$33 = createProperty$1('bufferWhileBy', mixin$30);

  function bufferWhileBy(primary, secondary, options /* optional */) {
    return new (primary._ofSameType(S$38, P$33))(primary, secondary, options);
  }

  var f = function f() {
    return false;
  };
  var t = function t() {
    return true;
  };

  function awaiting(a, b) {
    var result = merge([map$1(a, t), map$1(b, f)]);
    result = skipDuplicates(result);
    result = toProperty(result, f);
    return result.setName(a, 'awaiting');
  }

  var mixin$31 = {
    _init: function _init(_ref) {
      var fn = _ref.fn;

      this._fn = fn;
    },
    _free: function _free() {
      this._fn = null;
    },
    _handleValue: function _handleValue(x) {
      var fn = this._fn;
      var result = fn(x);
      if (result.convert) {
        this._emitError(result.error);
      } else {
        this._emitValue(x);
      }
    }
  };

  var S$39 = createStream('valuesToErrors', mixin$31);
  var P$34 = createProperty('valuesToErrors', mixin$31);

  var defFn = function defFn(x) {
    return { convert: true, error: x };
  };

  function valuesToErrors(obs) {
    var fn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defFn;

    return new (obs._ofSameType(S$39, P$34))(obs, { fn: fn });
  }

  var mixin$32 = {
    _init: function _init(_ref) {
      var fn = _ref.fn;

      this._fn = fn;
    },
    _free: function _free() {
      this._fn = null;
    },
    _handleError: function _handleError(x) {
      var fn = this._fn;
      var result = fn(x);
      if (result.convert) {
        this._emitValue(result.value);
      } else {
        this._emitError(x);
      }
    }
  };

  var S$40 = createStream('errorsToValues', mixin$32);
  var P$35 = createProperty('errorsToValues', mixin$32);

  var defFn$1 = function defFn$1(x) {
    return { convert: true, value: x };
  };

  function errorsToValues(obs) {
    var fn = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defFn$1;

    return new (obs._ofSameType(S$40, P$35))(obs, { fn: fn });
  }

  var mixin$33 = {
    _handleError: function _handleError(x) {
      this._emitError(x);
      this._emitEnd();
    }
  };

  var S$41 = createStream('endOnError', mixin$33);
  var P$36 = createProperty('endOnError', mixin$33);

  function endOnError(obs) {
    return new (obs._ofSameType(S$41, P$36))(obs);
  }

  // Create a stream
  // -----------------------------------------------------------------------------

  // () -> Stream
  // (number, any) -> Stream
  // (number, any) -> Stream
  // (number, Array<any>) -> Stream
  // (number, Function) -> Stream
  // (number, Function) -> Stream
  // (Function) -> Stream
  // (Function) -> Stream
  // Target = {addEventListener, removeEventListener}|{addListener, removeListener}|{on, off}
  // (Target, string, Function|undefined) -> Stream
  // (Function) -> Stream
  // Create a property
  // -----------------------------------------------------------------------------

  // (any) -> Property
  // (any) -> Property
  // Convert observables
  // -----------------------------------------------------------------------------

  // (Stream|Property, Function|undefined) -> Property
  Observable.prototype.toProperty = function (fn) {
    return toProperty(this, fn);
  };

  // (Stream|Property) -> Stream
  Observable.prototype.changes = function () {
    return changes(this);
  };

  // Interoperation with other implimentations
  // -----------------------------------------------------------------------------

  // (Promise) -> Property
  // (Stream|Property, Function|undefined) -> Promise
  Observable.prototype.toPromise = function (Promise) {
    return toPromise(this, Promise);
  };

  // (ESObservable) -> Stream
  // (Stream|Property) -> ES7 Observable
  Observable.prototype.toESObservable = toESObservable;
  Observable.prototype[$$observable] = toESObservable;

  // Modify an observable
  // -----------------------------------------------------------------------------

  // (Stream, Function|undefined) -> Stream
  // (Property, Function|undefined) -> Property
  Observable.prototype.map = function (fn) {
    return map$1(this, fn);
  };

  // (Stream, Function|undefined) -> Stream
  // (Property, Function|undefined) -> Property
  Observable.prototype.filter = function (fn) {
    return filter(this, fn);
  };

  // (Stream, number) -> Stream
  // (Property, number) -> Property
  Observable.prototype.take = function (n) {
    return take(this, n);
  };

  // (Stream, number) -> Stream
  // (Property, number) -> Property
  Observable.prototype.takeErrors = function (n) {
    return takeErrors(this, n);
  };

  // (Stream, Function|undefined) -> Stream
  // (Property, Function|undefined) -> Property
  Observable.prototype.takeWhile = function (fn) {
    return takeWhile(this, fn);
  };

  // (Stream) -> Stream
  // (Property) -> Property
  Observable.prototype.last = function () {
    return last(this);
  };

  // (Stream, number) -> Stream
  // (Property, number) -> Property
  Observable.prototype.skip = function (n) {
    return skip(this, n);
  };

  // (Stream, Function|undefined) -> Stream
  // (Property, Function|undefined) -> Property
  Observable.prototype.skipWhile = function (fn) {
    return skipWhile(this, fn);
  };

  // (Stream, Function|undefined) -> Stream
  // (Property, Function|undefined) -> Property
  Observable.prototype.skipDuplicates = function (fn) {
    return skipDuplicates(this, fn);
  };

  // (Stream, Function|falsey, any|undefined) -> Stream
  // (Property, Function|falsey, any|undefined) -> Property
  Observable.prototype.diff = function (fn, seed) {
    return diff(this, fn, seed);
  };

  // (Stream|Property, Function, any|undefined) -> Property
  Observable.prototype.scan = function (fn, seed) {
    return scan(this, fn, seed);
  };

  // (Stream, Function|undefined) -> Stream
  // (Property, Function|undefined) -> Property
  Observable.prototype.flatten = function (fn) {
    return flatten(this, fn);
  };

  // (Stream, number) -> Stream
  // (Property, number) -> Property
  Observable.prototype.delay = function (wait) {
    return delay(this, wait);
  };

  // Options = {leading: boolean|undefined, trailing: boolean|undefined}
  // (Stream, number, Options|undefined) -> Stream
  // (Property, number, Options|undefined) -> Property
  Observable.prototype.throttle = function (wait, options) {
    return throttle(this, wait, options);
  };

  // Options = {immediate: boolean|undefined}
  // (Stream, number, Options|undefined) -> Stream
  // (Property, number, Options|undefined) -> Property
  Observable.prototype.debounce = function (wait, options) {
    return debounce(this, wait, options);
  };

  // (Stream, Function|undefined) -> Stream
  // (Property, Function|undefined) -> Property
  Observable.prototype.mapErrors = function (fn) {
    return mapErrors(this, fn);
  };

  // (Stream, Function|undefined) -> Stream
  // (Property, Function|undefined) -> Property
  Observable.prototype.filterErrors = function (fn) {
    return filterErrors(this, fn);
  };

  // (Stream) -> Stream
  // (Property) -> Property
  Observable.prototype.ignoreValues = function () {
    return ignoreValues(this);
  };

  // (Stream) -> Stream
  // (Property) -> Property
  Observable.prototype.ignoreErrors = function () {
    return ignoreErrors(this);
  };

  // (Stream) -> Stream
  // (Property) -> Property
  Observable.prototype.ignoreEnd = function () {
    return ignoreEnd(this);
  };

  // (Stream, Function) -> Stream
  // (Property, Function) -> Property
  Observable.prototype.beforeEnd = function (fn) {
    return beforeEnd(this, fn);
  };

  // (Stream, number, number|undefined) -> Stream
  // (Property, number, number|undefined) -> Property
  Observable.prototype.slidingWindow = function (max, min) {
    return slidingWindow(this, max, min);
  };

  // Options = {flushOnEnd: boolean|undefined}
  // (Stream, Function|falsey, Options|undefined) -> Stream
  // (Property, Function|falsey, Options|undefined) -> Property
  Observable.prototype.bufferWhile = function (fn, options) {
    return bufferWhile(this, fn, options);
  };

  // (Stream, number) -> Stream
  // (Property, number) -> Property
  Observable.prototype.bufferWithCount = function (count, options) {
    return bufferWhile$1(this, count, options);
  };

  // Options = {flushOnEnd: boolean|undefined}
  // (Stream, number, number, Options|undefined) -> Stream
  // (Property, number, number, Options|undefined) -> Property
  Observable.prototype.bufferWithTimeOrCount = function (wait, count, options) {
    return bufferWithTimeOrCount(this, wait, count, options);
  };

  // (Stream, Function) -> Stream
  // (Property, Function) -> Property
  Observable.prototype.transduce = function (transducer) {
    return transduce(this, transducer);
  };

  // (Stream, Function) -> Stream
  // (Property, Function) -> Property
  Observable.prototype.withHandler = function (fn) {
    return withHandler(this, fn);
  };

  // (Stream, Stream -> a) -> a
  // (Property, Property -> a) -> a
  Observable.prototype.thru = function (fn) {
    return fn(this);
  };

  // Combine observables
  // -----------------------------------------------------------------------------

  // (Array<Stream|Property>, Function|undefiend) -> Stream
  // (Array<Stream|Property>, Array<Stream|Property>, Function|undefiend) -> Stream
  Observable.prototype.combine = function (other, combinator) {
    return combine([this, other], combinator);
  };

  // (Array<Stream|Property>, Function|undefiend) -> Stream
  Observable.prototype.zip = function (other, combinator) {
    return zip([this, other], combinator);
  };

  // (Array<Stream|Property>) -> Stream
  Observable.prototype.merge = function (other) {
    return merge([this, other]);
  };

  // (Array<Stream|Property>) -> Stream
  Observable.prototype.concat = function (other) {
    return concat$1([this, other]);
  };

  // () -> Pool
  var pool = function pool() {
    return new Pool();
  };

  // (Function) -> Stream
  // Options = {concurLim: number|undefined, queueLim: number|undefined, drop: 'old'|'new'|undefiend}
  // (Stream|Property, Function|falsey, Options|undefined) -> Stream
  Observable.prototype.flatMap = function (fn) {
    return new FlatMap(this, fn).setName(this, 'flatMap');
  };
  Observable.prototype.flatMapLatest = function (fn) {
    return new FlatMap(this, fn, { concurLim: 1, drop: 'old' }).setName(this, 'flatMapLatest');
  };
  Observable.prototype.flatMapFirst = function (fn) {
    return new FlatMap(this, fn, { concurLim: 1 }).setName(this, 'flatMapFirst');
  };
  Observable.prototype.flatMapConcat = function (fn) {
    return new FlatMap(this, fn, { queueLim: -1, concurLim: 1 }).setName(this, 'flatMapConcat');
  };
  Observable.prototype.flatMapConcurLimit = function (fn, limit) {
    return new FlatMap(this, fn, { queueLim: -1, concurLim: limit }).setName(this, 'flatMapConcurLimit');
  };

  // (Stream|Property, Function|falsey) -> Stream
  Observable.prototype.flatMapErrors = function (fn) {
    return new FlatMapErrors(this, fn).setName(this, 'flatMapErrors');
  };

  // Combine two observables
  // -----------------------------------------------------------------------------

  // (Stream, Stream|Property) -> Stream
  // (Property, Stream|Property) -> Property
  Observable.prototype.filterBy = function (other) {
    return filterBy(this, other);
  };

  // (Stream, Stream|Property, Function|undefiend) -> Stream
  // (Property, Stream|Property, Function|undefiend) -> Property
  Observable.prototype.sampledBy = function (other, combinator) {
    return sampledBy(this, other, combinator);
  };

  // (Stream, Stream|Property) -> Stream
  // (Property, Stream|Property) -> Property
  Observable.prototype.skipUntilBy = function (other) {
    return skipUntilBy(this, other);
  };

  // (Stream, Stream|Property) -> Stream
  // (Property, Stream|Property) -> Property
  Observable.prototype.takeUntilBy = function (other) {
    return takeUntilBy(this, other);
  };

  // Options = {flushOnEnd: boolean|undefined}
  // (Stream, Stream|Property, Options|undefined) -> Stream
  // (Property, Stream|Property, Options|undefined) -> Property
  Observable.prototype.bufferBy = function (other, options) {
    return bufferBy(this, other, options);
  };

  // Options = {flushOnEnd: boolean|undefined}
  // (Stream, Stream|Property, Options|undefined) -> Stream
  // (Property, Stream|Property, Options|undefined) -> Property
  Observable.prototype.bufferWhileBy = function (other, options) {
    return bufferWhileBy(this, other, options);
  };

  // Deprecated
  // -----------------------------------------------------------------------------

  var DEPRECATION_WARNINGS = true;
  function dissableDeprecationWarnings() {
    DEPRECATION_WARNINGS = false;
  }

  function warn(msg) {
    if (DEPRECATION_WARNINGS && console && typeof console.warn === 'function') {
      var msg2 = '\nHere is an Error object for you containing the call stack:';
      console.warn(msg, msg2, new Error());
    }
  }

  // (Stream|Property, Stream|Property) -> Property
  Observable.prototype.awaiting = function (other) {
    warn('You are using deprecated .awaiting() method, see https://github.com/rpominov/kefir/issues/145');
    return awaiting(this, other);
  };

  // (Stream, Function|undefined) -> Stream
  // (Property, Function|undefined) -> Property
  Observable.prototype.valuesToErrors = function (fn) {
    warn('You are using deprecated .valuesToErrors() method, see https://github.com/rpominov/kefir/issues/149');
    return valuesToErrors(this, fn);
  };

  // (Stream, Function|undefined) -> Stream
  // (Property, Function|undefined) -> Property
  Observable.prototype.errorsToValues = function (fn) {
    warn('You are using deprecated .errorsToValues() method, see https://github.com/rpominov/kefir/issues/149');
    return errorsToValues(this, fn);
  };

  // (Stream) -> Stream
  // (Property) -> Property
  Observable.prototype.endOnError = function () {
    warn('You are using deprecated .endOnError() method, see https://github.com/rpominov/kefir/issues/150');
    return endOnError(this);
  };

  // Exports
  // --------------------------------------------------------------------------

  var Kefir = {
    Observable: Observable,
    Stream: Stream,
    Property: Property,
    never: never,
    later: later,
    interval: interval,
    sequentially: sequentially,
    fromPoll: fromPoll,
    withInterval: withInterval,
    fromCallback: fromCallback,
    fromNodeCallback: fromNodeCallback,
    fromEvents: fromEvents,
    stream: stream,
    constant: constant,
    constantError: constantError,
    fromPromise: fromPromise,
    fromESObservable: fromESObservable,
    combine: combine,
    zip: zip,
    merge: merge,
    concat: concat$1,
    Pool: Pool,
    pool: pool,
    repeat: repeat,
    staticLand: staticLand
  };

  Kefir.Kefir = Kefir;

  exports.dissableDeprecationWarnings = dissableDeprecationWarnings;
  exports.Kefir = Kefir;
  exports.Observable = Observable;
  exports.Stream = Stream;
  exports.Property = Property;
  exports.never = never;
  exports.later = later;
  exports.interval = interval;
  exports.sequentially = sequentially;
  exports.fromPoll = fromPoll;
  exports.withInterval = withInterval;
  exports.fromCallback = fromCallback;
  exports.fromNodeCallback = fromNodeCallback;
  exports.fromEvents = fromEvents;
  exports.stream = stream;
  exports.constant = constant;
  exports.constantError = constantError;
  exports.fromPromise = fromPromise;
  exports.fromESObservable = fromESObservable;
  exports.combine = combine;
  exports.zip = zip;
  exports.merge = merge;
  exports.concat = concat$1;
  exports.Pool = Pool;
  exports.pool = pool;
  exports.repeat = repeat;
  exports.staticLand = staticLand;
  exports['default'] = Kefir;

  Object.defineProperty(exports, '__esModule', { value: true });
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(107)))

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SideContainer = exports.SideContainer = function (_React$Component) {
	_inherits(SideContainer, _React$Component);

	function SideContainer() {
		_classCallCheck(this, SideContainer);

		return _possibleConstructorReturn(this, (SideContainer.__proto__ || Object.getPrototypeOf(SideContainer)).apply(this, arguments));
	}

	_createClass(SideContainer, [{
		key: 'render',
		value: function render() {
			return React.createElement(
				'div',
				{ style: Object.assign({}, this.props.style, { padding: '0 1rem 0 1rem' }) },
				this.props.children
			);
		}
	}]);

	return SideContainer;
}(React.Component);

/***/ }),
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(55);


/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _searchApplication = __webpack_require__(56);

var _searchApplication2 = _interopRequireDefault(_searchApplication);

var _settingsApplication = __webpack_require__(110);

var _settingsApplication2 = _interopRequireDefault(_settingsApplication);

var _globalData = __webpack_require__(111);

var _globalData2 = _interopRequireDefault(_globalData);

var _topBar = __webpack_require__(112);

var _topBar2 = _interopRequireDefault(_topBar);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

window.__global = new _globalData2.default();

var totalHashListeners = 0;
var hashListeners = {
	late: [],
	normal: []
};
var contentHashes = {
	default: function _default(hash) {
		return _react2.default.createElement(_searchApplication2.default, null);
	},
	settings: function settings(hash) {
		return _react2.default.createElement(_settingsApplication2.default, { menu: hash.indexOf('-') != -1 ? hash.substring(hash.indexOf('-') + 1) : 'account' });
	}
};

var onHashChange = function onHashChange() {
	var hash = location.hash.substring(1);
	var indexOfDash = hash.indexOf('-');
	var command = indexOfDash == -1 ? hash : hash.substring(0, indexOfDash);

	for (var i = 0; i < hashListeners.normal.length; i++) {
		var l = hashListeners.normal[i];
		if (typeof l === 'function') {
			try {
				l();
			} catch (e) {
				console.error(e);
			}
		}
	}

	if (command in contentHashes) {
		ReactDOM.render(contentHashes[command](hash), document.getElementById('content-react'));
	} else {
		ReactDOM.render(contentHashes.default(hash), document.getElementById('content-react'));
	}

	for (var i = 0; i < hashListeners.late.length; i++) {
		var _l = hashListeners.late[i];
		if (typeof _l === 'function') {
			try {
				_l();
			} catch (e) {
				console.error(e);
			}
		}
	}
};

window.addEventListener("hashchange", onHashChange, false);

/**
 * Adds an onHash change listener for Search App.
 *
 * @param  {function} listener listener to be execute on hash change
 * @param  {bool} 	  late     If truthy the listener will be executed after ReactDOM render, else otherwise
 */
window.addOnApplicationHashChange = function (listener, late) {
	totalHashListeners++;

	if (totalHashListeners > 19) {
		console.warn("There is a total of " + totalHashListeners + " hash listeners added right now.");
	}

	hashListeners[!late ? 'normal' : 'late'].push(listener);
};

/**
 * Removes an onHash change listener for Search App.
 *
 * @param  {function} listener listener that was executed on hash change
 * @param  {bool} 	  late     If truthy a late onHash listener will be removed, else otherwise
 */
window.removeOnApplicationHashChange = function (listener, late) {
	var listeners = hashListeners[!late ? 'normal' : 'late'];

	for (var i = 0; i < listeners.length; i++) {
		var l = listeners[i];

		if (l == listener) {
			totalHashListeners--;
			return hashListeners[!late ? 'normal' : 'late'].splice(i, 1);
		}
	}
	return false;
};

onHashChange();

ReactDOM.render(_react2.default.createElement(_topBar2.default, null), document.getElementById('top-bar-react'));

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _reactSmoothCollapse = __webpack_require__(57);

var _reactSmoothCollapse2 = _interopRequireDefault(_reactSmoothCollapse);

var _generic = __webpack_require__(49);

var _iconAudioPlayer = __webpack_require__(16);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SearchApplication = function (_React$Component) {
	_inherits(SearchApplication, _React$Component);

	function SearchApplication(props) {
		_classCallCheck(this, SearchApplication);

		var _this = _possibleConstructorReturn(this, (SearchApplication.__proto__ || Object.getPrototypeOf(SearchApplication)).call(this, props));

		var a_client = algoliasearch("RCKCJVSL87", "4114df211bc103af5170f711da975ef7");

		_this.state = {
			opacity: 0,
			is_loading: true,
			account_info: null,
			selected_music_result: false,

			search_value: '',
			search_database: null,
			search_results: []
		};

		_this.account_info_request = null;
		_this.algolia_index = a_client.initIndex('dev_library');

		_this.openMusicResult = _this.openMusicResult.bind(_this);
		_this.isLoadingUpdate = _this.isLoadingUpdate.bind(_this);
		_this.onSearchValueChange = _this.onSearchValueChange.bind(_this);
		return _this;
	}

	_createClass(SearchApplication, [{
		key: 'openMusicResult',
		value: function openMusicResult(event, music) {
			this.setState({
				selected_music_result: music
			});
		}
	}, {
		key: 'isLoadingUpdate',
		value: function isLoadingUpdate() {
			var opacity = this.state.opacity;
			var is_loading = this.account_info_request && this.search_database_request;

			if (!is_loading) {
				opacity = 1;
			}

			this.setState({
				is_loading: is_loading,
				opacity: opacity
			});
		}
	}, {
		key: 'onSearchValueChange',
		value: function onSearchValueChange(event) {
			var _this2 = this;

			var query = event.target.value.toLowerCase();
			var search_results = [];

			if (query.length) {
				this.setState({
					is_loading: true
				});

				this.algolia_index.search(query).then(function (data) {
					_this2.setState({
						search_results: data.hits,
						is_loading: false
					});
				});
			} else this.setState({
				search_results: []
			});

			this.setState({
				search_value: event.target.value
			});
		}
	}, {
		key: 'componentWillMount',
		value: function componentWillMount() {
			// this.search_database_request = $.get('/account/search_database', (data) => {
			// 	this.setState({
			// 		search_database: data.data
			// 	})
			// },'json').always(() => {
			// 	this.search_database_request = null;
			this.isLoadingUpdate();
			// })
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {}
	}, {
		key: 'render',
		value: function render() {
			var _this3 = this;

			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(
					'div',
					{ className: 'search-app-container', style: { opacity: this.state.opacity } },
					_react2.default.createElement(
						_generic.SideContainer,
						null,
						_react2.default.createElement('input', { value: this.state.search_value, style: {
								fontSize: '1.4rem',
								border: 'none',
								outline: 'none',
								width: 'calc(100% - 1.4rem)'
							}, placeholder: 'Search...', onChange: this.onSearchValueChange }),
						_react2.default.createElement('i', { style: { fontSize: '1.4rem', color: '#F05F40' }, className: 'fa ' + (this.state.is_loading ? 'fa-circle-o-notch fa-spin' : 'fa-search'), 'aria-hidden': 'true' })
					),
					_react2.default.createElement('hr', null),
					_react2.default.createElement(
						_generic.SideContainer,
						{ style: { display: 'none' } },
						_react2.default.createElement(Checkbox, { label: 'Long (> 3 mins.)' }),
						_react2.default.createElement(Checkbox, { label: 'Mid (< 3 mins.)' }),
						_react2.default.createElement(Checkbox, { label: 'Short (< 1 mins.)' })
					),
					_react2.default.createElement(
						_reactSmoothCollapse2.default,
						{ expanded: !!this.state.search_results && !!this.state.search_results.length || !!this.state.selected_music_result },
						this.state.selected_music_result ? _react2.default.createElement(BigMusicResult, { music: this.state.selected_music_result, onBackClick: function onBackClick() {
								_this3.setState({ selected_music_result: null });
							} }) : _react2.default.createElement(
							_generic.SideContainer,
							null,
							_react2.default.createElement('br', null),
							this.state.search_results.map(function (search_result) {
								return _react2.default.createElement(
									'div',
									{ key: search_result.id },
									_react2.default.createElement(MusicResult, { music: search_result, onResultClick: function onResultClick(event) {
											_this3.openMusicResult(event, search_result);
										} })
								);
							})
						)
					),
					!!this.state.selected_music_result ? null : _react2.default.createElement(
						_generic.SideContainer,
						{ style: { marginTop: '0.5rem' } },
						_react2.default.createElement(
							'a',
							{ href: 'https://www.algolia.com/' },
							_react2.default.createElement('img', { className: 'pull-right', src: 'https://www.algolia.com/static_assets/images/press/downloads/search-by-algolia.svg', style: { height: '1rem' } })
						)
					)
				)
			);
		}
	}]);

	return SearchApplication;
}(_react2.default.Component);

exports.default = SearchApplication;

var ClearFloats = function (_React$Component2) {
	_inherits(ClearFloats, _React$Component2);

	function ClearFloats() {
		_classCallCheck(this, ClearFloats);

		return _possibleConstructorReturn(this, (ClearFloats.__proto__ || Object.getPrototypeOf(ClearFloats)).apply(this, arguments));
	}

	_createClass(ClearFloats, [{
		key: 'render',
		value: function render() {
			return _react2.default.createElement('div', { style: {
					content: "",
					display: 'table',
					clear: 'both' } });
		}
	}]);

	return ClearFloats;
}(_react2.default.Component);

var BigMusicResult = function (_React$Component3) {
	_inherits(BigMusicResult, _React$Component3);

	function BigMusicResult() {
		_classCallCheck(this, BigMusicResult);

		return _possibleConstructorReturn(this, (BigMusicResult.__proto__ || Object.getPrototypeOf(BigMusicResult)).apply(this, arguments));
	}

	_createClass(BigMusicResult, [{
		key: 'render',
		value: function render() {
			var music = this.props.music || {};

			var date = new Date(null);
			date.setSeconds(music.time_length);
			var lengthString = date.toISOString().substr(14, 5);

			return _react2.default.createElement(
				_generic.SideContainer,
				null,
				_react2.default.createElement(
					'div',
					{ onClick: this.props.onBackClick, 'aria-hidden': 'true', style: { width: "4rem", cursor: "pointer", margin: '0.5rem 0 0.5rem 0' } },
					_react2.default.createElement('i', { className: 'fa fa-chevron-left fa-2x orange' }),
					_react2.default.createElement(
						'span',
						{ className: 'hidden-small-inline', style: {
								position: 'absolute',
								marginTop: '0.25rem',
								marginLeft: '0.25rem'
							} },
						'Back'
					)
				),
				_react2.default.createElement(
					'div',
					null,
					music.cover_photo ? _react2.default.createElement('img', { style: { verticalAlign: 'middle', display: 'inline-block', width: '4em', height: '4em', borderRadius: '4px' }, src: music.cover_photo }) : _react2.default.createElement('i', { className: 'fa fa-music fa-2x', style: { verticalAlign: 'middle', padding: '16px', width: '1em', color: 'gray', background: '#eaeaea', borderRadius: '4px' } }),
					_react2.default.createElement(
						'span',
						{ onClick: this.props.onResultClick, style: { paddingLeft: '1rem' } },
						music.name || _react2.default.createElement(
							'em',
							null,
							'Untitled Track'
						)
					),
					_react2.default.createElement(
						'span',
						{ className: 'pull-right', style: { marginTop: '1.1rem' } },
						lengthString
					)
				),
				_react2.default.createElement(AudioWithSeek, { src: music.src }),
				_react2.default.createElement(
					'b',
					null,
					'Description:'
				),
				_react2.default.createElement(
					'p',
					null,
					music.description || _react2.default.createElement(
						'em',
						null,
						'No description'
					)
				),
				_react2.default.createElement(
					'p',
					{ className: 'pull-right' },
					'Posted by: ',
					music.owner.profile_picture,
					' ',
					music.owner.display_name
				),
				_react2.default.createElement(ClearFloats, null),
				_react2.default.createElement(
					'b',
					null,
					'Tags:'
				),
				_react2.default.createElement(
					'p',
					null,
					music.tags && music.tags.length ? music.tags.map(function (tag, index) {
						return _react2.default.createElement(
							'span',
							{ key: index, className: 'badge' },
							tag
						);
					}) : _react2.default.createElement(
						'em',
						null,
						'No tags'
					)
				)
			);
		}
	}]);

	return BigMusicResult;
}(_react2.default.Component);

var MusicResult = function (_React$Component4) {
	_inherits(MusicResult, _React$Component4);

	function MusicResult() {
		_classCallCheck(this, MusicResult);

		return _possibleConstructorReturn(this, (MusicResult.__proto__ || Object.getPrototypeOf(MusicResult)).apply(this, arguments));
	}

	_createClass(MusicResult, [{
		key: 'render',
		value: function render() {
			var music = this.props.music || {};

			var date = new Date(null);
			date.setSeconds(music.time_length);
			var lengthString = date.toISOString().substr(14, 5);

			return _react2.default.createElement(
				'div',
				null,
				music.cover_photo ? _react2.default.createElement('img', { style: { verticalAlign: 'middle', display: 'inline-block', width: '32px', height: '32px', borderRadius: '4px' }, src: music.cover_photo }) : _react2.default.createElement('i', { onClick: this.props.onResultClick, className: 'fa fa-music', style: { verticalAlign: 'middle', padding: '8px', color: 'gray', background: '#eaeaea', borderRadius: '4px', width: '16px' } }),
				_react2.default.createElement(
					'div',
					{ style: { display: 'inline' } },
					_react2.default.createElement(
						'a',
						{ href: 'javascript:;', onClick: this.props.onResultClick, style: { paddingLeft: '0.6rem' } },
						music.name || _react2.default.createElement(
							'em',
							null,
							'Untitled Track'
						)
					),
					_react2.default.createElement(
						'span',
						{ className: 'pull-right', style: { marginTop: '0.5rem' } },
						lengthString
					)
				),
				_react2.default.createElement(AudioWithSeek, { src: music.src, use_global_player: true })
			);
		}
	}]);

	return MusicResult;
}(_react2.default.Component);

var AudioWithSeek = function (_React$Component5) {
	_inherits(AudioWithSeek, _React$Component5);

	function AudioWithSeek(props) {
		_classCallCheck(this, AudioWithSeek);

		var _this7 = _possibleConstructorReturn(this, (AudioWithSeek.__proto__ || Object.getPrototypeOf(AudioWithSeek)).call(this, props));

		_this7.onSeekBarClick = _this7.onSeekBarClick.bind(_this7);
		_this7.onAudioProgress = _this7.onAudioProgress.bind(_this7);
		_this7.onAudioDurationChange = _this7.onAudioDurationChange.bind(_this7);
		_this7.onAudioTimeUpdate = _this7.onAudioTimeUpdate.bind(_this7);
		_this7.onSeekMouseDown = _this7.onSeekMouseDown.bind(_this7);
		_this7.onSeekMouseUp = _this7.onSeekMouseUp.bind(_this7);
		_this7.onSeekMouseMove = _this7.onSeekMouseMove.bind(_this7);

		_this7.state = {
			audio_track_length: 0,
			audio_current_time: 0,
			buffered_progress: 0
		};
		return _this7;
	}

	_createClass(AudioWithSeek, [{
		key: 'onSeekMouseDown',
		value: function onSeekMouseDown(event) {
			this.setState({
				is_mouse_held: true
			});
		}
	}, {
		key: 'onSeekMouseUp',
		value: function onSeekMouseUp(event) {
			this.setState({
				is_mouse_held: false
			});
		}
	}, {
		key: 'onSeekMouseMove',
		value: function onSeekMouseMove(event) {
			if (this.state.is_mouse_held && event.which == 1) {
				this.onSeekBarClick(event);
			}
		}
	}, {
		key: 'onSeekBarClick',
		value: function onSeekBarClick(event) {
			var _this8 = this;

			if (!this._seekbar) {
				return;
			}

			var t = this._seekbar;
			var rect = t.getBoundingClientRect();

			var clickedOnWidth = event.pageX - rect.left; // hope to god there is not going to be any horizontal scroll
			var percentageOfWidth = clickedOnWidth / t.clientWidth;

			if (this._audio) {
				var currentTime = this.state.audio_track_length * percentageOfWidth;

				if (!this._audio.audio) {
					this._audio.initializeAudio();
					var onLoaded = function onLoaded() {
						_this8._audio.audio.currentTime = _this8.state.audio_track_length * percentageOfWidth;
						_this8._audio.audio.removeEventListener('canplaythrough', onLoaded);
					};

					this._audio.audio.addEventListener('canplaythrough', onLoaded, false);
				} else {
					this._audio.audio.currentTime = currentTime;
				}

				if (this._audio.audio.paused) {
					this._audio.audio.play();
				}

				this.setState({
					audio_current_time: currentTime
				});
			}
		}
	}, {
		key: 'onAudioDurationChange',
		value: function onAudioDurationChange(event) {
			this.setState({
				audio_track_length: event.path[0].duration
			});
		}
	}, {
		key: 'onAudioTimeUpdate',
		value: function onAudioTimeUpdate(event) {
			this.setState({
				audio_current_time: event.path[0].currentTime
			});
		}
	}, {
		key: 'onAudioProgress',
		value: function onAudioProgress(event) {
			var audio = event.path[0];
			var duration = audio.duration;
			var progresss = this.state.buffered_progress || 0;

			if (duration > 0) {
				for (var i = 0; i < audio.buffered.length; i++) {
					if (audio.buffered.start(audio.buffered.length - 1 - i) < audio.currentTime) {
						progresss = audio.buffered.end(audio.buffered.length - 1 - i);
						break;
					}
				}
			}

			this.setState({
				buffered_progress: progresss
			});
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			$(document).mouseup(this.onSeekMouseUp);
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			$(document).unbind('mouseup', this.onSeekMouseUp);
		}
	}, {
		key: 'render',
		value: function render() {
			var _this9 = this;

			var current_play = 0,
			    buffered = 0;

			if (this.state.audio_track_length) {
				current_play = this.state.audio_current_time * 100 / this.state.audio_track_length;
				buffered = this.state.buffered_progress * 100 / this.state.audio_track_length;
			}

			return _react2.default.createElement(
				'div',
				{ style: { padding: '0.5rem 0 0.5rem 0' } },
				_react2.default.createElement(_iconAudioPlayer.IconAudioPlayer, { ref: function ref(r) {
						return _this9._audio = r;
					}, source: this.props.src, style: { display: 'inline-block', width: '20px' }, ignore_analyser: true, progress: this.onAudioProgress, durationchange: this.onAudioDurationChange, timeupdate: this.onAudioTimeUpdate }),
				_react2.default.createElement(
					'div',
					{ className: 'audio-seek', ref: function ref(r) {
							return _this9._seekbar = r;
						}, onClick: this.onSeekBarClick, onMouseDown: this.onSeekMouseDown, onMouseUp: this.onSeekMouseUp, onMouseMove: this.onSeekMouseMove },
					_react2.default.createElement('div', { style: { background: '#F05F40', height: '0.5rem', width: current_play + '%' } }),
					_react2.default.createElement('div', { style: { background: '#808080', height: '0.5rem', width: buffered + '%' } })
				)
			);
		}
	}]);

	return AudioWithSeek;
}(_react2.default.Component);

var Checkbox = function (_React$Component6) {
	_inherits(Checkbox, _React$Component6);

	function Checkbox(props) {
		_classCallCheck(this, Checkbox);

		var _this10 = _possibleConstructorReturn(this, (Checkbox.__proto__ || Object.getPrototypeOf(Checkbox)).call(this, props));

		_this10.state = {
			isChecked: !!props.isChecked
		};

		_this10.toggleCheckboxChange = _this10.toggleCheckboxChange.bind(_this10);
		return _this10;
	}

	_createClass(Checkbox, [{
		key: 'toggleCheckboxChange',
		value: function toggleCheckboxChange(event) {
			this.setState({
				isChecked: event.target.checked
			});
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'label',
				null,
				_react2.default.createElement('input', {
					type: 'checkbox',
					value: this.props.label,
					checked: this.state.isChecked,
					onChange: this.toggleCheckboxChange
				}),
				this.props.label
			);
		}
	}]);

	return Checkbox;
}(_react2.default.Component);

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(58);

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(63);

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(64);

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(68);

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(94);

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(102);

var _propTypes2 = _interopRequireDefault(_propTypes);

var _kefir = __webpack_require__(48);

var _kefir2 = _interopRequireDefault(_kefir);

var _kefirBus = __webpack_require__(108);

var _kefirBus2 = _interopRequireDefault(_kefirBus);

var _getTransitionTimeMs = __webpack_require__(109);

var _getTransitionTimeMs2 = _interopRequireDefault(_getTransitionTimeMs);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var SmoothCollapse = function (_React$Component) {
  (0, _inherits3.default)(SmoothCollapse, _React$Component);

  function SmoothCollapse(props) {
    (0, _classCallCheck3.default)(this, SmoothCollapse);

    var _this = (0, _possibleConstructorReturn3.default)(this, (SmoothCollapse.__proto__ || (0, _getPrototypeOf2.default)(SmoothCollapse)).call(this, props));

    _this._resetter = (0, _kefirBus2.default)();
    _this._mainEl = null;
    _this._innerEl = null;

    _this._mainElSetter = function (el) {
      _this._mainEl = el;
    };

    _this._innerElSetter = function (el) {
      _this._innerEl = el;
    };

    _this.state = {
      hasBeenVisibleBefore: props.expanded || _this._visibleWhenClosed(),
      fullyClosed: !props.expanded,
      height: props.expanded ? 'auto' : props.collapsedHeight
    };
    return _this;
  }

  (0, _createClass3.default)(SmoothCollapse, [{
    key: '_visibleWhenClosed',
    value: function _visibleWhenClosed(props) {
      if (!props) props = this.props;
      return parseFloat(props.collapsedHeight) !== 0;
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this._resetter.emit(null);
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      var _this2 = this;

      if (!this.props.expanded && nextProps.expanded) {
        this._resetter.emit(null);

        // In order to expand, we need to know the height of the children, so we
        // need to setState first so they get rendered before we continue.

        this.setState({
          fullyClosed: false,
          hasBeenVisibleBefore: true
        }, function () {
          var mainEl = _this2._mainEl;
          var innerEl = _this2._innerEl;
          if (!mainEl || !innerEl) throw new Error('Should not happen');

          // Set the collapser to the target height instead of auto so that it
          // animates correctly. Then switch it to 'auto' after the animation so
          // that it flows correctly if the page is resized.
          var targetHeight = innerEl.clientHeight + 'px';
          _this2.setState({
            height: targetHeight
          });

          // Wait until the transitionend event, or until a timer goes off in
          // case the event doesn't fire because the browser doesn't support it
          // or the element is hidden before it happens. The timer is a little
          // longer than the transition is supposed to take to make sure we don't
          // cut the animation early while it's still going if the browser is
          // running it just a little slow.
          _kefir2.default.fromEvents(mainEl, 'transitionend').merge(_kefir2.default.later((0, _getTransitionTimeMs2.default)(nextProps.heightTransition) * 1.1 + 500)).takeUntilBy(_this2._resetter).take(1).onValue(function () {
            _this2.setState({
              height: 'auto'
            }, function () {
              if (_this2.props.onChangeEnd) {
                _this2.props.onChangeEnd();
              }
            });
          });
        });
      } else if (this.props.expanded && !nextProps.expanded) {
        this._resetter.emit(null);

        if (!this._innerEl) throw new Error('Should not happen');
        this.setState({
          height: this._innerEl.clientHeight + 'px'
        }, function () {
          var mainEl = _this2._mainEl;
          if (!mainEl) throw new Error('Should not happen');

          mainEl.clientHeight; // force the page layout
          _this2.setState({
            height: nextProps.collapsedHeight
          });

          // See comment above about previous use of transitionend event.
          _kefir2.default.fromEvents(mainEl, 'transitionend').merge(_kefir2.default.later((0, _getTransitionTimeMs2.default)(nextProps.heightTransition) * 1.1 + 500)).takeUntilBy(_this2._resetter).take(1).onValue(function () {
            _this2.setState({
              fullyClosed: true
            });
            if (_this2.props.onChangeEnd) {
              _this2.props.onChangeEnd();
            }
          });
        });
      } else if (!nextProps.expanded && this.props.collapsedHeight !== nextProps.collapsedHeight) {
        this.setState({
          hasBeenVisibleBefore: this.state.hasBeenVisibleBefore || this._visibleWhenClosed(nextProps),
          height: nextProps.collapsedHeight
        });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var visibleWhenClosed = this._visibleWhenClosed();
      var _state = this.state,
          height = _state.height,
          fullyClosed = _state.fullyClosed,
          hasBeenVisibleBefore = _state.hasBeenVisibleBefore;

      var innerEl = hasBeenVisibleBefore ? _react2.default.createElement('div', { ref: this._innerElSetter, style: { overflow: 'hidden' } }, this.props.children) : null;

      return _react2.default.createElement('div', {
        ref: this._mainElSetter,
        style: {
          height: height, overflow: 'hidden',
          display: fullyClosed && !visibleWhenClosed ? 'none' : null,
          transition: 'height ' + this.props.heightTransition
        }
      }, innerEl);
    }
  }]);
  return SmoothCollapse;
}(_react2.default.Component);

SmoothCollapse.propTypes = {
  expanded: _propTypes2.default.bool.isRequired,
  onChangeEnd: _propTypes2.default.func,
  collapsedHeight: _propTypes2.default.string,
  heightTransition: _propTypes2.default.string
};
SmoothCollapse.defaultProps = {
  collapsedHeight: '0',
  heightTransition: '.25s ease'
};
exports.default = SmoothCollapse;
module.exports = exports['default'];

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = { "default": __webpack_require__(59), __esModule: true };

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(60);
module.exports = __webpack_require__(2).Object.getPrototypeOf;

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// 19.1.2.9 Object.getPrototypeOf(O)
var toObject = __webpack_require__(34);
var $getPrototypeOf = __webpack_require__(35);

__webpack_require__(61)('getPrototypeOf', function () {
  return function getPrototypeOf(it) {
    return $getPrototypeOf(toObject(it));
  };
});

/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// most Object methods by ES6 should accept primitives
var $export = __webpack_require__(6);
var core = __webpack_require__(2);
var fails = __webpack_require__(12);
module.exports = function (KEY, exec) {
  var fn = (core.Object || {})[KEY] || Object[KEY];
  var exp = {};
  exp[KEY] = exec(fn);
  $export($export.S + $export.F * fails(function () {
    fn(1);
  }), 'Object', exp);
};

/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (it) {
  if (typeof it != 'function') throw TypeError(it + ' is not a function!');
  return it;
};

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports.default = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _defineProperty = __webpack_require__(65);

var _defineProperty2 = _interopRequireDefault(_defineProperty);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

exports.default = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      (0, _defineProperty2.default)(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = { "default": __webpack_require__(66), __esModule: true };

/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(67);
var $Object = __webpack_require__(2).Object;
module.exports = function defineProperty(it, key, desc) {
  return $Object.defineProperty(it, key, desc);
};

/***/ }),
/* 67 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var $export = __webpack_require__(6);
// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)
$export($export.S + $export.F * !__webpack_require__(5), 'Object', { defineProperty: __webpack_require__(4).f });

/***/ }),
/* 68 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof2 = __webpack_require__(39);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

exports.default = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && ((typeof call === "undefined" ? "undefined" : (0, _typeof3.default)(call)) === "object" || typeof call === "function") ? call : self;
};

/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = { "default": __webpack_require__(70), __esModule: true };

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(71);
__webpack_require__(80);
module.exports = __webpack_require__(28).f('iterator');

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var $at = __webpack_require__(72)(true);

// 21.1.3.27 String.prototype[@@iterator]()
__webpack_require__(40)(String, 'String', function (iterated) {
  this._t = String(iterated); // target
  this._i = 0; // next index
  // 21.1.5.2.1 %StringIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var index = this._i;
  var point;
  if (index >= O.length) return { value: undefined, done: true };
  point = $at(O, index);
  this._i += point.length;
  return { value: point, done: false };
});

/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var toInteger = __webpack_require__(21);
var defined = __webpack_require__(17);
// true  -> String#at
// false -> String#codePointAt
module.exports = function (TO_STRING) {
  return function (that, pos) {
    var s = String(defined(that));
    var i = toInteger(pos);
    var l = s.length;
    var a, b;
    if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
    a = s.charCodeAt(i);
    return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff ? TO_STRING ? s.charAt(i) : a : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
  };
};

/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var create = __webpack_require__(24);
var descriptor = __webpack_require__(15);
var setToStringTag = __webpack_require__(27);
var IteratorPrototype = {};

// 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
__webpack_require__(7)(IteratorPrototype, __webpack_require__(9)('iterator'), function () {
  return this;
});

module.exports = function (Constructor, NAME, next) {
  Constructor.prototype = create(IteratorPrototype, { next: descriptor(1, next) });
  setToStringTag(Constructor, NAME + ' Iterator');
};

/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var dP = __webpack_require__(4);
var anObject = __webpack_require__(10);
var getKeys = __webpack_require__(25);

module.exports = __webpack_require__(5) ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = getKeys(Properties);
  var length = keys.length;
  var i = 0;
  var P;
  while (length > i) {
    dP.f(O, P = keys[i++], Properties[P]);
  }return O;
};

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// fallback for non-array-like ES3 and non-enumerable old V8 strings
var cof = __webpack_require__(43);
// eslint-disable-next-line no-prototype-builtins
module.exports = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
  return cof(it) == 'String' ? it.split('') : Object(it);
};

/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// false -> Array#indexOf
// true  -> Array#includes
var toIObject = __webpack_require__(8);
var toLength = __webpack_require__(77);
var toAbsoluteIndex = __webpack_require__(78);
module.exports = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
      // Array#indexOf ignores holes, Array#includes - not
    } else for (; length > index; index++) {
      if (IS_INCLUDES || index in O) {
        if (O[index] === el) return IS_INCLUDES || index || 0;
      }
    }return !IS_INCLUDES && -1;
  };
};

/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// 7.1.15 ToLength
var toInteger = __webpack_require__(21);
var min = Math.min;
module.exports = function (it) {
  return it > 0 ? min(toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
};

/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var toInteger = __webpack_require__(21);
var max = Math.max;
var min = Math.min;
module.exports = function (index, length) {
  index = toInteger(index);
  return index < 0 ? max(index + length, 0) : min(index, length);
};

/***/ }),
/* 79 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var document = __webpack_require__(1).document;
module.exports = document && document.documentElement;

/***/ }),
/* 80 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(81);
var global = __webpack_require__(1);
var hide = __webpack_require__(7);
var Iterators = __webpack_require__(23);
var TO_STRING_TAG = __webpack_require__(9)('toStringTag');

var DOMIterables = ('CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,' + 'DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,' + 'MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,' + 'SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,' + 'TextTrackList,TouchList').split(',');

for (var i = 0; i < DOMIterables.length; i++) {
  var NAME = DOMIterables[i];
  var Collection = global[NAME];
  var proto = Collection && Collection.prototype;
  if (proto && !proto[TO_STRING_TAG]) hide(proto, TO_STRING_TAG, NAME);
  Iterators[NAME] = Iterators.Array;
}

/***/ }),
/* 81 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var addToUnscopables = __webpack_require__(82);
var step = __webpack_require__(83);
var Iterators = __webpack_require__(23);
var toIObject = __webpack_require__(8);

// 22.1.3.4 Array.prototype.entries()
// 22.1.3.13 Array.prototype.keys()
// 22.1.3.29 Array.prototype.values()
// 22.1.3.30 Array.prototype[@@iterator]()
module.exports = __webpack_require__(40)(Array, 'Array', function (iterated, kind) {
  this._t = toIObject(iterated); // target
  this._i = 0; // next index
  this._k = kind; // kind
  // 22.1.5.2.1 %ArrayIteratorPrototype%.next()
}, function () {
  var O = this._t;
  var kind = this._k;
  var index = this._i++;
  if (!O || index >= O.length) {
    this._t = undefined;
    return step(1);
  }
  if (kind == 'keys') return step(0, index);
  if (kind == 'values') return step(0, O[index]);
  return step(0, [index, O[index]]);
}, 'values');

// argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
Iterators.Arguments = Iterators.Array;

addToUnscopables('keys');
addToUnscopables('values');
addToUnscopables('entries');

/***/ }),
/* 82 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function () {/* empty */};

/***/ }),
/* 83 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (done, value) {
  return { value: value, done: !!done };
};

/***/ }),
/* 84 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = { "default": __webpack_require__(85), __esModule: true };

/***/ }),
/* 85 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(86);
__webpack_require__(91);
__webpack_require__(92);
__webpack_require__(93);
module.exports = __webpack_require__(2).Symbol;

/***/ }),
/* 86 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// ECMAScript 6 symbols shim

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var global = __webpack_require__(1);
var has = __webpack_require__(3);
var DESCRIPTORS = __webpack_require__(5);
var $export = __webpack_require__(6);
var redefine = __webpack_require__(41);
var META = __webpack_require__(87).KEY;
var $fails = __webpack_require__(12);
var shared = __webpack_require__(19);
var setToStringTag = __webpack_require__(27);
var uid = __webpack_require__(14);
var wks = __webpack_require__(9);
var wksExt = __webpack_require__(28);
var wksDefine = __webpack_require__(29);
var enumKeys = __webpack_require__(88);
var isArray = __webpack_require__(89);
var anObject = __webpack_require__(10);
var toIObject = __webpack_require__(8);
var toPrimitive = __webpack_require__(20);
var createDesc = __webpack_require__(15);
var _create = __webpack_require__(24);
var gOPNExt = __webpack_require__(90);
var $GOPD = __webpack_require__(46);
var $DP = __webpack_require__(4);
var $keys = __webpack_require__(25);
var gOPD = $GOPD.f;
var dP = $DP.f;
var gOPN = gOPNExt.f;
var $Symbol = global.Symbol;
var $JSON = global.JSON;
var _stringify = $JSON && $JSON.stringify;
var PROTOTYPE = 'prototype';
var HIDDEN = wks('_hidden');
var TO_PRIMITIVE = wks('toPrimitive');
var isEnum = {}.propertyIsEnumerable;
var SymbolRegistry = shared('symbol-registry');
var AllSymbols = shared('symbols');
var OPSymbols = shared('op-symbols');
var ObjectProto = Object[PROTOTYPE];
var USE_NATIVE = typeof $Symbol == 'function';
var QObject = global.QObject;
// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
var setter = !QObject || !QObject[PROTOTYPE] || !QObject[PROTOTYPE].findChild;

// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
var setSymbolDesc = DESCRIPTORS && $fails(function () {
  return _create(dP({}, 'a', {
    get: function get() {
      return dP(this, 'a', { value: 7 }).a;
    }
  })).a != 7;
}) ? function (it, key, D) {
  var protoDesc = gOPD(ObjectProto, key);
  if (protoDesc) delete ObjectProto[key];
  dP(it, key, D);
  if (protoDesc && it !== ObjectProto) dP(ObjectProto, key, protoDesc);
} : dP;

var wrap = function wrap(tag) {
  var sym = AllSymbols[tag] = _create($Symbol[PROTOTYPE]);
  sym._k = tag;
  return sym;
};

var isSymbol = USE_NATIVE && _typeof($Symbol.iterator) == 'symbol' ? function (it) {
  return (typeof it === 'undefined' ? 'undefined' : _typeof(it)) == 'symbol';
} : function (it) {
  return it instanceof $Symbol;
};

var $defineProperty = function defineProperty(it, key, D) {
  if (it === ObjectProto) $defineProperty(OPSymbols, key, D);
  anObject(it);
  key = toPrimitive(key, true);
  anObject(D);
  if (has(AllSymbols, key)) {
    if (!D.enumerable) {
      if (!has(it, HIDDEN)) dP(it, HIDDEN, createDesc(1, {}));
      it[HIDDEN][key] = true;
    } else {
      if (has(it, HIDDEN) && it[HIDDEN][key]) it[HIDDEN][key] = false;
      D = _create(D, { enumerable: createDesc(0, false) });
    }return setSymbolDesc(it, key, D);
  }return dP(it, key, D);
};
var $defineProperties = function defineProperties(it, P) {
  anObject(it);
  var keys = enumKeys(P = toIObject(P));
  var i = 0;
  var l = keys.length;
  var key;
  while (l > i) {
    $defineProperty(it, key = keys[i++], P[key]);
  }return it;
};
var $create = function create(it, P) {
  return P === undefined ? _create(it) : $defineProperties(_create(it), P);
};
var $propertyIsEnumerable = function propertyIsEnumerable(key) {
  var E = isEnum.call(this, key = toPrimitive(key, true));
  if (this === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return false;
  return E || !has(this, key) || !has(AllSymbols, key) || has(this, HIDDEN) && this[HIDDEN][key] ? E : true;
};
var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(it, key) {
  it = toIObject(it);
  key = toPrimitive(key, true);
  if (it === ObjectProto && has(AllSymbols, key) && !has(OPSymbols, key)) return;
  var D = gOPD(it, key);
  if (D && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) D.enumerable = true;
  return D;
};
var $getOwnPropertyNames = function getOwnPropertyNames(it) {
  var names = gOPN(toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (!has(AllSymbols, key = names[i++]) && key != HIDDEN && key != META) result.push(key);
  }return result;
};
var $getOwnPropertySymbols = function getOwnPropertySymbols(it) {
  var IS_OP = it === ObjectProto;
  var names = gOPN(IS_OP ? OPSymbols : toIObject(it));
  var result = [];
  var i = 0;
  var key;
  while (names.length > i) {
    if (has(AllSymbols, key = names[i++]) && (IS_OP ? has(ObjectProto, key) : true)) result.push(AllSymbols[key]);
  }return result;
};

// 19.4.1.1 Symbol([description])
if (!USE_NATIVE) {
  $Symbol = function _Symbol() {
    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor!');
    var tag = uid(arguments.length > 0 ? arguments[0] : undefined);
    var $set = function $set(value) {
      if (this === ObjectProto) $set.call(OPSymbols, value);
      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
      setSymbolDesc(this, tag, createDesc(1, value));
    };
    if (DESCRIPTORS && setter) setSymbolDesc(ObjectProto, tag, { configurable: true, set: $set });
    return wrap(tag);
  };
  redefine($Symbol[PROTOTYPE], 'toString', function toString() {
    return this._k;
  });

  $GOPD.f = $getOwnPropertyDescriptor;
  $DP.f = $defineProperty;
  __webpack_require__(45).f = gOPNExt.f = $getOwnPropertyNames;
  __webpack_require__(30).f = $propertyIsEnumerable;
  __webpack_require__(44).f = $getOwnPropertySymbols;

  if (DESCRIPTORS && !__webpack_require__(22)) {
    redefine(ObjectProto, 'propertyIsEnumerable', $propertyIsEnumerable, true);
  }

  wksExt.f = function (name) {
    return wrap(wks(name));
  };
}

$export($export.G + $export.W + $export.F * !USE_NATIVE, { Symbol: $Symbol });

for (var es6Symbols =
// 19.4.2.2, 19.4.2.3, 19.4.2.4, 19.4.2.6, 19.4.2.8, 19.4.2.9, 19.4.2.10, 19.4.2.11, 19.4.2.12, 19.4.2.13, 19.4.2.14
'hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables'.split(','), j = 0; es6Symbols.length > j;) {
  wks(es6Symbols[j++]);
}for (var wellKnownSymbols = $keys(wks.store), k = 0; wellKnownSymbols.length > k;) {
  wksDefine(wellKnownSymbols[k++]);
}$export($export.S + $export.F * !USE_NATIVE, 'Symbol', {
  // 19.4.2.1 Symbol.for(key)
  'for': function _for(key) {
    return has(SymbolRegistry, key += '') ? SymbolRegistry[key] : SymbolRegistry[key] = $Symbol(key);
  },
  // 19.4.2.5 Symbol.keyFor(sym)
  keyFor: function keyFor(sym) {
    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol!');
    for (var key in SymbolRegistry) {
      if (SymbolRegistry[key] === sym) return key;
    }
  },
  useSetter: function useSetter() {
    setter = true;
  },
  useSimple: function useSimple() {
    setter = false;
  }
});

$export($export.S + $export.F * !USE_NATIVE, 'Object', {
  // 19.1.2.2 Object.create(O [, Properties])
  create: $create,
  // 19.1.2.4 Object.defineProperty(O, P, Attributes)
  defineProperty: $defineProperty,
  // 19.1.2.3 Object.defineProperties(O, Properties)
  defineProperties: $defineProperties,
  // 19.1.2.6 Object.getOwnPropertyDescriptor(O, P)
  getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
  // 19.1.2.7 Object.getOwnPropertyNames(O)
  getOwnPropertyNames: $getOwnPropertyNames,
  // 19.1.2.8 Object.getOwnPropertySymbols(O)
  getOwnPropertySymbols: $getOwnPropertySymbols
});

// 24.3.2 JSON.stringify(value [, replacer [, space]])
$JSON && $export($export.S + $export.F * (!USE_NATIVE || $fails(function () {
  var S = $Symbol();
  // MS Edge converts symbol values to JSON as {}
  // WebKit converts symbol values to JSON as null
  // V8 throws on boxed symbols
  return _stringify([S]) != '[null]' || _stringify({ a: S }) != '{}' || _stringify(Object(S)) != '{}';
})), 'JSON', {
  stringify: function stringify(it) {
    if (it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
    var args = [it];
    var i = 1;
    var replacer, $replacer;
    while (arguments.length > i) {
      args.push(arguments[i++]);
    }replacer = args[1];
    if (typeof replacer == 'function') $replacer = replacer;
    if ($replacer || !isArray(replacer)) replacer = function replacer(key, value) {
      if ($replacer) value = $replacer.call(this, key, value);
      if (!isSymbol(value)) return value;
    };
    args[1] = replacer;
    return _stringify.apply($JSON, args);
  }
});

// 19.4.3.4 Symbol.prototype[@@toPrimitive](hint)
$Symbol[PROTOTYPE][TO_PRIMITIVE] || __webpack_require__(7)($Symbol[PROTOTYPE], TO_PRIMITIVE, $Symbol[PROTOTYPE].valueOf);
// 19.4.3.5 Symbol.prototype[@@toStringTag]
setToStringTag($Symbol, 'Symbol');
// 20.2.1.9 Math[@@toStringTag]
setToStringTag(Math, 'Math', true);
// 24.3.3 JSON[@@toStringTag]
setToStringTag(global.JSON, 'JSON', true);

/***/ }),
/* 87 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var META = __webpack_require__(14)('meta');
var isObject = __webpack_require__(11);
var has = __webpack_require__(3);
var setDesc = __webpack_require__(4).f;
var id = 0;
var isExtensible = Object.isExtensible || function () {
  return true;
};
var FREEZE = !__webpack_require__(12)(function () {
  return isExtensible(Object.preventExtensions({}));
});
var setMeta = function setMeta(it) {
  setDesc(it, META, { value: {
      i: 'O' + ++id, // object ID
      w: {} // weak collections IDs
    } });
};
var fastKey = function fastKey(it, create) {
  // return primitive with prefix
  if (!isObject(it)) return (typeof it === 'undefined' ? 'undefined' : _typeof(it)) == 'symbol' ? it : (typeof it == 'string' ? 'S' : 'P') + it;
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return 'F';
    // not necessary to add metadata
    if (!create) return 'E';
    // add missing metadata
    setMeta(it);
    // return object ID
  }return it[META].i;
};
var getWeak = function getWeak(it, create) {
  if (!has(it, META)) {
    // can't set metadata to uncaught frozen object
    if (!isExtensible(it)) return true;
    // not necessary to add metadata
    if (!create) return false;
    // add missing metadata
    setMeta(it);
    // return hash weak collections IDs
  }return it[META].w;
};
// add metadata on freeze-family methods calling
var onFreeze = function onFreeze(it) {
  if (FREEZE && meta.NEED && isExtensible(it) && !has(it, META)) setMeta(it);
  return it;
};
var meta = module.exports = {
  KEY: META,
  NEED: false,
  fastKey: fastKey,
  getWeak: getWeak,
  onFreeze: onFreeze
};

/***/ }),
/* 88 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// all enumerable object keys, includes symbols
var getKeys = __webpack_require__(25);
var gOPS = __webpack_require__(44);
var pIE = __webpack_require__(30);
module.exports = function (it) {
  var result = getKeys(it);
  var getSymbols = gOPS.f;
  if (getSymbols) {
    var symbols = getSymbols(it);
    var isEnum = pIE.f;
    var i = 0;
    var key;
    while (symbols.length > i) {
      if (isEnum.call(it, key = symbols[i++])) result.push(key);
    }
  }return result;
};

/***/ }),
/* 89 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// 7.2.2 IsArray(argument)
var cof = __webpack_require__(43);
module.exports = Array.isArray || function isArray(arg) {
  return cof(arg) == 'Array';
};

/***/ }),
/* 90 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
var toIObject = __webpack_require__(8);
var gOPN = __webpack_require__(45).f;
var toString = {}.toString;

var windowNames = (typeof window === 'undefined' ? 'undefined' : _typeof(window)) == 'object' && window && Object.getOwnPropertyNames ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function getWindowNames(it) {
  try {
    return gOPN(it);
  } catch (e) {
    return windowNames.slice();
  }
};

module.exports.f = function getOwnPropertyNames(it) {
  return windowNames && toString.call(it) == '[object Window]' ? getWindowNames(it) : gOPN(toIObject(it));
};

/***/ }),
/* 91 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/***/ }),
/* 92 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(29)('asyncIterator');

/***/ }),
/* 93 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(29)('observable');

/***/ }),
/* 94 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setPrototypeOf = __webpack_require__(95);

var _setPrototypeOf2 = _interopRequireDefault(_setPrototypeOf);

var _create = __webpack_require__(99);

var _create2 = _interopRequireDefault(_create);

var _typeof2 = __webpack_require__(39);

var _typeof3 = _interopRequireDefault(_typeof2);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

exports.default = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : (0, _typeof3.default)(superClass)));
  }

  subClass.prototype = (0, _create2.default)(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf2.default ? (0, _setPrototypeOf2.default)(subClass, superClass) : subClass.__proto__ = superClass;
};

/***/ }),
/* 95 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = { "default": __webpack_require__(96), __esModule: true };

/***/ }),
/* 96 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(97);
module.exports = __webpack_require__(2).Object.setPrototypeOf;

/***/ }),
/* 97 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// 19.1.3.19 Object.setPrototypeOf(O, proto)
var $export = __webpack_require__(6);
$export($export.S, 'Object', { setPrototypeOf: __webpack_require__(98).set });

/***/ }),
/* 98 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var isObject = __webpack_require__(11);
var anObject = __webpack_require__(10);
var check = function check(O, proto) {
  anObject(O);
  if (!isObject(proto) && proto !== null) throw TypeError(proto + ": can't set as prototype!");
};
module.exports = {
  set: Object.setPrototypeOf || ('__proto__' in {} ? // eslint-disable-line
  function (test, buggy, set) {
    try {
      set = __webpack_require__(36)(Function.call, __webpack_require__(46).f(Object.prototype, '__proto__').set, 2);
      set(test, []);
      buggy = !(test instanceof Array);
    } catch (e) {
      buggy = true;
    }
    return function setPrototypeOf(O, proto) {
      check(O, proto);
      if (buggy) O.__proto__ = proto;else set(O, proto);
      return O;
    };
  }({}, false) : undefined),
  check: check
};

/***/ }),
/* 99 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = { "default": __webpack_require__(100), __esModule: true };

/***/ }),
/* 100 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(101);
var $Object = __webpack_require__(2).Object;
module.exports = function create(P, D) {
  return $Object.create(P, D);
};

/***/ }),
/* 101 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var $export = __webpack_require__(6);
// 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])
$export($export.S, 'Object', { create: __webpack_require__(24) });

/***/ }),
/* 102 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

if (process.env.NODE_ENV !== 'production') {
  var REACT_ELEMENT_TYPE = typeof Symbol === 'function' && Symbol.for && Symbol.for('react.element') || 0xeac7;

  var isValidElement = function isValidElement(object) {
    return (typeof object === 'undefined' ? 'undefined' : _typeof(object)) === 'object' && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
  };

  // By explicitly using `prop-types` you are opting into new development behavior.
  // http://fb.me/prop-types-in-prod
  var throwOnDirectAccess = true;
  module.exports = __webpack_require__(103)(isValidElement, throwOnDirectAccess);
} else {
  // By explicitly using `prop-types` you are opting into new production behavior.
  // http://fb.me/prop-types-in-prod
  module.exports = __webpack_require__(106)();
}
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(13)))

/***/ }),
/* 103 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var emptyFunction = __webpack_require__(31);
var invariant = __webpack_require__(32);
var warning = __webpack_require__(47);
var assign = __webpack_require__(104);

var ReactPropTypesSecret = __webpack_require__(33);
var checkPropTypes = __webpack_require__(105);

module.exports = function (isValidElement, throwOnDirectAccess) {
  /* global Symbol */
  var ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
  var FAUX_ITERATOR_SYMBOL = '@@iterator'; // Before Symbol spec.

  /**
   * Returns the iterator method function contained on the iterable object.
   *
   * Be sure to invoke the function with the iterable as context:
   *
   *     var iteratorFn = getIteratorFn(myIterable);
   *     if (iteratorFn) {
   *       var iterator = iteratorFn.call(myIterable);
   *       ...
   *     }
   *
   * @param {?object} maybeIterable
   * @return {?function}
   */
  function getIteratorFn(maybeIterable) {
    var iteratorFn = maybeIterable && (ITERATOR_SYMBOL && maybeIterable[ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL]);
    if (typeof iteratorFn === 'function') {
      return iteratorFn;
    }
  }

  /**
   * Collection of methods that allow declaration and validation of props that are
   * supplied to React components. Example usage:
   *
   *   var Props = require('ReactPropTypes');
   *   var MyArticle = React.createClass({
   *     propTypes: {
   *       // An optional string prop named "description".
   *       description: Props.string,
   *
   *       // A required enum prop named "category".
   *       category: Props.oneOf(['News','Photos']).isRequired,
   *
   *       // A prop named "dialog" that requires an instance of Dialog.
   *       dialog: Props.instanceOf(Dialog).isRequired
   *     },
   *     render: function() { ... }
   *   });
   *
   * A more formal specification of how these methods are used:
   *
   *   type := array|bool|func|object|number|string|oneOf([...])|instanceOf(...)
   *   decl := ReactPropTypes.{type}(.isRequired)?
   *
   * Each and every declaration produces a function with the same signature. This
   * allows the creation of custom validation functions. For example:
   *
   *  var MyLink = React.createClass({
   *    propTypes: {
   *      // An optional string or URI prop named "href".
   *      href: function(props, propName, componentName) {
   *        var propValue = props[propName];
   *        if (propValue != null && typeof propValue !== 'string' &&
   *            !(propValue instanceof URI)) {
   *          return new Error(
   *            'Expected a string or an URI for ' + propName + ' in ' +
   *            componentName
   *          );
   *        }
   *      }
   *    },
   *    render: function() {...}
   *  });
   *
   * @internal
   */

  var ANONYMOUS = '<<anonymous>>';

  // Important!
  // Keep this list in sync with production version in `./factoryWithThrowingShims.js`.
  var ReactPropTypes = {
    array: createPrimitiveTypeChecker('array'),
    bool: createPrimitiveTypeChecker('boolean'),
    func: createPrimitiveTypeChecker('function'),
    number: createPrimitiveTypeChecker('number'),
    object: createPrimitiveTypeChecker('object'),
    string: createPrimitiveTypeChecker('string'),
    symbol: createPrimitiveTypeChecker('symbol'),

    any: createAnyTypeChecker(),
    arrayOf: createArrayOfTypeChecker,
    element: createElementTypeChecker(),
    instanceOf: createInstanceTypeChecker,
    node: createNodeChecker(),
    objectOf: createObjectOfTypeChecker,
    oneOf: createEnumTypeChecker,
    oneOfType: createUnionTypeChecker,
    shape: createShapeTypeChecker,
    exact: createStrictShapeTypeChecker
  };

  /**
   * inlined Object.is polyfill to avoid requiring consumers ship their own
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/is
   */
  /*eslint-disable no-self-compare*/
  function is(x, y) {
    // SameValue algorithm
    if (x === y) {
      // Steps 1-5, 7-10
      // Steps 6.b-6.e: +0 != -0
      return x !== 0 || 1 / x === 1 / y;
    } else {
      // Step 6.a: NaN == NaN
      return x !== x && y !== y;
    }
  }
  /*eslint-enable no-self-compare*/

  /**
   * We use an Error-like object for backward compatibility as people may call
   * PropTypes directly and inspect their output. However, we don't use real
   * Errors anymore. We don't inspect their stack anyway, and creating them
   * is prohibitively expensive if they are created too often, such as what
   * happens in oneOfType() for any type before the one that matched.
   */
  function PropTypeError(message) {
    this.message = message;
    this.stack = '';
  }
  // Make `instanceof Error` still work for returned errors.
  PropTypeError.prototype = Error.prototype;

  function createChainableTypeChecker(validate) {
    if (process.env.NODE_ENV !== 'production') {
      var manualPropTypeCallCache = {};
      var manualPropTypeWarningCount = 0;
    }
    function checkType(isRequired, props, propName, componentName, location, propFullName, secret) {
      componentName = componentName || ANONYMOUS;
      propFullName = propFullName || propName;

      if (secret !== ReactPropTypesSecret) {
        if (throwOnDirectAccess) {
          // New behavior only for users of `prop-types` package
          invariant(false, 'Calling PropTypes validators directly is not supported by the `prop-types` package. ' + 'Use `PropTypes.checkPropTypes()` to call them. ' + 'Read more at http://fb.me/use-check-prop-types');
        } else if (process.env.NODE_ENV !== 'production' && typeof console !== 'undefined') {
          // Old behavior for people using React.PropTypes
          var cacheKey = componentName + ':' + propName;
          if (!manualPropTypeCallCache[cacheKey] &&
          // Avoid spamming the console because they are often not actionable except for lib authors
          manualPropTypeWarningCount < 3) {
            warning(false, 'You are manually calling a React.PropTypes validation ' + 'function for the `%s` prop on `%s`. This is deprecated ' + 'and will throw in the standalone `prop-types` package. ' + 'You may be seeing this warning due to a third-party PropTypes ' + 'library. See https://fb.me/react-warning-dont-call-proptypes ' + 'for details.', propFullName, componentName);
            manualPropTypeCallCache[cacheKey] = true;
            manualPropTypeWarningCount++;
          }
        }
      }
      if (props[propName] == null) {
        if (isRequired) {
          if (props[propName] === null) {
            return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required ' + ('in `' + componentName + '`, but its value is `null`.'));
          }
          return new PropTypeError('The ' + location + ' `' + propFullName + '` is marked as required in ' + ('`' + componentName + '`, but its value is `undefined`.'));
        }
        return null;
      } else {
        return validate(props, propName, componentName, location, propFullName);
      }
    }

    var chainedCheckType = checkType.bind(null, false);
    chainedCheckType.isRequired = checkType.bind(null, true);

    return chainedCheckType;
  }

  function createPrimitiveTypeChecker(expectedType) {
    function validate(props, propName, componentName, location, propFullName, secret) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== expectedType) {
        // `propValue` being instance of, say, date/regexp, pass the 'object'
        // check, but we can offer a more precise error message here rather than
        // 'of type `object`'.
        var preciseType = getPreciseType(propValue);

        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + preciseType + '` supplied to `' + componentName + '`, expected ') + ('`' + expectedType + '`.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createAnyTypeChecker() {
    return createChainableTypeChecker(emptyFunction.thatReturnsNull);
  }

  function createArrayOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside arrayOf.');
      }
      var propValue = props[propName];
      if (!Array.isArray(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an array.'));
      }
      for (var i = 0; i < propValue.length; i++) {
        var error = typeChecker(propValue, i, componentName, location, propFullName + '[' + i + ']', ReactPropTypesSecret);
        if (error instanceof Error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createElementTypeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      if (!isValidElement(propValue)) {
        var propType = getPropType(propValue);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected a single ReactElement.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createInstanceTypeChecker(expectedClass) {
    function validate(props, propName, componentName, location, propFullName) {
      if (!(props[propName] instanceof expectedClass)) {
        var expectedClassName = expectedClass.name || ANONYMOUS;
        var actualClassName = getClassName(props[propName]);
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + actualClassName + '` supplied to `' + componentName + '`, expected ') + ('instance of `' + expectedClassName + '`.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createEnumTypeChecker(expectedValues) {
    if (!Array.isArray(expectedValues)) {
      process.env.NODE_ENV !== 'production' ? warning(false, 'Invalid argument supplied to oneOf, expected an instance of array.') : void 0;
      return emptyFunction.thatReturnsNull;
    }

    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      for (var i = 0; i < expectedValues.length; i++) {
        if (is(propValue, expectedValues[i])) {
          return null;
        }
      }

      var valuesString = JSON.stringify(expectedValues);
      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of value `' + propValue + '` ' + ('supplied to `' + componentName + '`, expected one of ' + valuesString + '.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createObjectOfTypeChecker(typeChecker) {
    function validate(props, propName, componentName, location, propFullName) {
      if (typeof typeChecker !== 'function') {
        return new PropTypeError('Property `' + propFullName + '` of component `' + componentName + '` has invalid PropType notation inside objectOf.');
      }
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type ' + ('`' + propType + '` supplied to `' + componentName + '`, expected an object.'));
      }
      for (var key in propValue) {
        if (propValue.hasOwnProperty(key)) {
          var error = typeChecker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
          if (error instanceof Error) {
            return error;
          }
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createUnionTypeChecker(arrayOfTypeCheckers) {
    if (!Array.isArray(arrayOfTypeCheckers)) {
      process.env.NODE_ENV !== 'production' ? warning(false, 'Invalid argument supplied to oneOfType, expected an instance of array.') : void 0;
      return emptyFunction.thatReturnsNull;
    }

    for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
      var checker = arrayOfTypeCheckers[i];
      if (typeof checker !== 'function') {
        warning(false, 'Invalid argument supplied to oneOfType. Expected an array of check functions, but ' + 'received %s at index %s.', getPostfixForTypeWarning(checker), i);
        return emptyFunction.thatReturnsNull;
      }
    }

    function validate(props, propName, componentName, location, propFullName) {
      for (var i = 0; i < arrayOfTypeCheckers.length; i++) {
        var checker = arrayOfTypeCheckers[i];
        if (checker(props, propName, componentName, location, propFullName, ReactPropTypesSecret) == null) {
          return null;
        }
      }

      return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`.'));
    }
    return createChainableTypeChecker(validate);
  }

  function createNodeChecker() {
    function validate(props, propName, componentName, location, propFullName) {
      if (!isNode(props[propName])) {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` supplied to ' + ('`' + componentName + '`, expected a ReactNode.'));
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      for (var key in shapeTypes) {
        var checker = shapeTypes[key];
        if (!checker) {
          continue;
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
        if (error) {
          return error;
        }
      }
      return null;
    }
    return createChainableTypeChecker(validate);
  }

  function createStrictShapeTypeChecker(shapeTypes) {
    function validate(props, propName, componentName, location, propFullName) {
      var propValue = props[propName];
      var propType = getPropType(propValue);
      if (propType !== 'object') {
        return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` of type `' + propType + '` ' + ('supplied to `' + componentName + '`, expected `object`.'));
      }
      // We need to check all keys in case some are required but missing from
      // props.
      var allKeys = assign({}, props[propName], shapeTypes);
      for (var key in allKeys) {
        var checker = shapeTypes[key];
        if (!checker) {
          return new PropTypeError('Invalid ' + location + ' `' + propFullName + '` key `' + key + '` supplied to `' + componentName + '`.' + '\nBad object: ' + JSON.stringify(props[propName], null, '  ') + '\nValid keys: ' + JSON.stringify(Object.keys(shapeTypes), null, '  '));
        }
        var error = checker(propValue, key, componentName, location, propFullName + '.' + key, ReactPropTypesSecret);
        if (error) {
          return error;
        }
      }
      return null;
    }

    return createChainableTypeChecker(validate);
  }

  function isNode(propValue) {
    switch (typeof propValue === 'undefined' ? 'undefined' : _typeof(propValue)) {
      case 'number':
      case 'string':
      case 'undefined':
        return true;
      case 'boolean':
        return !propValue;
      case 'object':
        if (Array.isArray(propValue)) {
          return propValue.every(isNode);
        }
        if (propValue === null || isValidElement(propValue)) {
          return true;
        }

        var iteratorFn = getIteratorFn(propValue);
        if (iteratorFn) {
          var iterator = iteratorFn.call(propValue);
          var step;
          if (iteratorFn !== propValue.entries) {
            while (!(step = iterator.next()).done) {
              if (!isNode(step.value)) {
                return false;
              }
            }
          } else {
            // Iterator will provide entry [k,v] tuples rather than values.
            while (!(step = iterator.next()).done) {
              var entry = step.value;
              if (entry) {
                if (!isNode(entry[1])) {
                  return false;
                }
              }
            }
          }
        } else {
          return false;
        }

        return true;
      default:
        return false;
    }
  }

  function isSymbol(propType, propValue) {
    // Native Symbol.
    if (propType === 'symbol') {
      return true;
    }

    // 19.4.3.5 Symbol.prototype[@@toStringTag] === 'Symbol'
    if (propValue['@@toStringTag'] === 'Symbol') {
      return true;
    }

    // Fallback for non-spec compliant Symbols which are polyfilled.
    if (typeof Symbol === 'function' && propValue instanceof Symbol) {
      return true;
    }

    return false;
  }

  // Equivalent of `typeof` but with special handling for array and regexp.
  function getPropType(propValue) {
    var propType = typeof propValue === 'undefined' ? 'undefined' : _typeof(propValue);
    if (Array.isArray(propValue)) {
      return 'array';
    }
    if (propValue instanceof RegExp) {
      // Old webkits (at least until Android 4.0) return 'function' rather than
      // 'object' for typeof a RegExp. We'll normalize this here so that /bla/
      // passes PropTypes.object.
      return 'object';
    }
    if (isSymbol(propType, propValue)) {
      return 'symbol';
    }
    return propType;
  }

  // This handles more types than `getPropType`. Only used for error messages.
  // See `createPrimitiveTypeChecker`.
  function getPreciseType(propValue) {
    if (typeof propValue === 'undefined' || propValue === null) {
      return '' + propValue;
    }
    var propType = getPropType(propValue);
    if (propType === 'object') {
      if (propValue instanceof Date) {
        return 'date';
      } else if (propValue instanceof RegExp) {
        return 'regexp';
      }
    }
    return propType;
  }

  // Returns a string that is postfixed to a warning about an invalid type.
  // For example, "undefined" or "of type array"
  function getPostfixForTypeWarning(value) {
    var type = getPreciseType(value);
    switch (type) {
      case 'array':
      case 'object':
        return 'an ' + type;
      case 'boolean':
      case 'date':
      case 'regexp':
        return 'a ' + type;
      default:
        return type;
    }
  }

  // Returns class name of the object, if any.
  function getClassName(propValue) {
    if (!propValue.constructor || !propValue.constructor.name) {
      return ANONYMOUS;
    }
    return propValue.constructor.name;
  }

  ReactPropTypes.checkPropTypes = checkPropTypes;
  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(13)))

/***/ }),
/* 104 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*
object-assign
(c) Sindre Sorhus
@license MIT
*/


/* eslint-disable no-unused-vars */

var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var propIsEnumerable = Object.prototype.propertyIsEnumerable;

function toObject(val) {
	if (val === null || val === undefined) {
		throw new TypeError('Object.assign cannot be called with null or undefined');
	}

	return Object(val);
}

function shouldUseNative() {
	try {
		if (!Object.assign) {
			return false;
		}

		// Detect buggy property enumeration order in older V8 versions.

		// https://bugs.chromium.org/p/v8/issues/detail?id=4118
		var test1 = new String('abc'); // eslint-disable-line no-new-wrappers
		test1[5] = 'de';
		if (Object.getOwnPropertyNames(test1)[0] === '5') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test2 = {};
		for (var i = 0; i < 10; i++) {
			test2['_' + String.fromCharCode(i)] = i;
		}
		var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
			return test2[n];
		});
		if (order2.join('') !== '0123456789') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test3 = {};
		'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
			test3[letter] = letter;
		});
		if (Object.keys(Object.assign({}, test3)).join('') !== 'abcdefghijklmnopqrst') {
			return false;
		}

		return true;
	} catch (err) {
		// We don't expect any of the above to throw, but better to be safe.
		return false;
	}
}

module.exports = shouldUseNative() ? Object.assign : function (target, source) {
	var from;
	var to = toObject(target);
	var symbols;

	for (var s = 1; s < arguments.length; s++) {
		from = Object(arguments[s]);

		for (var key in from) {
			if (hasOwnProperty.call(from, key)) {
				to[key] = from[key];
			}
		}

		if (getOwnPropertySymbols) {
			symbols = getOwnPropertySymbols(from);
			for (var i = 0; i < symbols.length; i++) {
				if (propIsEnumerable.call(from, symbols[i])) {
					to[symbols[i]] = from[symbols[i]];
				}
			}
		}
	}

	return to;
};

/***/ }),
/* 105 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

if (process.env.NODE_ENV !== 'production') {
  var invariant = __webpack_require__(32);
  var warning = __webpack_require__(47);
  var ReactPropTypesSecret = __webpack_require__(33);
  var loggedTypeFailures = {};
}

/**
 * Assert that the values match with the type specs.
 * Error messages are memorized and will only be shown once.
 *
 * @param {object} typeSpecs Map of name to a ReactPropType
 * @param {object} values Runtime values that need to be type-checked
 * @param {string} location e.g. "prop", "context", "child context"
 * @param {string} componentName Name of the component for error messages.
 * @param {?Function} getStack Returns the component stack.
 * @private
 */
function checkPropTypes(typeSpecs, values, location, componentName, getStack) {
  if (process.env.NODE_ENV !== 'production') {
    for (var typeSpecName in typeSpecs) {
      if (typeSpecs.hasOwnProperty(typeSpecName)) {
        var error;
        // Prop type validation may throw. In case they do, we don't want to
        // fail the render phase where it didn't fail before. So we log it.
        // After these have been cleaned up, we'll let them throw.
        try {
          // This is intentionally an invariant that gets caught. It's the same
          // behavior as without this statement except with a better message.
          invariant(typeof typeSpecs[typeSpecName] === 'function', '%s: %s type `%s` is invalid; it must be a function, usually from ' + 'the `prop-types` package, but received `%s`.', componentName || 'React class', location, typeSpecName, _typeof(typeSpecs[typeSpecName]));
          error = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, ReactPropTypesSecret);
        } catch (ex) {
          error = ex;
        }
        warning(!error || error instanceof Error, '%s: type specification of %s `%s` is invalid; the type checker ' + 'function must return `null` or an `Error` but returned a %s. ' + 'You may have forgotten to pass an argument to the type checker ' + 'creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and ' + 'shape all require an argument).', componentName || 'React class', location, typeSpecName, typeof error === 'undefined' ? 'undefined' : _typeof(error));
        if (error instanceof Error && !(error.message in loggedTypeFailures)) {
          // Only monitor this failure once because there tends to be a lot of the
          // same error.
          loggedTypeFailures[error.message] = true;

          var stack = getStack ? getStack() : '';

          warning(false, 'Failed %s type: %s%s', location, error.message, stack != null ? stack : '');
        }
      }
    }
  }
}

module.exports = checkPropTypes;
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(13)))

/***/ }),
/* 106 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var emptyFunction = __webpack_require__(31);
var invariant = __webpack_require__(32);
var ReactPropTypesSecret = __webpack_require__(33);

module.exports = function () {
  function shim(props, propName, componentName, location, propFullName, secret) {
    if (secret === ReactPropTypesSecret) {
      // It is still safe when called from React.
      return;
    }
    invariant(false, 'Calling PropTypes validators directly is not supported by the `prop-types` package. ' + 'Use PropTypes.checkPropTypes() to call them. ' + 'Read more at http://fb.me/use-check-prop-types');
  };
  shim.isRequired = shim;
  function getShim() {
    return shim;
  };
  // Important!
  // Keep this list in sync with production version in `./factoryWithTypeCheckers.js`.
  var ReactPropTypes = {
    array: shim,
    bool: shim,
    func: shim,
    number: shim,
    object: shim,
    string: shim,
    symbol: shim,

    any: shim,
    arrayOf: getShim,
    element: shim,
    instanceOf: getShim,
    node: shim,
    objectOf: getShim,
    oneOf: getShim,
    oneOfType: getShim,
    shape: getShim,
    exact: getShim
  };

  ReactPropTypes.checkPropTypes = emptyFunction;
  ReactPropTypes.PropTypes = ReactPropTypes;

  return ReactPropTypes;
};

/***/ }),
/* 107 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var g;

// This works in non-strict mode
g = function () {
	return this;
}();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1, eval)("this");
} catch (e) {
	// This works if the window reference is available
	if ((typeof window === "undefined" ? "undefined" : _typeof(window)) === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;

/***/ }),
/* 108 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

(function (root, factory) {
  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(48)], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else if ((typeof exports === 'undefined' ? 'undefined' : _typeof(exports)) === 'object') {
    module.exports = factory(require('kefir'));
  } else {
    root.KefirBus = factory(root.Kefir);
  }
})(undefined, function (Kefir) {

  var dummyPool = { plug: function plug() {}, unplug: function unplug() {} };

  return function kefirBus() {
    var ended = false;
    var pool = Kefir.pool();
    var emitter = null;

    var stream = Kefir.stream(function (_emitter) {
      function sub(event) {
        _emitter.emitEvent(event);
      }
      emitter = _emitter;

      if (ended) {
        _emitter.end();
      } else {
        pool.onAny(sub);
        return function () {
          emitter = null;
          pool.offAny(sub);
        };
      }
    });

    stream.value = stream.emit = function (x) {
      if (emitter) emitter.emit(x);
      return stream;
    };

    stream.error = function (err) {
      if (emitter) emitter.error(err);
      return stream;
    };

    stream.event = stream.emitEvent = function (event) {
      if (event.type === 'end') {
        stream.end();
      } else {
        if (emitter) emitter.emitEvent(event);
      }
      return stream;
    };

    stream.plug = function (s) {
      pool.plug(s);
      return stream;
    };

    stream.unplug = function (s) {
      pool.unplug(s);
      return stream;
    };

    stream.end = function () {
      if (!ended) {
        ended = true;
        if (emitter) emitter.end();
        pool = dummyPool;
      }
      return stream;
    };

    return stream.setName('bus');
  };
});

/***/ }),
/* 109 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getTransitionTimeMs;
function getTransitionTimeMs(heightTransition) {
  var m = /(\d+(?:\.\d+)?|\.\d+)(m?s)\b/i.exec(heightTransition);
  if (!m) throw new Error('Could not parse time from transition value');
  return Number(m[1]) * (m[2].length === 1 ? 1000 : 1);
}
module.exports = exports['default'];

/***/ }),
/* 110 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

var _generic = __webpack_require__(49);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SettingsApplication = function (_React$Component) {
	_inherits(SettingsApplication, _React$Component);

	function SettingsApplication(props) {
		_classCallCheck(this, SettingsApplication);

		var _this = _possibleConstructorReturn(this, (SettingsApplication.__proto__ || Object.getPrototypeOf(SettingsApplication)).call(this, props));

		_this.state = {
			menu: _this.props.menu,
			session_data: null
		};
		return _this;
	}

	_createClass(SettingsApplication, [{
		key: 'componentWillReceiveProps',
		value: function componentWillReceiveProps(nextProps) {
			if (nextProps.menu != this.props.menu) {
				this.setState({
					menu: nextProps.menu
				});
			}
		}
	}, {
		key: 'componentWillMount',
		value: function componentWillMount() {
			var _this2 = this;

			__global.session(function (data) {
				_this2.setState({
					session_data: data
				});
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var isAccount = this.state.menu == 'account';
			var isBilling = this.state.menu == 'billing';
			var settings = null;

			if (!this.state.session_data) {
				return _react2.default.createElement(
					'div',
					{ className: 'search-app-container' },
					_react2.default.createElement(
						_generic.SideContainer,
						null,
						_react2.default.createElement(
							'span',
							null,
							'Loading...'
						)
					)
				);
			}

			return _react2.default.createElement(
				'div',
				{ className: 'search-app-container' },
				_react2.default.createElement(
					_generic.SideContainer,
					null,
					_react2.default.createElement(
						'div',
						{ className: 'row accent-color' },
						_react2.default.createElement(
							'div',
							{ className: 'columns six' },
							_react2.default.createElement(
								'a',
								{ href: '#settings-account', className: 'accent' },
								_react2.default.createElement('i', { className: 'fa fa-circle' + (isAccount ? '' : '-o') }),
								' Account'
							)
						),
						_react2.default.createElement(
							'div',
							{ className: 'columns six' },
							_react2.default.createElement(
								'a',
								{ href: '#settings-billing', className: 'accent' },
								_react2.default.createElement('i', { className: 'fa fa-circle' + (isBilling ? '' : '-o') }),
								' Billing'
							)
						)
					),
					_react2.default.createElement('hr', { className: 'hr-subtle' }),
					isAccount ? _react2.default.createElement(AccountSettings, { session_data: this.state.session_data }) : null,
					isBilling ? _react2.default.createElement(BillingSettings, { session_data: this.state.session_data }) : null
				)
			);
		}
	}]);

	return SettingsApplication;
}(_react2.default.Component);

exports.default = SettingsApplication;

var AccountSettings = function (_React$Component2) {
	_inherits(AccountSettings, _React$Component2);

	function AccountSettings(props) {
		_classCallCheck(this, AccountSettings);

		var _this3 = _possibleConstructorReturn(this, (AccountSettings.__proto__ || Object.getPrototypeOf(AccountSettings)).call(this, props));

		_this3.state = {
			session_data: props.session_data
		};

		_this3.onValueChange = _this3.onValueChange.bind(_this3);
		return _this3;
	}

	_createClass(AccountSettings, [{
		key: 'onValueChange',
		value: function onValueChange(event) {
			var data = this.state.session_data;

			data[event.target.name] = event.target.value;

			this.setState({
				session_data: data
			});
		}
	}, {
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				{ className: 'row' },
				_react2.default.createElement(
					'div',
					{ className: 'columns three' },
					_react2.default.createElement(
						'div',
						{ className: 'profile-picture-wrapper' },
						this.props.session_data.profile_picture ? _react2.default.createElement('img', { style: { width: '15vw', height: '15vw', borderRadius: '50%' }, src: this.props.session_data.profile_picture }) : _react2.default.createElement('i', { className: 'fa fa-user-circle white icon', style: { fontSize: '50px' }, 'aria-hidden': 'true' })
					),
					_react2.default.createElement('br', null),
					_react2.default.createElement(
						'a',
						{ className: 'accent', href: 'javascript:;' },
						'Change Profile Picture'
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'columns nine' },
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'four columns no-stack' },
							'Username'
						),
						_react2.default.createElement(
							'div',
							{ className: 'eight columns no-stack' },
							_react2.default.createElement('input', { type: 'text', value: this.props.session_data.username, readOnly: true })
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'four columns no-stack' },
							'Display name'
						),
						_react2.default.createElement(
							'div',
							{ className: 'eight columns no-stack' },
							_react2.default.createElement('input', { type: 'text', name: 'display_name', value: this.state.session_data.display_name, onChange: this.onValueChange })
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'row' },
						_react2.default.createElement(
							'div',
							{ className: 'four columns no-stack' },
							'Email'
						),
						_react2.default.createElement(
							'div',
							{ className: 'eight columns no-stack' },
							_react2.default.createElement('input', { type: 'text', name: 'email', value: this.state.session_data.email, onChange: this.onValueChange })
						)
					)
				)
			);
		}
	}]);

	return AccountSettings;
}(_react2.default.Component);

var BillingSettings = function (_React$Component3) {
	_inherits(BillingSettings, _React$Component3);

	function BillingSettings() {
		_classCallCheck(this, BillingSettings);

		return _possibleConstructorReturn(this, (BillingSettings.__proto__ || Object.getPrototypeOf(BillingSettings)).apply(this, arguments));
	}

	_createClass(BillingSettings, [{
		key: 'render',
		value: function render() {
			return null;
		}
	}]);

	return BillingSettings;
}(_react2.default.Component);

/***/ }),
/* 111 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var GlobalData = function () {
	function GlobalData() {
		_classCallCheck(this, GlobalData);

		this._data = {
			session: null
		};

		this.queued_resolves = {
			session: []
		};

		this.data = this.data.bind(this);
	}

	_createClass(GlobalData, [{
		key: "data",
		value: function data(key) {
			return this._data[key];
		}
	}, {
		key: "session",
		value: function session(resolve) {
			var _this = this;

			if (this._data.session) {
				return resolve(this._data.session);
			} else {
				this.queued_resolves.session.push(resolve);
				if (this.is_getting_session) {
					return;
				}

				this.is_getting_session = true;
				return new Promise(function (resolve, reject) {
					var xhr = new XMLHttpRequest();
					xhr.open("GET", '/account/session_info');
					xhr.onload = function () {
						_this.is_getting_session = false;
						_this._data.session = JSON.parse(xhr.responseText);
						for (var i = 0; i < _this.queued_resolves.session.length; i++) {
							try {
								_this.queued_resolves.session[i](_this._data.session);
							} catch (e) {
								console.error(e);
							}
						}
					};
					xhr.onerror = function () {
						return reject(xhr.statusText);
					};
					xhr.send();
				});
			}
		}
	}]);

	return GlobalData;
}();

exports.default = GlobalData;

/***/ }),
/* 112 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = __webpack_require__(0);

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var IconAudioPlayer = function (_React$Component) {
	_inherits(IconAudioPlayer, _React$Component);

	function IconAudioPlayer(props) {
		_classCallCheck(this, IconAudioPlayer);

		var _this = _possibleConstructorReturn(this, (IconAudioPlayer.__proto__ || Object.getPrototypeOf(IconAudioPlayer)).call(this, props));

		_this.onMenuClick = _this.onMenuClick.bind(_this);
		_this.onProfileClick = _this.onProfileClick.bind(_this);

		_this.state = {
			hash: ''
		};

		_this._menu_refs = [];
		return _this;
	}

	_createClass(IconAudioPlayer, [{
		key: 'onProfileClick',
		value: function onProfileClick(event) {
			window.location.hash = '#settings-profile';

			this.setState({
				hash: '#settings-profile'
			});
		}
	}, {
		key: 'onMenuClick',
		value: function onMenuClick(event) {
			this.setState({
				hash: event.target.href
			});
		}
	}, {
		key: 'componentWillMount',
		value: function componentWillMount() {
			var _this2 = this;

			__global.session(function (data) {
				_this2.setState({
					account_info: data,
					is_loading: false
				});
			});
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			for (var i = 0; i < this._menu_refs.length; i++) {
				var node = this._menu_refs[i];

				node._href = node.href.substring(node.href.indexOf('#'));
				node.href = 'javascript:;';
				// Evergreen event listener || IE8 event listener
				var addEvent = node.addEventListener;

				addEvent("click", this.onMenuMouseClick, false);
			}
		}
	}, {
		key: 'render',
		value: function render() {
			var active = 0;

			if (this.state.hash.indexOf("#news") != -1) {
				active = 1;
			} else if (this.state.hash.indexOf("#settings") != -1) {
				active = 2;
			}

			return _react2.default.createElement(
				'nav',
				null,
				_react2.default.createElement(
					'ul',
					null,
					_react2.default.createElement(
						'li',
						{ className: 'show-mobile' },
						_react2.default.createElement('i', { onClick: this.onProfileClick, className: 'fa fa-user-circle white fa-2x icon', 'aria-hidden': 'true' })
					),
					_react2.default.createElement(
						'li',
						{ className: active == 0 ? 'active' : '' },
						_react2.default.createElement(
							'a',
							{ onClick: this.onMenuClick, href: '#home' },
							'Home'
						)
					),
					_react2.default.createElement(
						'li',
						{ className: active == 1 ? 'active' : '' },
						_react2.default.createElement(
							'a',
							{ onClick: this.onMenuClick, href: '#news' },
							'News'
						)
					),
					_react2.default.createElement(
						'li',
						{ className: (active == 2 ? 'active ' : '') + 'dropdown' },
						_react2.default.createElement(
							'a',
							{ href: 'javascript:;', className: 'dropbtn' },
							'Settings'
						),
						_react2.default.createElement(
							'div',
							{ className: 'dropdown-content' },
							_react2.default.createElement(
								'a',
								{ onClick: this.onMenuClick, href: '#settings-account' },
								'Account'
							),
							_react2.default.createElement(
								'a',
								{ onClick: this.onMenuClick, href: '#settings-billing' },
								'Billing'
							)
						)
					),
					_react2.default.createElement(
						'li',
						{ className: 'dropdown menu-icon' },
						_react2.default.createElement('i', { className: 'fa fa-bars white fa-2x icon', 'aria-hidden': 'true' }),
						_react2.default.createElement(
							'div',
							{ className: 'dropdown-content' },
							_react2.default.createElement(
								'a',
								{ onClick: this.onMenuClick, href: '#home' },
								'Home'
							),
							_react2.default.createElement(
								'a',
								{ onClick: this.onMenuClick, href: '#news' },
								'News'
							),
							_react2.default.createElement(
								'a',
								{ onClick: this.onMenuClick, href: '#settings' },
								'Settings'
							)
						)
					)
				)
			);
		}
	}]);

	return IconAudioPlayer;
}(_react2.default.Component);

exports.default = IconAudioPlayer;

/***/ })
/******/ ]);