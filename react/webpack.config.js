const webpack = require('webpack');
const path = require('path');

module.exports = (env) => {
  const production = env && env.production;
  const plugins = production ? [
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false,
          drop_debugger: true,
        },
        output: { comments: false },
      })
    ] : [];
  plugins.unshift(new webpack.DefinePlugin({
    PRODUCTION: JSON.stringify(production ? true : false),
    DEVELOPMENT: JSON.stringify(!production ? true : false),
  }));

  const output_path = production ?
    path.resolve(__dirname, '../js/react-min') :
    path.resolve(__dirname, 'dist-min');
  return {
    entry: {
      "default-music-player": ['./src/default-music-player.js'],
      "login-form": ['./src/login-form.js'],
      "search-app": ['./src/search-app.js'],
    },
    output: {
      filename: '[name].js',
      chunkFilename: '[id].js',
      path: output_path,
    },
    resolve: {
      extensions: ['.js', '.jsx']
    },
    externals: {
      react: 'React',
      mobx: 'mobx',
      moment: 'moment'
    },
    module: {
      loaders: [{
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ["env", "es2015", "es2016", "react"],
            plugins: ["transform-class-properties"],
            cacheDirectory: path.resolve(__dirname, 'cache/babel-loader'),
            // plugins: [ 'transform-runtime' ],
          }
        }
      }],
    },
    stats: {
      modules: false,
      assetsSort: 'size',
    },
    performance: {
      hints: false,
    },
    plugins: plugins,
  }
};