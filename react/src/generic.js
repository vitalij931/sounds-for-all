export class SideContainer extends React.Component {
	render(){
		return (<div style={Object.assign({}, this.props.style, {padding: '0 1rem 0 1rem'})}>{this.props.children}</div>);
	}
}