export default class GlobalData {
	constructor(){
		this._data = {
			session: null
		};

		this.queued_resolves = {
			session: [],
		}

		this.data = this.data.bind(this);
	}

	data(key){
		return this._data[key];
	}

	session(resolve){
		if (this._data.session) {
			return resolve(this._data.session);
		}else{
			this.queued_resolves.session.push(resolve);
			if (this.is_getting_session) {
				return;
			}

			this.is_getting_session = true;
			return new Promise((resolve, reject) => {
				const xhr = new XMLHttpRequest();
				xhr.open("GET", '/account/session_info');
				xhr.onload = () => {
					this.is_getting_session = false;
					this._data.session = JSON.parse(xhr.responseText)
					for (var i = 0; i < this.queued_resolves.session.length; i++) {
						try{
							this.queued_resolves.session[i](this._data.session)
						} catch (e){
							console.error(e);
						}
					}
				};
				xhr.onerror = () => reject(xhr.statusText);
				xhr.send();
			});
		}
	}
}