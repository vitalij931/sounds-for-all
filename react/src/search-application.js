import React from 'react';
import SmoothCollapse from 'react-smooth-collapse';
import {SideContainer} from './generic';
import {IconAudioPlayer} from './icon-audio-player';

export default class SearchApplication extends React.Component{
	constructor(props){
		super(props);

		const a_client = algoliasearch("RCKCJVSL87", "4114df211bc103af5170f711da975ef7");

		this.state = {
			opacity: 0,
			is_loading : true,
			account_info: null,
			selected_music_result: false,

			search_value: '',
			search_database: null,
			search_results: [],
		};

		this.account_info_request = null
		this.algolia_index = a_client.initIndex('dev_library');

		this.openMusicResult = this.openMusicResult.bind(this)
		this.isLoadingUpdate = this.isLoadingUpdate.bind(this)
		this.onSearchValueChange = this.onSearchValueChange.bind(this)
	}

	openMusicResult(event, music){
		this.setState({
			selected_music_result: music
		})
	}

	isLoadingUpdate(){
		var opacity = this.state.opacity;
		const is_loading = this.account_info_request && this.search_database_request;

		if (!is_loading) {
			opacity = 1;
		}

		this.setState({
			is_loading: is_loading,
			opacity: opacity
		})
	}

	onSearchValueChange(event){
		var query = event.target.value.toLowerCase();
		var search_results = [];

		if (query.length){
			this.setState({
				is_loading: true
			})

			this.algolia_index.search(query).then(data => {
				this.setState({
					search_results : data.hits,
					is_loading: false
				})
			});
		} else
			this.setState({
				search_results: []
			})

		this.setState({
			search_value: event.target.value
		});
	}

	componentWillMount(){
		// this.search_database_request = $.get('/account/search_database', (data) => {
		// 	this.setState({
		// 		search_database: data.data
		// 	})
		// },'json').always(() => {
		// 	this.search_database_request = null;
			this.isLoadingUpdate();
		// })
	}

	componentDidMount(){

	}

	render(){
		return (<div>
			<div className="search-app-container" style={{opacity: this.state.opacity}}>
				<SideContainer>
					<input value={this.state.search_value} style={{
						fontSize: '1.4rem',
						border: 'none',
						outline: 'none',
						width: 'calc(100% - 1.4rem)'
					}} placeholder="Search..." onChange={this.onSearchValueChange}/>
					<i style={{fontSize: '1.4rem', color: '#F05F40'}} className={`fa ${this.state.is_loading ? 'fa-circle-o-notch fa-spin' : 'fa-search'}`} aria-hidden="true"></i>
				</SideContainer>
				<hr/>

				<SideContainer style={{display: 'none'}}>
					<Checkbox label='Long (> 3 mins.)'/>
					<Checkbox label='Mid (< 3 mins.)'/>
					<Checkbox label='Short (< 1 mins.)'/>
				</SideContainer>
				<SmoothCollapse expanded={!!this.state.search_results && !!this.state.search_results.length || !!this.state.selected_music_result}>
					{this.state.selected_music_result ? <BigMusicResult music={this.state.selected_music_result} onBackClick={() => {this.setState({selected_music_result: null})}}/> :
						<SideContainer>
							<br/>
							{this.state.search_results.map((search_result) => {
								return (
									<div key={search_result.id}>
										<MusicResult music={search_result} onResultClick={(event) => {this.openMusicResult(event, search_result)}}/>
									</div>)
							})}
						</SideContainer>}
				</SmoothCollapse>
				{!!this.state.selected_music_result ? null : <SideContainer style={{marginTop: '0.5rem'}}>
					<a href='https://www.algolia.com/'>
						<img className="pull-right" src="https://www.algolia.com/static_assets/images/press/downloads/search-by-algolia.svg" style={{height: '1rem'}}/>
					</a>
				</SideContainer>}
			</div>
		</div>)
	}
}

class ClearFloats extends React.Component {
	render() {
		return <div style={{
					content: "",
					display: 'table',
					clear: 'both'}}/>
	}
}

class BigMusicResult extends React.Component {
	render(){
		const music = this.props.music || {};

		const date = new Date(null);
		date.setSeconds(music.time_length);
		const lengthString = date.toISOString().substr(14, 5);

		return (
			<SideContainer>
				<div onClick={this.props.onBackClick} aria-hidden="true" style={{width: "4rem", cursor: "pointer", margin: '0.5rem 0 0.5rem 0'}}>
					<i className="fa fa-chevron-left fa-2x orange"></i>
					<span className="hidden-small-inline" style={{
						position: 'absolute',
						marginTop: '0.25rem',
						marginLeft: '0.25rem'
					}}>Back</span>
				</div>

				<div>
					{music.cover_photo ? <img style={{verticalAlign: 'middle', display: 'inline-block', width: '4em', height: '4em', borderRadius: '4px'}} src={music.cover_photo}/> : <i className='fa fa-music fa-2x' style={{verticalAlign: 'middle', padding: '16px', width: '1em' ,color: 'gray', background: '#eaeaea', borderRadius: '4px'}}/>}
					<span onClick={this.props.onResultClick} style={{paddingLeft: '1rem'}}>{music.name || <em>Untitled Track</em>}</span>
					<span className="pull-right" style={{marginTop: '1.1rem'}}>{lengthString}</span>
				</div>
				<AudioWithSeek src={music.src}/>
				<b>Description:</b>
				<p>{music.description || <em>No description</em>}</p>
				<p className='pull-right'>Posted by: {music.owner.profile_picture} {music.owner.display_name}</p>
				<ClearFloats/>
				<b>Tags:</b>
				<p>{music.tags && music.tags.length ? music.tags.map((tag, index) => {
					return <span key={index} className="badge">
						{tag}
					</span>
				}) : <em>No tags</em>}</p>
			</SideContainer>);
	}
}

class MusicResult extends React.Component {
	render (){
		const music = this.props.music || {};

		const date = new Date(null);
		date.setSeconds(music.time_length);
		const lengthString = date.toISOString().substr(14, 5);

		return (
			<div>
				{music.cover_photo ? <img style={{verticalAlign: 'middle', display: 'inline-block', width: '32px', height: '32px', borderRadius: '4px'}} src={music.cover_photo}/> : <i onClick={this.props.onResultClick} className='fa fa-music' style={{verticalAlign: 'middle', padding: '8px',color: 'gray', background: '#eaeaea', borderRadius: '4px', width: '16px'}}/>}
				<div style={{display: 'inline'}}>
					<a href='javascript:;' onClick={this.props.onResultClick} style={{paddingLeft: '0.6rem'}}>{music.name || <em>Untitled Track</em>}</a>
					<span className="pull-right" style={{marginTop: '0.5rem'}}>{lengthString}</span>
				</div>
				<AudioWithSeek src={music.src} use_global_player={true}/>
			</div>)
	}
}

class AudioWithSeek extends React.Component {
	constructor(props){
		super(props);

		this.onSeekBarClick = this.onSeekBarClick.bind(this);
		this.onAudioProgress = this.onAudioProgress.bind(this);
		this.onAudioDurationChange = this.onAudioDurationChange.bind(this);
		this.onAudioTimeUpdate = this.onAudioTimeUpdate.bind(this);
		this.onSeekMouseDown = this.onSeekMouseDown.bind(this);
		this.onSeekMouseUp = this.onSeekMouseUp.bind(this);
		this.onSeekMouseMove = this.onSeekMouseMove.bind(this);

		this.state = {
			audio_track_length: 0,
			audio_current_time: 0,
			buffered_progress: 0
		}
	}

	onSeekMouseDown(event){
		this.setState({
			is_mouse_held: true
		})
	}

	onSeekMouseUp(event){
		this.setState({
			is_mouse_held: false
		})
	}

	onSeekMouseMove(event){
		if (this.state.is_mouse_held && event.which == 1) {
			this.onSeekBarClick(event);
		}
	}


	onSeekBarClick(event){
		if (!this._seekbar) {
			return
		}

		const t = this._seekbar;
		const rect = t.getBoundingClientRect();

		const clickedOnWidth = event.pageX - rect.left; // hope to god there is not going to be any horizontal scroll
		const percentageOfWidth = clickedOnWidth / t.clientWidth;

		if (this._audio) {
			const currentTime = this.state.audio_track_length * percentageOfWidth;

			if (!this._audio.audio) {
				this._audio.initializeAudio();
				const onLoaded = (() => {
					this._audio.audio.currentTime = this.state.audio_track_length * percentageOfWidth;
					this._audio.audio.removeEventListener('canplaythrough', onLoaded)
				});

				this._audio.audio.addEventListener('canplaythrough', onLoaded, false)
			}else{
				this._audio.audio.currentTime = currentTime;
			}

			if (this._audio.audio.paused) {
				this._audio.audio.play()
			}

			this.setState({
				audio_current_time: currentTime
			})
		}
	}

	onAudioDurationChange(event) {
		this.setState({
			audio_track_length: event.path[0].duration
		})
	}

	onAudioTimeUpdate(event) {
		this.setState({
			audio_current_time: event.path[0].currentTime
		})
	}

	onAudioProgress(event) {
		const audio = event.path[0];
		const duration =  audio.duration;
		let progresss = this.state.buffered_progress || 0;

		if (duration > 0) {
			for (var i = 0; i < audio.buffered.length; i++) {
				if (audio.buffered.start(audio.buffered.length - 1 - i) < audio.currentTime) {
					progresss = audio.buffered.end(audio.buffered.length - 1 - i)
					break;
				}
			}
		}

		this.setState({
			buffered_progress: progresss
		})
	}

	componentDidMount(){
		$(document).mouseup(this.onSeekMouseUp);
	}

	componentWillUnmount(){
		$(document).unbind('mouseup',this.onSeekMouseUp);
	}

	render(){
		let current_play = 0, buffered = 0;

		if (this.state.audio_track_length) {
			current_play = (this.state.audio_current_time * 100)/ this.state.audio_track_length;
			buffered = (this.state.buffered_progress * 100)/ this.state.audio_track_length;
		}

		return (<div style={{padding: '0.5rem 0 0.5rem 0'}}>
			<IconAudioPlayer ref={(r) => this._audio = r} source={this.props.src} style={{display: 'inline-block', width: '20px'}} ignore_analyser={true} progress={this.onAudioProgress} durationchange={this.onAudioDurationChange} timeupdate={this.onAudioTimeUpdate}/>
			<div className='audio-seek' ref={(r) => this._seekbar = r} onClick={this.onSeekBarClick} onMouseDown={this.onSeekMouseDown} onMouseUp={this.onSeekMouseUp} onMouseMove={this.onSeekMouseMove}>
				<div style={{background: '#F05F40', height: '0.5rem', width: `${current_play}%` }}></div>
				<div style={{background: '#808080', height: '0.5rem', width: `${buffered}%` }}></div>
			</div>
		</div>);
	}
}

class Checkbox extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			isChecked: !!props.isChecked
		}

		this.toggleCheckboxChange = this.toggleCheckboxChange.bind(this)
	}

	toggleCheckboxChange(event){
		this.setState({
			isChecked: event.target.checked
		})
	}

	render(){
		return (<label>
					<input
						type="checkbox"
						value={this.props.label}
						checked={this.state.isChecked}
						onChange={this.toggleCheckboxChange}
					/>
					{this.props.label}
				</label>);
	}
}