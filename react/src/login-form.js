import React from 'react';

export class LoginForm extends React.Component{
	constructor(props){
		super(props);

		this.state = {
			error_message: window.login_error_message,
			is_loading: false,
			is_login: true,
		}

		this.onPageChange = this.onPageChange.bind(this);
		this.onRegisterClick = this.onRegisterClick.bind(this);
	}

	onRegisterClick(){
		this.setState({
			is_login: false,
			password: '',
			error_message: null,
		});
	}

	onPageChange(){
		this.setState({
			is_login: !this.state.is_login
		})
	}

	render(){
		if (this.state.is_login) {
			return (<LoginContent onPageChange={this.onPageChange}/>);
		} else {
			return (<RegisterContent onPageChange={this.onPageChange}/>);
		}
	}
}

class LoginContent extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			password: '',
			username: '',
			remember_me: true,
		}

		this.submit_request = null;

		this.onSubmit = this.onSubmit.bind(this);
		this.handleKeyPress = this.handleKeyPress.bind(this);
		this.onPasswordChange = this.onPasswordChange.bind(this);
		this.onUsernameChange = this.onUsernameChange.bind(this);
		this.onRememberMeChange = this.onRememberMeChange.bind(this);
	}

	onSubmit(){
		if (this.submit_request) {
			return;
		}

		const data = {
			username: this.state.username,
			password: this.state.password,
			ajax: 1
		}

		this.submit_request = $.post('/account/submit', data, (response) => {
			if (response.success) {
				console.log(response);
				console.log('Done!');
				location.href = '/account'
			}else{
				alertify.error(response.message)
			}
		},'json').always(() => {
			this.submit_request = null;
			this.forceUpdate();
		})

		this.forceUpdate();
	}

	handleKeyPress(e){
		if (e.key === 'Enter') {
			this.onSubmit();
		}
	}

	onPasswordChange(e){
		this.setState({
			password : e.target.value
		})
	}

	onUsernameChange(e){
		this.setState({
			username : e.target.value
		})
	}

	onRememberMeChange(e){
		this.setState({
			remember_me : e.target.checked
		})
	}

	componentWillUnmount(){
		if (this.submit_request) {
			this.submit_request.abort();
		}
	}

	render(){
		return (<div>
					<a href="/"><img src="/img/logo.png" className="login-logo img-responsive center-block" data-no-retina=""/></a>

					<div className="padding-top form-small">
						<div className="login-error-message">{this.state.error_message}</div>
						<input className="full-width input-sm" value={this.state.username} onChange={this.onUsernameChange} name='username' type="text" name="login" placeholder="Email/Username" id="" autoFocus=""/><br/>
						<input className="full-width input-sm" value={this.state.password} onKeyPress={this.handleKeyPress} onChange={this.onPasswordChange} style={{marginTop: "10px"}} type="password" placeholder="Password" name="password"/>
						<div className="checkbox" id="login_forgotten">
							<label htmlFor="remember_me">
								<input onChange={this.onRememberMeChange} name="remember_me" value="remember_me" type="checkbox" checked={this.state.remember_me}/>
								<label htmlFor="remember_me">Stay logged in</label>
							</label>
						</div>

						<button onClick={this.onSubmit} disabled={!!this.submit_request} className="btn-primary full-width">{!!this.submit_request ? "Logining in..." : "Login"}</button>
						<hr className="hr-half"/>
					</div>

					<p>Don't have an account? <a href="javascript:;" onClick={this.props.onPageChange}>Register</a></p>
					<a data-target="#forgotPasswordEmailModal" data-toggle="modal">Forgotten your password?</a><br/>
					<a data-target="#rev" data-toggle="modal">Resend email verification</a>
				</div>);
	}
}


class RegisterContent extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			email: '',
			password: '',
			username: '',
			remember_me: true,
		}

		this.submit_request = null;

		this.onSubmit = this.onSubmit.bind(this);
		this.onEmailChange = this.onEmailChange.bind(this);
		this.onPasswordChange = this.onPasswordChange.bind(this);
		this.onUsernameChange = this.onUsernameChange.bind(this);
	}

	onSubmit(){
		if (this.submit_request) {
			return;
		}

		const data = {
			username: this.state.username,
			password: this.state.password,
			email: this.state.email,
			ajax: 1
		}

		this.submit_request = $.post('/account/register',data, (response) => {
			if (response.success) {
				console.log(response);
				window.alertify.success('Registration successful!');
				this.props.onPageChange && this.props.onPageChange();
			}else{
				window.alertify.error(response.message);
			}
		},'json').always(() => {
			this.submit_request = null;
			this.forceUpdate();
		});
		this.forceUpdate();
	}

	onPasswordChange(e){
		this.setState({
			password : e.target.value
		})
	}

	onUsernameChange(e){
		this.setState({
			username : e.target.value
		})
	}

	onEmailChange(e){
		this.setState({
			email : e.target.value
		})
	}

	render(){
		return (<div>
					<a href="/"><img src="/img/logo.png" className="login-logo img-responsive center-block" data-no-retina=""/></a>

					<div className="padding-top form-small">
						<div className="login-error-message">{this.state.error_message}</div>
						<input className="full-width input-sm" value={this.state.username} onChange={this.onUsernameChange} name='username' type="text" name="login" placeholder="Username" autoFocus={true}/><br/>
						<input className="full-width input-sm" value={this.state.email} onChange={this.onEmailChange} style={{marginTop: "10px"}} name='email' type="text" name="login" placeholder="Email"/><br/>
						<input className="full-width input-sm" value={this.state.password} onChange={this.onPasswordChange} style={{marginTop: "10px"}} type="password" placeholder="Password" name="password"/>

						<button onClick={this.onSubmit} disabled={!!this.submit_request} style={{marginTop: "10px"}} className="btn-primary full-width">{!!this.submit_request ? "Registering..." : "Register"}</button>
						<hr className="hr-half"/>
					</div>

					<p>Already have an account? <a href="javascript:;" onClick={this.props.onPageChange}>Login</a></p>
					<a data-target="#rev" data-toggle="modal">Resend email verification</a>
				</div>);
	}
}

ReactDOM.render(
	<LoginForm />,
	document.getElementById('react-login')
);