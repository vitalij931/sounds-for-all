import React from 'react';

export default class IconAudioPlayer extends React.Component{
	constructor(props){
		super(props);

		this.onMenuClick = this.onMenuClick.bind(this);
		this.onProfileClick = this.onProfileClick.bind(this);

		this.state = {
			hash: ''
		}

		this._menu_refs = [];
	}

	onProfileClick(event){
		window.location.hash = '#settings-profile';

		this.setState({
			hash: '#settings-profile'
		})
	}

	onMenuClick(event){
		this.setState({
			hash: event.target.href
		})
	}

	componentWillMount(){
		__global.session((data) => {
			this.setState({
				account_info: data,
				is_loading: false
			})
		})
	}

	componentDidMount(){
		for (var i = 0; i < this._menu_refs.length; i++) {
			const node = this._menu_refs[i];

			node._href = node.href.substring(node.href.indexOf('#'));
			node.href = 'javascript:;';
			// Evergreen event listener || IE8 event listener
			const addEvent = node.addEventListener;

			addEvent("click", this.onMenuMouseClick, false);
		}
	}

	render(){
		let active = 0;

		if (this.state.hash.indexOf("#news") != -1) {
			active = 1;
		}else if(this.state.hash.indexOf("#settings") != -1){
			active = 2;
		}

		return (
			<nav>
				<ul>
					<li className="show-mobile">
						<i onClick={this.onProfileClick} className="fa fa-user-circle white fa-2x icon" aria-hidden="true"></i>
					</li>
					<li className={active == 0 ? 'active' : ''}><a onClick={this.onMenuClick} href="#home">Home</a></li>
					<li className={active == 1 ? 'active' : ''}><a onClick={this.onMenuClick} href="#news">News</a></li>
					<li className={`${active == 2 ? 'active ' : ''}dropdown`}>
						<a href="javascript:;" className="dropbtn">Settings</a>
						<div className="dropdown-content">
							<a onClick={this.onMenuClick} href="#settings-account">Account</a>
							<a onClick={this.onMenuClick} href="#settings-billing">Billing</a>
						</div>
					</li>
					<li className='dropdown menu-icon'>
						<i className="fa fa-bars white fa-2x icon" aria-hidden="true"></i>
						<div className="dropdown-content">
							<a onClick={this.onMenuClick} href="#home">Home</a>
							<a onClick={this.onMenuClick} href="#news">News</a>
							<a onClick={this.onMenuClick} href="#settings">Settings</a>
						</div>
					</li>
				</ul>
			</nav>);
	}
}