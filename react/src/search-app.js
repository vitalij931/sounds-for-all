window.__global = new GlobalData();

import React from 'react';
import SearchApplication from './search-application';
import SettingsApplication from './settings-application';
import GlobalData from './global-data';
import TopBar from './top-bar';

let totalHashListeners = 0;
const hashListeners = {
	late: [],
	normal: []
}
const contentHashes = {
	default: (hash) => {
		return  <SearchApplication />
	},
	settings: (hash) => {
		return <SettingsApplication menu={hash.indexOf('-') != -1 ? hash.substring(hash.indexOf('-')+1) : 'account'}/>
	}
}


const onHashChange = () => {
	const hash = location.hash.substring(1);
	const indexOfDash = hash.indexOf('-');
	const command = indexOfDash == -1 ? hash : hash.substring(0, indexOfDash);

	for (var i = 0; i < hashListeners.normal.length; i++) {
		let l = hashListeners.normal[i]
		if (typeof l === 'function') {
			try{
				l();
			} catch(e){
				console.error(e);
			}
		}
	}

	if (command in contentHashes) {
		ReactDOM.render(
			contentHashes[command](hash),
			document.getElementById('content-react')
		);
	} else {
		ReactDOM.render(
			contentHashes.default(hash),
			document.getElementById('content-react')
		);
	}

	for (var i = 0; i < hashListeners.late.length; i++) {
		let l = hashListeners.late[i]
		if (typeof l === 'function') {
			try{
				l();
			} catch(e){
				console.error(e);
			}
		}
	}
}

window.addEventListener("hashchange", onHashChange, false);

/**
 * Adds an onHash change listener for Search App.
 *
 * @param  {function} listener listener to be execute on hash change
 * @param  {bool} 	  late     If truthy the listener will be executed after ReactDOM render, else otherwise
 */
window.addOnApplicationHashChange = (listener, late) => {
	totalHashListeners++;

	if (totalHashListeners > 19) {
		console.warn("There is a total of " + totalHashListeners + " hash listeners added right now.");
	}

	hashListeners[!late ? 'normal' : 'late'].push(listener);
}

/**
 * Removes an onHash change listener for Search App.
 *
 * @param  {function} listener listener that was executed on hash change
 * @param  {bool} 	  late     If truthy a late onHash listener will be removed, else otherwise
 */
window.removeOnApplicationHashChange = (listener, late) => {
	const listeners = hashListeners[!late ? 'normal' : 'late'];

	for (var i = 0; i < listeners.length; i++) {
		let l = listeners[i];

		if (l == listener) {
			totalHashListeners--;
			return hashListeners[!late ? 'normal' : 'late'].splice(i, 1);
		}
	}
	return false;
}

onHashChange();


ReactDOM.render(
	<TopBar />,
	document.getElementById('top-bar-react'))