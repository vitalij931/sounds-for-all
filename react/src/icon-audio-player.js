import React from 'react';

const safety_styles = <style>{`
	.safety-bar {
		animation: sound 0ms -800ms linear infinite alternate;
	}
`}</style>

export class IconAudioPlayer extends React.Component{
	constructor(props){
		super(props);

		this.play = this.play.bind(this);
		this.onClick = this.onClick.bind(this);
		this.renderFrame = this.renderFrame.bind(this);
		this.initializeAudio = this.initializeAudio.bind(this);

		this.state = {
			use_safety_styles: !window.AudioContext || /Mobi/.test(navigator.userAgent),
			is_buffering: false,
		}
	}

	initializeAudio(source){
		if (!source)
			source = this.props.source

		this.audio = typeof source === 'object' ? source : new Audio(source);
		this.audio.onended = (e) => {
			this.forceUpdate();
			this.props.onEnded && this.props.onEnded(e, this);
		}

		this.props.durationchange && this.audio.addEventListener('durationchange', this.props.durationchange);
		this.props.progress && this.audio.addEventListener('progress', this.props.progress);
		this.props.timeupdate && this.audio.addEventListener('timeupdate', this.props.timeupdate);

		if (!this.props.ignore_analyser) {
			try {
				var ctx = new AudioContext();
				this.audioSrc = ctx.createMediaElementSource(this.audio);
				this.analyser = ctx.createAnalyser();
				// we have to connect the MediaElementSource with the analyser
				this.audioSrc.connect(this.analyser);
				this.audioSrc.connect(ctx.destination);
				// we could configure the analyser: e.g. analyser.fftSize (for further infos read the spec)

				// frequencyBinCount tells you how many values you'll receive from the analyser
				this.frequencyData = new Uint8Array(this.analyser.frequencyBinCount);
			} catch(ignored) {}
		}
	}

	play(){
		this.audio && this.audio.play() && this.forceUpdate();
	}

	componentWillReceiveProps(nextProps){
		if (nextProps.source && nextProps.source != this.props.source) {
			const was_paused = this.audio.paused;
			this.audio && this.audio.pause();
			this.initializeAudio(nextProps.source)

			if (!was_paused) {
				this.audio.play()
			}
		}
	}

	renderFrame(){
		if (this.state.use_safety_styles) {
			return;
		}

		const length = 400;
		const barCount = 10;
		const medium = Math.floor(length/barCount);

		if (!this.audio || this.audio.paused) {
			for (var i = 0; i < barCount; i++){
				var el = this.frequencyData[i*medium + 50];
				var dom = $('#bars .bar').get(i);

				$(dom).css({height: '1vh', opacity: 1});
			}
			return;
		}

		this.analyser.getByteFrequencyData(this.frequencyData);

		for (var i = 0; i < barCount; i++){
			var el = this.frequencyData[i*medium + 50];
			var dom = $('#bars .bar').get(i);
			var floatVal = el/255;

			$(dom).css({height: (floatVal * 30) + 'vh', opacity: floatVal*0.5 + 0.5});
		}

		requestAnimationFrame(this.renderFrame);
	}

	onClick(e){
		if (!this.audio) {
			this.initializeAudio(this.props.source);
		}

		if (this.audio.paused) {
			this.audio.play();

			if (!this.props.ignore_analyser)
				requestAnimationFrame(this.renderFrame);
		}else {
			this.audio.pause();
		}

		this.forceUpdate();
	}

	componentWillUnmount(){
		if (this.audio) {
			this.props.durationchange && this.audio.removeEventListener('durationchange', this.props.durationchange);
			this.props.progress && this.audio.removeEventListener('progress', this.props.progress);
			this.props.timeupdate && this.audio.removeEventListener('timeupdate', this.props.timeupdate);
			this.audio.src = '';
		}
	}

	render(){
		const is_paused = this.audio ? this.audio.paused : true;

		return (
			<div className="icon-audio-player" style={this.props.style}>
				{this.state.use_safety_styles && !is_paused ? safety_styles : null}
				<i onClick={this.onClick} className={`fa ${is_paused ? 'fa-play' : 'fa-pause'}`} aria-hidden="true"></i>
			</div>);
	}
}