import React from 'react';
import {SideContainer} from './generic';

export default class SettingsApplication extends React.Component{
	constructor(props){
		super(props);

		this.state = {
			menu : this.props.menu,
			session_data: null
		}
	}

	componentWillReceiveProps(nextProps){
		if (nextProps.menu != this.props.menu) {
			this.setState({
				menu : nextProps.menu
			})
		}
	}

	componentWillMount(){
		__global.session((data) => {
			this.setState({
				session_data: data
			})
		})
	}

	render(){
		const isAccount = this.state.menu == 'account';
		const isBilling = this.state.menu == 'billing';
		let settings = null;

		if (!this.state.session_data) {
			return <div className="search-app-container">
				<SideContainer>
					<span>Loading...</span>
				</SideContainer>
			</div>
		}

		return (
			<div className="search-app-container">
				<SideContainer>
					<div className='row accent-color'>
						<div className="columns six"><a href='#settings-account' className="accent"><i className={`fa fa-circle${isAccount ? '' : '-o'}`}/> Account</a></div>
						<div className="columns six"><a href='#settings-billing' className="accent"><i className={`fa fa-circle${isBilling ? '' : '-o'}`}/> Billing</a></div>
					</div>
					<hr className="hr-subtle"/>
					{isAccount ? <AccountSettings session_data={this.state.session_data}/> : null}
					{isBilling ? <BillingSettings session_data={this.state.session_data}/> : null}
				</SideContainer>
			</div>);
	}
}

class AccountSettings extends React.Component{
	constructor(props){
		super(props);

		this.state = {
			session_data : props.session_data
		}

		this.onValueChange = this.onValueChange.bind(this)
	}

	onValueChange(event){
		const data = this.state.session_data;

		data[event.target.name] = event.target.value;

		this.setState({
			session_data: data
		});
	}


	render(){
		return <div className='row'>
					<div className='columns three'>
						<div className='profile-picture-wrapper'>
							{this.props.session_data.profile_picture ? <img style={{width: '15vw', height: '15vw', borderRadius: '50%'}} src={this.props.session_data.profile_picture}/> : <i className="fa fa-user-circle white icon" style={{fontSize: '50px'}} aria-hidden="true"></i>}
						</div>
						<br/>
						<a className='accent' href='javascript:;'>Change Profile Picture</a>
					</div>
					<div className='columns nine'>
						<div className='row'>
							<div className='four columns no-stack'>
								Username
							</div>
							<div className='eight columns no-stack'>
								<input type='text' value={this.props.session_data.username} readOnly={true}/>
							</div>
						</div>
						<div className='row'>
							<div className='four columns no-stack'>
								Display name
							</div>
							<div className='eight columns no-stack'>
								<input type='text' name='display_name' value={this.state.session_data.display_name} onChange={this.onValueChange}/>
							</div>
						</div>
						<div className='row'>
							<div className='four columns no-stack'>
								Email
							</div>
							<div className='eight columns no-stack'>
								<input type='text' name='email' value={this.state.session_data.email} onChange={this.onValueChange}/>
							</div>
						</div>
					</div>
				</div>
	}
}

class BillingSettings extends React.Component{
	render(){
		return null;
	}
}