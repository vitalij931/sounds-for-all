import React from 'react';
import {IconAudioPlayer} from './icon-audio-player'

class DefaultIconAudioPlayer extends React.Component{
	constructor(props){
		super(props);

		this.onPlay = this.onPlay.bind(this);
		this.onEnded = this.onEnded.bind(this);
		this.onForward = this.onForward.bind(this);
		this.onBackward = this.onBackward.bind(this);

		this.state = {
			currentIndex: 0
		}
	}

	onPlay(){

	}

	onEnded(e, ref){
		this.setState({
			currentIndex: (this.state.currentIndex + 1)%4
		}, () => {
			ref.play()
		})
	}

	onForward(){
		this.setState({
			currentIndex: (this.state.currentIndex + 1)%4
		})
	}

	onBackward(){
		this.setState({
			currentIndex: (this.state.currentIndex == 0 ? 3 : this.state.currentIndex - 1)%4
		})
	}

	render(){
		return (
			<div>
				<div className="default-icon-audio-player">
					<i onClick={this.onBackward} className="fa fa-backward" aria-hidden="true"></i>
					<IconAudioPlayer onEnded={this.onEnded} source={`assets/2-${this.state.currentIndex + 1}.mp3`}/>
					<i onClick={this.onForward} className="fa fa-forward" aria-hidden="true"></i>
				</div>
			</div>);
	}
}

ReactDOM.render(
	<DefaultIconAudioPlayer />,
	document.getElementById('react-music')
);