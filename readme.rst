###################
What is this?
###################

This a small website for a shop that I was thinking of making for myself. It has some nice features, ie dynamic canvas background, full screen equalizer.
Quite a bit was focused on the design and how everything looks seamlessly so you may encounter some bugs here and there.

There is an example of the website running on https://vitalij.tech

\ 

###################
How do I deploy it?
###################

Running this script on centos or on an ec2 instance should do the trick:


``curl -sSL https://s3-eu-west-1.amazonaws.com/vitalij-public/sounds_for_all.sh | sh``


Once that is done, please look at the `Setting up the database` section.

In case the above fails here is what you need to deploy from scratch:

1. Install LAMP stack on your machine.
2. Make sure .htaccess files can run within the websites directory. Check ``Enable .htaccess`` here https://www.linode.com/docs/web-servers/apache/how-to-set-up-htaccess-on-apache
3. Prepare a MySQL server with credentials, see `Setting up the database`.
4. Run the server, make sure your server has all the necessary open ports ie 80.
 
\ 
 
###################
Setting up the database
###################

The application is configured to run on a MySQL server with the following credentials:

* Username: ``sounds_for_all``
* Password: ``sounds_for_all_password``

It uses ``sounds_for_all`` database. Make sure the user has read and write access to it.

All of the settings above can be changed in ``/application/config/database.php``

For first time setup, run the ``database_structure.sql`` file in your database. This will create the database and tables necessary for the application to run.

If the database is not running on ``localhost:3306`` you can change `hostname` value in ``/application/config/database.php``

Good luck and happy snooping.