<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LMEvent extends CI_Controller {

	// public function index(){
		
	// }

	public function update(){
		$data = json_decode($this->input->raw_input_stream);

		$this->load->model('M_lm_event','lm_event');

		$response = $this->lm_event->update();

		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}
}

/* End of file LMEvent.php */
/* Location: ./application/controllers/LMEvent.php */