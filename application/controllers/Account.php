<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct(){
		parent::__construct();

		// $this->load->model('M_account_management','account_management');
		// $this->load->model('M_facebook_session','facebook_session');
		$this->load->model('M_account','account');

	}

	public function index(){
		$logged_in = $_SESSION['logged_in'] ?? FALSE;

		if ($logged_in) {
			$this->load->view('V_search_app');
		} else {
			redirect('/login','refresh');
		}
	}

	public function login(){
		$this->load->view('V_login');
	}

	public function submit(){
		$response = $this->account->login($this->input->post('username'), $this->input->post('password'));

		if ($this->input->get_post('ajax')) {
			$this->output->set_content_type('application/json')->set_output(json_encode($response));
		} else {
			if ($response['success']) {
				redirect('/app','refresh');
			}else{
				$this->session->set_flashdata('login_error_message', $response['message']);

				redirect("/login",'refresh');
			}
		}
	}

	public function register(){
		$response = $this->account->register($this->input->post('username'), $this->input->post('email'), $this->input->post('password'));

		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}

	public function session_info(){
		$data = $_SESSION;

		unset($data['__ci_last_regenerate']);

		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	public function search_database(){
		$this->load->model('M_music');

		$response = $this->M_music->search_database();

		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}

	// For some sort of app
	// public function login(){
	// 	$data = json_decode($this->input->raw_input_stream,true);
	// 	$access_token = $data->access_token;

	// 	$response = $this->account_management->facebook_login($access_token);

	// 	$this->output->set_content_type('application/json')->set_output(json_encode($response));
	// }

	// public function create_event(){
	// 	$data = json_decode($this->input->raw_input_stream,true);

	// 	$response = $this->account_management->create_event($data['list']);

	// 	$this->output->set_content_type('application/json')->set_output(json_encode($response));
	// }

}

/* End of file Account.php */
/* Location: ./application/controllers/Account.php */