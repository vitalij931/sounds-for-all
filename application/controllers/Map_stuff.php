<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map_stuff extends CI_Controller {

	
	public function get_friends_markers(){
		$stream_clean = $this->security->xss_clean($this->input->raw_input_stream);
		$request = json_decode($stream_clean, TRUE);

		// $response = [
		// 	'success' => TRUE,
		// 	'data' => [
		// 		[
		// 			'long' => -4.220777750015,
		// 			'lat' => 55.8641363959
		// 		],
		// 		[
		// 			'long' => -4.220777750015,
		// 			'lat' => 55.8661363959
		// 		],
		// 		[
		// 			'long' => -4.220777750015,
		// 			'lat' => 55.8641363959
		// 		],
		// 		[
		// 			'long' => -4.223777750015,
		// 			'lat' => 55.8641363959
		// 		]
		// 	]
		// ];

		$this->load->model('M_map_stuff','map_stuff');

		$response = $this->map_stuff->get_friends_markers($request['facebook_access_token']);

		if (!empty($request['current_location'])) {
			// $response['data'][] = [
			// 	'long' => $request['current_location']['longitude'],
			// 	'lat' => $request['current_location']['latitude']
			// ];
			$this->map_stuff->set_current_marker($request['current_location']);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}
}

/* End of file MapStuff.php */
/* Location: ./application/controllers/MapStuff.php */