<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<meta property="og:image" content="" />

	<title><?=APPLICATION_TITLE?></title>

	<!-- Bootstrap Core CSS -->
	<link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

	<!-- Favicon -->
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="/img/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/img/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/img/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/img/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="/img/favicon/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/img/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="/img/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="/img/favicon/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="/img/favicon/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="/img/favicon/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="/img/favicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="/img/favicon/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="/img/favicon/favicon-128.png" sizes="128x128" />

	<!-- Plugin CSS -->
	<link href="/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

	<!-- Theme CSS -->
	<link href="/css/creative.min.css" rel="stylesheet">

	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/css/alertify.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body class="black">

	<nav hidden id="mainNav" class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
				</button>
				<a class="navbar-brand page-scroll" href="#page-top">Sounds for all</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="/login" class="btn btn-primary page-scroll btn-login" style="padding: 8px;margin: 7px;border-radius: 8px;">Login</a>
					</li>
					<li>
						<a class="page-scroll" href="#about">Services</a>
					</li>
					<li>
						<a class="page-scroll" href="#services">Features</a>
					</li>
					<li>
						<a class="page-scroll" href="#pricing">Pricing</a>
					</li>
					<li>
						<a class="page-scroll" href="#contact">Contact</a>
					</li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>

	<div id="react-login" class="container text-center"><div>

	<script>
		window.login_error_message = <?=isset($_SESSION['login_error_message']) ? '"' . $_SESSION['login_error_message'] . '"' : 'null'?>;
	</script>
	<style>
		.btn-login{
			background-color: #F05F40 !important;
			color: #fff !important;
		}

		.btn-login:hover{
			background-color: #dadada !important;
			color: #000 !important;
		}
	</style>

	<!-- jQuery -->
	<script src="/vendor/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script src="/vendor/scrollreveal/scrollreveal.min.js"></script>
	<script src="/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- Theme JavaScript -->
	<script src="js/creative.min.js"></script>
	<!-- <script src="js/react.min.js"></script> -->
	<!-- <script src="js/react-dom.min.js"></script> -->
	<script src="/js/alertify.js"></script>

	<script src="https://unpkg.com/react@15.5.4/dist/react.js"></script>
	<script src="https://unpkg.com/react-dom@15.5.4/dist/react-dom.js"></script>

	<script src="/react/dist-min/login-form.js?_=<?=filemtime('react/dist-min/login-form.js')?>" type="text/javascript"></script>
</body>

</html>