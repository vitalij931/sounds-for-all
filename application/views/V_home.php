<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<meta property="og:image" content="" />

	<title><?=APPLICATION_TITLE?></title>

	<!-- Bootstrap Core CSS -->
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

	<!-- Favicon -->
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="img/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="img/favicon/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="img/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="img/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="img/favicon/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="img/favicon/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="img/favicon/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="img/favicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="img/favicon/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="img/favicon/favicon-128.png" sizes="128x128" />

	<!-- Plugin CSS -->
	<link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

	<!-- Theme CSS -->
	<link href="css/creative.min.css" rel="stylesheet">
	<link rel="stylesheet" href="/css/style.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<style>
		#bars {
			height: 10vh;/*30px;*/
			/*left: 50%;*/
			/*margin: -30px 0 0 -20px;*/
			position: absolute;
			/*top: 50%;*/
			/*width: 40px;*/
			left: 5vw;
			width: 90vw;
			bottom: 0;
		}

		.bar {
			background: #F05F40;
			bottom: 0px;
			height: 3px;
			position: absolute;
			width: 8vw;
			transition: height 0.05s linear;
			/*animation: sound 0ms -800ms linear infinite alternate;*/
		}

		@keyframes sound {
			0% {
				 opacity: .35;
				height: 3px;
			}
			100% {
				opacity: 1;
				height: 10vh;
			}
		}

		#bars .bar:nth-child(1)	{ left: 1vw;	animation-duration: 474ms; }
		#bars .bar:nth-child(2)	{ left: 10vw; animation-duration: 433ms; }
		#bars .bar:nth-child(3)	{ left: 19vw; animation-duration: 407ms; }
		#bars .bar:nth-child(4)	{ left: 28vw; animation-duration: 458ms; }
		#bars .bar:nth-child(5)	{ left: 37vw; animation-duration: 400ms; }
		#bars .bar:nth-child(6)	{ left: 46vw; animation-duration: 427ms; }
		#bars .bar:nth-child(7)	{ left: 55vw; animation-duration: 441ms; }
		#bars .bar:nth-child(8)	{ left: 64vw; animation-duration: 419ms; }
		#bars .bar:nth-child(9)	{ left: 73vw; animation-duration: 487ms; }
		#bars .bar:nth-child(10) { left: 82vw; animation-duration: 442ms; }​
	</style>

	<style type="text/css">
		.default-icon-audio-player{
			width: fit-content;
			border: 2px solid white;
			border-radius: 8px;
			padding: 8px;
		}
		.icon-audio-player{
			display: inline;
			text-align: center;
			margin: 1rem;
		}

		.icon-audio-player i{
			width: 39px;
			height: 39px;
		}

		div#react-music{
			padding-top: 50px;
			font-size: 3rem;
			width: 135px;
			margin: 0 auto;
		}

		th {
			text-align: center !important;
		}

		.list-item {
			padding: 8px;
			background: #fff;
			border-radius: 4px;
			color: black;
		}

		ul.features-list{
			padding: 8px;
		}

		ul.features-list li {
			list-style: none;
			margin-bottom: 8px;
			text-align: left;
			padding-left: 28px;
		}

		ul.features-list li:before {
			float: left;
			display: inline-block;
			background-image: url('img/checkbox.svg');
			background-size: 20px 20px;
			width: 20px;
			height: 20px;
			content: "";
			margin-left: -28px;
		}

		ul.features-list li.text-primary:before {
			background-image: url('img/checkbox_primary.svg');
		}

		ul.features-list li.disabled-feature:before {
			color: #adadad;
			background-image: url('img/disabled_checkbox.svg');
		}

		@media (max-width: 1200px){
			.list-item{
				margin-bottom: 20px;
				width: 33%;
				margin-left: 33.5% !important;
			}
		}

		@media (max-width: 992px){
			.list-item{
				margin-bottom: 20px;
				width: 50%;
				margin-left: 25% !important;
			}
		}

		@media (max-width: 768px){
			.list-item{
				margin-bottom: 20px;
				width: 70%;
				margin-left: 15% !important;
			}
		}
	</style>
</head>

<body id="page-top">

	<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
				</button>
				<a class="navbar-brand page-scroll" href="#page-top"><?=APPLICATION_TITLE?></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="/login" class="btn btn-primary page-scroll btn-login" style="padding: 8px;margin: 7px;border-radius: 8px;">Login</a>
					</li>
					<li>
						<a class="page-scroll" href="#about">Services</a>
					</li>
					<li>
						<a class="page-scroll" href="#services">Features</a>
					</li>
					<li>
						<a class="page-scroll" href="#pricing">Pricing</a>
					</li>
					<li>
						<a class="page-scroll" href="#contact">Contact</a>
					</li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>


	<header style="background-image: none; background: #000">
		<div id="bars">
			<div class="bar safety-bar"></div>
			<div class="bar safety-bar"></div>
			<div class="bar safety-bar"></div>
			<div class="bar safety-bar"></div>
			<div class="bar safety-bar"></div>
			<div class="bar safety-bar"></div>
			<div class="bar safety-bar"></div>
			<div class="bar safety-bar"></div>
			<div class="bar safety-bar"></div>
			<div class="bar safety-bar"></div>
		</div>
		<div class="header-content">
			<div class="header-content-inner">
				<h1 id="homeHeading">Music for every moment</h1>
				<hr>
				<p>Here you can get music and soundtracks for all your digital needs</p>
				<a href="#about" class="btn btn-primary btn-xl page-scroll">Find Out More</a><br/>
				<div id="react-music"></div>
			</div>
		</div>
	</header>

	<section class="bg-primary" id="about">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 text-center">
					<h2 class="section-heading">We've got what you need!</h2>
					<hr class="light">
					<p class="text-faded">Having the right song or background music can make or break a video. Download any soundtrack from our library and enhance your vlogs, movies, holidays and even your morning coffee trips with our royalty free music!</p>
					<a href="#services" class="hidden page-scroll btn btn-default btn-xl sr-button">Get Started!</a>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="text-secondary fa fa-4x fa-video-camera text-primary sr-icons hover-zoom"></i>
						<h3 style="min-height: 53px">Background or foreground</h3>
						<p class="text-faded">Drag &amp; drop our songs as background or foreground music in your vlogs, podcasts, ads, intros and much more.</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="text-secondary fa fa-4x fa-youtube-square text-primary sr-icons hover-zoom"></i>
						<h3 style="min-height: 53px">Music for your youtube videos</h3>
						<p class="text-faded">Our music is copyright free and suitable for all social platforms. Never get audio muted in your videos or any copyright issues.</p><br/>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="text-secondary fa fa-4x fa-calendar-o text-primary sr-icons hover-zoom"></i>
						<h3 style="min-height: 53px">Event ready</h3>
						<p class="text-faded">Music made for different occasions whether it's a gaming or sports event or your wedding.</p><br/>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="text-secondary fa fa-4x fa-headphones text-primary sr-icons hover-zoom"></i>
						<h3 style="min-height: 53px">Music on demand</h3>
						<p class="text-faded">If you can't find a song matching your style or your needs, artists will make or change a track for you.</p><br/>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="services">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading">Our features</h2>
					<hr class="primary">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="fa fa-4x fa-diamond text-primary sr-icons hover-zoom"></i>
						<h3>High quality</h3>
						<p class="text-muted">Music is always available in WAV and high quality 320kbps MP3 formats.</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="fa fa-4x fa-random text-primary sr-icons hover-zoom"></i>
						<h3>Any genre, any style</h3>
						<p class="text-muted">Our library grows daily and supports more genres and styles.</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="fa fa-4x fa-cloud-download	text-primary sr-icons hover-zoom"></i>
						<h3>Cloud storage</h3>
						<p class="text-muted">Downlaod music any time, any where, fast and simple</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="fa fa-4x fa-file-audio-o text-primary sr-icons hover-zoom"></i>
						<h3>Multiple versions</h3>
						<p class="text-muted">Get a 5 second professional cut or a 3 minute version of the same track.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="bg-primary" id="pricing">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1 text-center">
					<h2 class="section-heading">Pricing</h2>
					<hr class="light">
					<div class="col-lg-3 list-item hover-zoom sr-icons">
						<div class="list-content">
							<h3>Free</h3>
							<span class="pricing" style="height: 79px;">
								<h4>
									£0.00 <span class="month"> / month</span>
								</h4>
								<h6 style="opacity: 0; color: #adadad; text-decoration: line-through">
									£0.00 <span class="month"> / month</span>
								</h6>
							</span>
							<hr class="primary" style="max-width: 60%">
							<div class="features">
								<ul class="features-list">
									<li>5 downlaods</li>
									<li class="disabled-feature">Commercial use</li>
									<li class="disabled-feature">Ad free</li>
									<li class="disabled-feature">High quality audio</li>
									<li class="disabled-feature">Access for multiple people</li>
								</ul>
							</div>
							<a href="/uk/download/" class="btn btn-green js-goto-signup js-button-module-get-free" id="plan-get-free-button" data-tracking="{&quot;category&quot;: &quot;home (subscriptions)&quot;, &quot;action&quot;: &quot;free&quot;}">Get Free
							</a>
						</div>
					</div>

					<div class="col-lg-3 list-item hover-zoom sr-icons" style="margin-left: 12.5%">
						<div class="list-content">
							<h3>Premium</h3>
							<span class="pricing" style="height: 79px;">
								<h4>
									£4.99 <span class="month"> / month</span>
								</h4>
								<h6 style="color: #adadad; text-decoration: line-through">
									£7.99 <span class="month"> / month</span>
								</h6>
							</span>
							<hr class="primary" style="max-width: 60%">
							<div class="features">
								<ul class="features-list">
									<li>10 downlaods/month</li>
									<li>Commercial use</li>
									<li>Ad free</li>
									<li>High quality audio</li>
									<li class="disabled-feature">Access for multiple people</li>
								</ul>
							</div>
							<a href="/uk/download/" class="btn btn-green js-goto-signup js-button-module-get-free" id="plan-get-free-button" data-tracking="{&quot;category&quot;: &quot;home (subscriptions)&quot;, &quot;action&quot;: &quot;free&quot;}">Get Plan
							</a>
						</div>
					</div>

					<div class="col-lg-3 list-item hover-zoom sr-icons" style="margin-left: 12.5%">
						<div class="list-content">
							<h3>Contributor</h3>
							<span class="pricing" style="height: 79px;">
								<h4>
									£7.99 <span class="month"> / month</span>
								</h4>
								<h6 style="color: #adadad; text-decoration: line-through">
									£14.99 <span class="month"> / month</span>
								</h6>
							</span>
							<hr class="primary" style="max-width: 60%">
							<div class="features">
								<ul class="features-list">
									<li class="text-primary">Unlimited downlaods</li>
									<li>Commercial use</li>
									<li>Ad free</li>
									<li>High quality audio</li>
									<li>Access for multiple people</li>
								</ul>
							</div>
							<a href="/uk/download/" class="btn btn-green js-goto-signup js-button-module-get-free" id="plan-get-free-button" data-tracking="{&quot;category&quot;: &quot;home (subscriptions)&quot;, &quot;action&quot;: &quot;free&quot;}">Get Plan
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<aside hidden class="bg-dark">
		<div class="container text-center">
			<div class="call-to-action">
				<h2>Ready to start?</h2>
				<a href="login" class="btn btn-default btn-xl sr-button">Start Now!</a>
			</div>
		</div>
	</aside>

	<section id="contact">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 text-center">
					<h2 class="section-heading">Let's Get In Touch!</h2>
					<hr class="primary">
					<p>Collaboration? Need help? No worries! Send us an email and we will get back to you as soon as possible!</p>
				</div>
				<div class="col-lg-4 col-lg-offset-2 text-center" hidden>
					<i class="fa fa-phone fa-3x sr-contact"></i>
					<p>123-456-6789</p>
				</div>
				<div class="col-sm-12 text-center">
					<i class="fa fa-envelope-o fa-3x sr-contact"></i>
					<p><a href="mailto:your-email@your-domain.com">vitalij@vitalij.tech</a></p>
				</div>
			</div>
		</div>
	</section>

	<style>
		.btn-login{
			background-color: #F05F40 !important;
			color: #fff !important;
		}

		.btn-login:hover{
			background-color: #dadada !important;
			color: #000 !important;
		}
	</style>

	<!-- jQuery -->
	<script src="vendor/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
	<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- Theme JavaScript -->
	<script src="js/creative.min.js"></script>
	<!-- <script src="js/react.min.js"></script> -->
	<!-- <script src="js/react-dom.min.js"></script> -->

	<script src="https://unpkg.com/react@15.5.4/dist/react.js"></script>
	<script src="https://unpkg.com/react-dom@15.5.4/dist/react-dom.js"></script>

	<script src="react/dist-min/default-music-player.js" type="text/javascript"></script>
</body>

</html>