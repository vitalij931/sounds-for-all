<!DOCTYPE html>
<html>
<head>
	<title><?=APPLICATION_TITLE?></title>
	<link href="https://fonts.googleapis.com/css?family=Montserrat%20Regular" rel="stylesheet">
	<style>
		body.app{
			background: white;
			margin: 0;
			padding:0;
			width:100%;
			height:100%;

			font-family: 'Montserrat', sans-serif;
		}

		.content-wraper {
			position: absolute;
			top: 0;
			right: 0;
			left: 0;
			bottom: 0;
		}

		#background{
			background: black;
			width: 100vw;
			height: 100vh;
		}

		#search_app_react {
			position: absolute;
			top: 10vh;
		}

		canvas { display:block; position: fixed;} /* To remove the scrollbars */

		.search-app-container {
			position: absolute;
			top: 60px;
			margin-left: auto;
			margin-right: auto;
			left: 0;
			right: 0;
			padding: 1rem 0 1rem 0;
			width: 50vw;
			border-radius: 0.5rem;
			border: solid #F05F40;
			background: white;
			transition: opacity .6s;
		}

		@media screen and (max-width: 650px) {
			.search-app-container {
				top: 48px;
				width: calc(100% - 0.5rem);
				border-radius: 0.5rem;
			}
		}

		hr{
			border-color: #F05F40
		}

		.audio-seek {
			border-radius: 2rem;
			width: calc(100% - 20px);
			background: #9a9aa9;
			height: 0.5em;
			overflow: hidden;
			display: inline-block;
		}

		.badge {
			display: inline-block;
			min-width: 10px;
			padding: 2px 3px 3px;
			font-size: 11px;
			font-weight: 700;
			line-height: 1;
			color: #fff;
			text-align: center;
			white-space: nowrap;
			background-color: #777;
			border-radius: 4px;
			margin-right: 0.5rem;
		}

		.white {
			color: white;
		}

		.hidden-small {
			display: block !important;
		}

		.hidden-small-inline {
			display: inline-block !important;
		}

		@media (max-width: 370px) {
			.hidden-small {
				display: none !important;
			}
			.hidden-small-inline {
				display: none !important;
			}
		}

		a {
			color: darkslategrey;
			text-decoration: none;
		}

		a:hover {
			text-decoration: underline;
		}

		/* NAV */
		nav ul {
			list-style-type: none;
			margin: 0;
			padding: 0;
			overflow: hidden;
			background-color: #333;
		}

		nav li {
			float: left;
		}

		nav li a, nav .dropbtn {
			display: inline-block;
			color: white;
			text-align: center;
			padding: 14px 16px;
			text-decoration: none;
		}

		nav .icon:hover{
			background-color: #4D4D4D;
		}

		nav li a:hover, nav .dropdown:hover .dropbtn {
			background-color: #F05F40;
		}

		nav li.dropdown {
			display: inline-block;
		}

		nav .dropdown-content {
			display: none;
			position: absolute;
			background-color: #f9f9f9;
			min-width: 160px;
			box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
			z-index: 1;
		}

		nav .menu-icon .dropdown-content {
			right: 0;
		}

		nav .dropdown-content a {
			color: black;
			padding: 12px 16px;
			text-decoration: none;
			display: block;
			text-align: left;
		}

		nav .dropdown-content a:hover {background-color: #f1f1f1}

		nav .dropdown:hover .dropdown-content {
			display: block;
		}

		nav li.menu-icon {
			display: none;
			float: right;
		}

		@media screen and (max-width: 360px) {
			nav li:not(.menu-icon):not(.show-mobile) {
				display: none;
			}

			nav li.menu-icon {
				display: inline-block;
			}
		}

		nav .icon{
			padding: 8px;
		}

		nav .active{
			background: #4D4D4D;
		}

		#top-bar-react {
			position: fixed;
			top: 0;
			left: 0;
			right: 0;
		}

		.accent-color{
			color: #F05F40;
		}

		.hr-subtle{
			border-color: #00000014;
			border-radius: 4px;
		}

		/* Anchor */
		a.accent{
			color: #F05F40;
		}

		a:hover {
			text-decoration: none;
		}

		a.accent:hover {
			color: #FF8566;
			text-decoration: none;
		}
		.profile-picture-wrapper {
			border-radius: 4px;
			background: #333;
			display: inline-block;
			padding: 4px;
		}
	</style>
	<meta name="HandheldFriendly" content="true" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
	<link rel="stylesheet" href="/css/font-awesome.min.css">
	<link rel="stylesheet" href="/css/grid-system.css">
</head>
<body class="app">
	<canvas id='background'></canvas>
	<div class='content-wraper'>
		<div id='content-react'></div>
		<div id='top-bar-react'></div>
	</div>

	<script src="/vendor/jquery/jquery.min.js"></script>

	<script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
	<script crossorigin src="https://unpkg.com/react@16.2.0/umd/react.development.js"></script>
	<script crossorigin src="https://unpkg.com/react-dom@16.2.0/umd/react-dom.development.js"></script>
	<!-- <script src="https://unpkg.com/react-dom@16.2.0/dist/react-dom.js"></script> -->
	<script type="text/javascript">
		(function() {
			var lastTime = 0;
			var vendors = ['ms', 'moz', 'webkit', 'o'];
			for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
				window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
				window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
				|| window[vendors[x]+'CancelRequestAnimationFrame'];
			}

			if (!window.requestAnimationFrame)
				window.requestAnimationFrame = function(callback, element) {
					var currTime = new Date().getTime();
					var timeToCall = Math.max(0, 16 - (currTime - lastTime));
					var id = window.setTimeout(function() { callback(currTime + timeToCall); },
					timeToCall);
					lastTime = currTime + timeToCall;
					return id;
				};

			if (!window.cancelAnimationFrame)
				window.cancelAnimationFrame = function(id) {
					clearTimeout(id);
				};
		}());

		var canvas = document.getElementById('background');
		window.bg_animation = {
			canvas: canvas
		};
	</script>
	<script src="/js/background-animation.js"></script>
	<script src="/react/dist-min/search-app.js" type="text/javascript"></script>
</body>
</html>