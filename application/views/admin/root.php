<div id='root_cp'>
	<h2 style='text-align: center; margin: 10px 0px;'>Root Controls</h2>
	<b>Add new admin</b><br>
	<form action='/admin/add_admin' method='POST'>
		<input type="text" name="username" value="" placeholder="Username">
		<input type="password" name="password" value="" placeholder="Password">
		<input type="password" name="password_repeat" value="" placeholder="Password Again">
		<input type="submit" value="Add">
	</form>
	<br>
	<b>Delete admin</b><br>
	<form action='/admin/delete_admin' onsubmit="return popup(this, 'Are you sure you want to delete {username}?')" method='POST'>
		<input type="text" name="username" value="" placeholder="Username">
		<input type="submit" value="Remove">
	</form>
	<br>
	<b>Receipts Permission</b><br>
	<form action='/admin/change_permission_view_receipts' onsubmit="return popup(this, 'Are you sure you want to change permission for {username}?')" method='POST'>
		<input type="text" name="username" value="" placeholder="Username">
		<input type="checkbox" name="can_view_receipts" value="">
		<input type="submit" value="Change">
	</form>
	<br>
	<b>Kick Admin</b><br>
	<form action='/admin/kick_admin' method='POST'>
		<input type="text" name="username" value="" placeholder="Username">
		<input type="submit" value="Kick">
	</form>
	<br>
	<b>Change password for admin</b><br>
	<form action='/admin/change_admin_password' onsubmit="return popup(this, 'Are you sure you want to change password for {username}?')" method='POST'>
		<input type="text" name="username" value="" placeholder="Username">
		<input type="password" name="password" value="" placeholder="Password">
		<input type="password" name="password_repeat" value="" placeholder="Password Again">
		<input type="submit" value="Remove">
	</form>
	<br>
	<b>Active admins</b><br>
	<?=$this->viewtemplate->add_json($active_admins,FALSE)?>
	<br>
	<b>All admins</b><br>
	<?=$this->viewtemplate->add_json($admins,FALSE)?>
	<br>
</div>