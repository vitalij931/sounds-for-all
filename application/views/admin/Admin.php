<script>
	if (!localStorage.wasShown) {
		alert("When changing password for someone. !! DO NOT ASK THEM FOR PASSWORD !! choose a password for them and ask them to change it ASAP");
	}

	localStorage.wasShown = true;
</script>
<div id='admin_cp'>
	<h2 style='text-align: center; margin: 10px 0px;'>Admin Controls</h2>
	<b>Change Password</b><br>
	<form action='/admin/change_password' onsubmit="return popup(this, 'Are you sure you want to change your password?')" method='POST'>
		<input type="password" name="old_password" value="" placeholder="Old Password">
		<input type="password" name="password" value="" placeholder="Password">
		<input type="password" name="password_repeat" value="" placeholder="Password Again">
		<input type="submit" value="Change">
	</form>
	<br>
	<b>All Receiptps</b>
	<?=$this->viewtemplate->add_json($receipts,FALSE)?>
	<br>
	<b>Get User Data</b>
	<form action="/admin/get_user_data" method="post" accept-charset="utf-8">
		<input type="text" name="username" value="" placeholder="Username">
		<input type="submit" name="" value="Get">
	</form>
	<br>
	<b>Search Staff Actions</b>
	<form id="search_staff_actions_form" onsubmit="return false">
		<input type="text" name="search_staff" value="" placeholder="Search for staff">
		<input type="text" name="search_business" value="" placeholder="Search for business name">
		<input type="submit" name="" id="search_staff_actions_button" value="Get">
	</form>
	<div id="search_staff_actions"></div>
	<br>
<!-- 	<b>Start session as</b>
	<form action='/admin/run_session_as' onsubmit="return popup(this, 'WARNING!! Your soul will suffer if you use this for personal use.\n\nStart session as {username}?')" method='POST'>
		<input type="text" name="username" placeholder="Enter username">
		<input type="submit">
	</form> -->
	<br>
	<a href="/admin/complaint_tickets/">View Support Tickets</a> 
	<br>
	<a href="/admin/business_complaint_tickets/">View Business Tickets</a>
	<br>
	<a href="/admin/media_reports/">Media Reports</a>
	<br>
	<a href="/admin/faq/">FAQ Management</a>
</div>

<script>
	(function() {
		var submit_staff_action = function(e) {
			while(!e);

			var el = $(e.target);
			var searchEl = el.closest('tr');
			var id = el.data('id');
			var level = searchEl.find('#staff_level');

			data = {
				id: id,
				password: searchEl.find('#passwordV' + id).val(),
				comment: searchEl.find('#commentingV' + id+':checked').length > 0 ? 1 : 0,
				managed: searchEl.find('#allow_adding_managedV' + id+':checked').length > 0 ? 1 : 0,
				support: searchEl.find('#supportV' + id+':checked').length > 0 ? 1 : 0,
				level: level.val()
			};

			$.post("/admin/search_staff_actions/do", data, function(data){
				alert((data.data.success ? "Success. " : "Fail. ") + data.data.message);
			},'json');
		}

		$("#search_staff_actions_button").click(function () {
			$.post("/admin/search_staff_actions/get",$('#search_staff_actions_form').serialize(), function (data) {
				if (data.success) {
					$('#search_staff_actions').html(data.data);
					$('.submit_staff_action').click(submit_staff_action);
				}else{
					alert(data.message);
				}
			},'json')
		});
	})();
</script>

<script type="text/javascript">
	if (!String.prototype.format) {
		String.prototype.format = function() {
			var str = this.toString();

			if (!arguments.length)
				return str;
			var args = typeof arguments[0],
				args = (("string" == args || "number" == args) ? arguments : arguments[0]);
			for (arg in args)
				str = str.replace(RegExp("\\{" + arg + "\\}", "gi"), args[arg]);
			return str;
		}
	}

	function popup(form, msg, strs){
		var inputs = form.getElementsByTagName('input');
		var args = '{';

		for(var i = 0; i < inputs.length; i++){
			if((inputs[i].type != 'submit' && inputs[i].type != 'checkbox') && inputs[i].value == ''){
				console.log(form.id + ' form not filled completely')
				return false;
			}

			if(inputs[i].name != '')
				args += '"' + inputs[i].name + '":"' + inputs[i].value + '",';
		}
		args = args.replace(/,$/, "") + "}";
		msg = msg.format(JSON.parse(args));

		return confirm(msg);
	}
</script>