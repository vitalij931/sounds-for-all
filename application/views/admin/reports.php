<style type="text/css" media="screen">
	div.temp{
		width: 400px;
		max-height: 100%;

		margin: 0 auto;
		position: relative;
		top: 50%;
		transform: translateY(-50%);

		border: 1px rgb(89,89,89) solid;

		background: rgb(255,255,255);

		-moz-box-shadow:  2px 2px 0px 0px rgba(0, 0, 0, 0.13);
		-webkit-box-shadow:  2px 2px 0px 0px rgba(0, 0, 0, 0.13);
		box-shadow:  2px 2px 0px 0px rgba(0, 0, 0, 0.13);
	}

	.full-size{
		position: absolute;
		top:0;
		bottom:0;
		right:0;
		left:0;

		height:100%;
		width:100%;
		background-color: rgba(0,0,0,0.1);
		overflow:hidden; /* or overflow:auto; if you want scrollbars */
	}

	img#temp_image{
		max-width: 400px;
		height: auto;

		display: block;
		margin: 0 auto;
	}
</style>
<div id='temp_full' onclick="close_temp()" style="display: none" class="full-size">
	<div class="temp">
		<a style='float: right; cursor: hand;' onclick="close_temp()">X</a>

		<div id='temp_content' style="padding: 10px"></div>
	</div>
</div>
<script type="text/javascript">
	$('div.temp').click(function(e){
		e.stopPropagation();
	});
	function showTemporaryView(el){
		var data = JSON.parse($(el).attr('data'));
		var string = '';

		$('div#temp_full').toggle();

		if(data['url_id'])
			string += "<img id='temp_image' style='text-align: center' src='/media/get/" + data['url_id'] + "'></img><br>";

		for (var i in data) {
			switch (i) {
				case 'url_id':
					string += "<b>" + i +"</b>: <a href='/media/get/" + data[i] + "'>" + data[i] + "</a><br>";
					break;
				case 'alternative_url':
					string += "<b>" + i +"</b>: <a href='" + data[i] + "'>" + data[i] + "</a><br>";
					break;
				case 'upload_date':
					var date = new Date(parseInt(data[i])*1000);
					string += "<b>" + i +"</b>: " + dateFormat(date, "dddd, mmmm dS, yyyy, H:MM:ss") + "<br>";
					break;
				case 'u_id':
					string += "<b>" + i +"</b>: <a href='/account/" + data[i] + "'>" + data[i] + "</a><br>";
					break;
				case 'verified':
				case 'comments_enabled':
					data[i] = data[i] == 1 ? 'Yes' : 'No';
				default:
					if(data[i] == null){
						data[i] = '<mark style="color: red; background-color: #fff">Empty</mark>'
					}
					string += "<b>" + i +"</b>: " + data[i] + "<br>";
					break;
			}
		}
		$('div#temp_content').html(string);
	}

	function close_temp(){
		$('div#temp_full').toggle();
	}

	var dateFormat = function () {
        var    token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
            timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
            timezoneClip = /[^-+\dA-Z]/g,
            pad = function (val, len) {
                val = String(val);
                len = len || 2;
                while (val.length < len) val = "0" + val;
                return val;
            };
    
        // Regexes and supporting functions are cached through closure
        return function (date, mask, utc) {
            var dF = dateFormat;
    
            // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
            if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
                mask = date;
                date = undefined;
            }
    
            // Passing date through Date applies Date.parse, if necessary
            date = date ? new Date(date) : new Date;
            if (isNaN(date)) throw SyntaxError("invalid date");
    
            mask = String(dF.masks[mask] || mask || dF.masks["default"]);
    
            // Allow setting the utc argument via the mask
            if (mask.slice(0, 4) == "UTC:") {
                mask = mask.slice(4);
                utc = true;
            }
    
            var    _ = utc ? "getUTC" : "get",
                d = date[_ + "Date"](),
                D = date[_ + "Day"](),
                m = date[_ + "Month"](),
                y = date[_ + "FullYear"](),
                H = date[_ + "Hours"](),
                M = date[_ + "Minutes"](),
                s = date[_ + "Seconds"](),
                L = date[_ + "Milliseconds"](),
                o = utc ? 0 : date.getTimezoneOffset(),
                flags = {
                    d:    d,
                    dd:   pad(d),
                    ddd:  dF.i18n.dayNames[D],
                    dddd: dF.i18n.dayNames[D + 7],
                    m:    m + 1,
                    mm:   pad(m + 1),
                    mmm:  dF.i18n.monthNames[m],
                    mmmm: dF.i18n.monthNames[m + 12],
                    yy:   String(y).slice(2),
                    yyyy: y,
                    h:    H % 12 || 12,
                    hh:   pad(H % 12 || 12),
                    H:    H,
                    HH:   pad(H),
                    M:    M,
                    MM:   pad(M),
                    s:    s,
                    ss:   pad(s),
                    l:    pad(L, 3),
                    L:    pad(L > 99 ? Math.round(L / 10) : L),
                    t:    H < 12 ? "a"  : "p",
                    tt:   H < 12 ? "am" : "pm",
                    T:    H < 12 ? "A"  : "P",
                    TT:   H < 12 ? "AM" : "PM",
                    Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                    o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                    S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
                };
    
            return mask.replace(token, function ($0) {
                return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
            });
        };
    }();

    // Some common format strings
    dateFormat.masks = {
        "default":      "ddd mmm dd yyyy HH:MM:ss",
        shortDate:      "m/d/yy",
        mediumDate:     "mmm d, yyyy",
        longDate:       "mmmm d, yyyy",
        fullDate:       "dddd, mmmm d, yyyy",
        shortTime:      "h:MM TT",
        mediumTime:     "h:MM:ss TT",
        longTime:       "h:MM:ss TT Z",
        isoDate:        "yyyy-mm-dd",
        isoTime:        "HH:MM:ss",
        isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
        isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
    };
    
    // Internationalization strings
    dateFormat.i18n = {
        dayNames: [
            "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
            "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
        ],
        monthNames: [
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
        ]
    };
    
    // For convenience...
    Date.prototype.format = function (mask, utc) {
        return dateFormat(this, mask, utc);
    };
</script>