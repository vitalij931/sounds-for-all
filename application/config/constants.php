<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

// Custom constants
defined('DEFUALT_PROFILE_PICTURE_URL') OR define('DEFUALT_PROFILE_PICTURE_URL', "images/profile-grey-full.png");
defined('DEFUALT_PROFILE_PICTURE_BUSINESS_URL') OR define('DEFUALT_PROFILE_PICTURE_BUSINESS_URL', "images/profile-placeholder-business.png");
defined('DEFUALT_COVER_PICTURE_BUSINESS_URL') OR define('DEFUALT_COVER_PICTURE_BUSINESS_URL', "images/cover-placeholder.png");
defined('DEFUALT_COVER_PICTURE_URL') OR define('DEFUALT_COVER_PICTURE_URL', "images/cover-placeholder.png"); //
defined('NOTHING_PROFILE_PICTURE_URL') OR define('NOTHING_PROFILE_PICTURE_URL', "images/nothing_profile.png");
defined('DEFAULT_CATEGORY_IMAGE_URL') OR define('DEFAULT_CATEGORY_IMAGE_URL', "images/grid-packer-default.png");
defined('DEFAULT_STAFF_IMAGE_URL') OR define('DEFAULT_STAFF_IMAGE_URL', "images/profile-placeholder-staff.png");
defined('DEFAULT_STAFF_ADMIN_IMAGE_URL') OR define('DEFAULT_STAFF_ADMIN_IMAGE_URL', "images/profile-placeholder-admin.png");

defined('BUCKET') OR define('BUCKET', 'storii'); //'storii'
defined('CF_URL') OR define('CF_URL', 'https://d585fm2ghhc46.cloudfront.net/'); //'https://d14lmuv6o0couv.cloudfront.net/'
defined('UPLOAD_ACCESS_KEY_ID') OR define('UPLOAD_ACCESS_KEY_ID','AKIAJCC4P3SEU5MCFVFQ');
defined('UPLOAD_SECRET_ACCESS_KEY') OR define('UPLOAD_SECRET_ACCESS_KEY','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');
defined('AWS_ACCESS_KEY_ID') OR define('AWS_ACCESS_KEY_ID', 'AKIAJWG2WNNJP7WGRY7Q');
defined('AWS_SECRET_ACCESS_KEY') OR define('AWS_SECRET_ACCESS_KEY', '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');
defined('CLOUDFRONT_KEY_PAIR_ID') OR define('CLOUDFRONT_KEY_PAIR_ID', 'APKAJ4ZHXLYSEVZ455CQ');
defined('YOUTUBE_API_KEY') OR define('YOUTUBE_API_KEY', 'AIzaSyAAV-WdMugVIZdFLZRXNV2evaU7X4bdzEg');
defined('SPOTIFY_CLIENT_ID') OR define('SPOTIFY_CLIENT_ID',"dc04e2c4b7704e62aab7e80e73352c26");
defined('SPOTIFY_CLIENT_SECRET') OR  define('SPOTIFY_CLIENT_SECRET',"3d444597cb7f4f8e841454ebb687e953");
defined('TRANSCODER_PIPELINE_ID') OR define('TRANSCODER_PIPELINE_ID', "1460577968248-utkoc0"); // 1460542466251-mssa0o
defined('STRIPE_SECRET_KEY') OR define('STRIPE_SECRET_KEY', "sk_live_GQQFMBFLNihz43QfGjJF05TN");
defined('STRIPE_PUBLISHABLE_KEY') OR define('STRIPE_PUBLISHABLE_KEY', "pk_live_22ejfaCzuocFK0dlU9wAWeBC");
defined('TEST_STRIPE_SECRET_KEY') OR define('TEST_STRIPE_SECRET_KEY', "sk_test_oc4xTi6LHNTzGFHVnnhy3zmj");
defined('TEST_STRIPE_PUBLISHABLE_KEY') OR define('TEST_STRIPE_PUBLISHABLE_KEY', "pk_test_BQpMN2b6n5BVcSuha4wajaaF");

defined('APPLICATION_TITLE') OR define('APPLICATION_TITLE', 'Sounds For All');