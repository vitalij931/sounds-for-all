<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_account extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function login($username = '', $password = ''){
		if (empty($username)) {
			return ['success' => FALSE,
					'message' => 'Username can not be empty.'];
		}

		if (empty($password)) {
			return ['success' => FALSE,
					'message' => 'Username can not be empty.'];
		}

		$user = $this->db->select()
			->from('users')
			->where('username', $username)
			->limit(1)
			->get()
			->row();

		if (!$user) {
			return ['success' => FALSE,
					'message' => 'User not found. Please check your username'];
		}

		if (!password_verify($password, $user->password)) {
			return ['success' => FALSE,
					'message' => 'Incorrect password.'];
		}

		$newdata = [
			'username'  => $user->username,
			'email' => $user->email,
			'logged_in' => TRUE,
			'profile_picture' => null
		];

		$this->session->set_userdata($newdata);
		session_write_close();

		return ['success' => TRUE,
				'message' => ''];
	}

	public function register_check($username = '', $email = '', $password = ''){
		if (empty($username)) {
			return ['success' => FALSE,
					'message' => 'Username can not be empty.'];
		}

		if (empty($password)) {
			return ['success' => FALSE,
					'message' => 'Password can not be empty.'];
		}

		if (strlen($password) < 8) {
			return ['success' => FALSE,
					'message' => 'Password must be 8 characters or more.'];
		}

		if (empty($email)) {
			return ['success' => FALSE,
					'message' => 'Email cannot be empty.'];
		}

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return ['success' => FALSE,
					'message' => 'Email provided is invalid.'];
		}

		$user = $this->db->select()
			->where('username', $username)
			->get('users',1)
			->row_array();

		if ($user) {
			return ['success' => FALSE,
					'message' => 'This username is already taken.'];
		}

		return ['success' => TRUE,
				'message' => ''];
	}

	public function register($username = '', $email = '', $password = ''){
		$register_check = $this->register_check($username, $email, $password);
		if (!$register_check['success']) {
			return $register_check;
		}

		$new_user = [
			'username' => $username,
			'password' => password_hash($password, PASSWORD_DEFAULT),
			'time_created' => time(),
			'email' => $email
		];

		$inserted = $this->db->insert('users', $new_user);

		if (!$inserted) {
			return ['success' => FALSE,
					'message' => 'Something went wrong. Please try again later. (1)'];
		}

		$login_response = $this->login($username, $password);

		return $login_response;
	}
}

/* End of file M_account.php */
/* Location: ./application/models/M_account.php */