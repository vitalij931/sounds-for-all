<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_map_stuff extends CI_Model {
	private $facebook_object, $facebook_app;
	public function __construct(){
		parent::__construct();
		
		$this->facebook_object = new Facebook\Facebook([
			'app_id' => '1411789275574091',
			'app_secret' => '46805b54cc74ca40d300e9ffc239a4f6',
			'default_graph_version' => 'v2.10',
		]);

		$this->facebook_app = $this->facebook_object->getApp();
	}

	public function get_friends_markers($facebook_access_token = NULL){
		// Check session
		if (empty($facebook_access_token)) {
			return [
				'success' => FALSE,
				'message' => 'No access token received'
			];
		}

		// Get friends from facebook token

		$request = new Facebook\FacebookRequest(
			$this->facebook_app,
			$facebook_access_token,
			'GET',
			'/me/friends'
		);

		try{
			$response = $this->facebook_object->getClient()->sendRequest($request);
		}catch(Facebook\Exceptions\FacebookSDKException $e){
			return [
				'success' => FALSE,
				'message' => $e->getMessage()
			];
		}

		$graphArray = $response->getGraphEdge()->asArray();

		// Do select where with friends

		// Return last location if found or if active last 24h

		return [
			'success' => TRUE,
			'data' => [
				[
					'long' => -4.220777750015,
					'lat' => 55.8641363959
				],
				[
					'long' => -4.220777750015,
					'lat' => 55.8661363959
				],
				[
					'long' => -4.220777750015,
					'lat' => 55.8641363959
				],
				[
					'long' => -4.223777750015,
					'lat' => 55.8641363959
				]
			],
			'debug' => $graphArray
		];
	}

	public function set_current_marker($current_location = []){
		# code...
	}
}

/* End of file M_map_stuff.php */
/* Location: ./application/models/M_map_stuff.php */