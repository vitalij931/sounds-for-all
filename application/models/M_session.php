<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// GLOBALS FOR EASIER USE _____________________________________________________
function _s() {
	$CI =& get_instance();
	return $CI->session;
}

function _sna() {
	$CI =& get_instance();
	return !$CI->session->is_active();
}
// Session Expired Return/aRray
const SE_R = [ 'success' => FALSE, 'message' => "Session has expired" ];
// ____________________________________________________________________________

class M_Session extends CI_Model {
	private $id;
	private $user_id;
	private $expire_date;
	private $is_business;
	private $staff_id = -1;
	private $active = FALSE;
	private $err_msg = "";
	private $managing_id = -1;
	private $verified = FALSE;
	private $staff_level = -1;
	private $show_verify = FALSE;

	private $sid_changed = FALSE;
	private $user_name = NULL;
	private $creation_date = 0;

	private $elasticache;

	private $SESSION_TIME = 1800; // 30 minutes
	private $REMEMBER_ME_TIME = 2419200; // 1 month
	private $STAFF_SESSION_TIME = 900; // 15 minutes
	private $BUSINESS_REMEMBER_ME_TIME = 5184000; // 60 days
	private $P_USER_VALIDATION = 86400; // 24 hours
	private $B_USER_VALIDATION = 259200; // 72 hours
	private $COOKIE_TIME = 15768000;

	public function __construct() {
		parent::__construct();

		if (FALSE && class_exists('Memcached')) {
			$this->elasticache = new Memcached('main');

			if (!count($this->elasticache->getServerList())) {
				$this->elasticache->addServer('mainm.h42gp1.0001.euw1.cache.amazonaws.com', 11211);
			}
		}else{
			$this->elasticache = NULL;
		}

		// $this->load->database();
		$this->load->helper('cookie');
		$this->load->helper('string');
		// $this->load->helper('mobile');
		$this->start();
		// const _S = $this;
	}

	public function start($user_id=NULL, $remember_me = FALSE) {
		if ($user_id == NULL) {  
			//$sid = is_user_mobile() ? '' : get_cookie('sid', TRUE);
			$sid = get_cookie('sid', TRUE);
			$rem = get_cookie('rem', TRUE);

			$query = NULL;
			$row = NULL;
			if (isset($sid)) {
				if ($this->elasticache != NULL) {
					$row = $this->elasticache->get("s_{$sid}");
				}

				if (!$row) {
					$query = $this->db
							->select('s.*, u.verified, u.creation_date, u.user_name, mu.managed_id')
							->from('sessions s')
							->where('token', $sid)
							->join('users u', 'u.id = s.user_id')
							->join('manageable_users mu', 'mu.managed_id = s.user_id', 'left')
							->get();
					$row = $query->row();

					if ($this->elasticache != NULL && $row != NULL && count($this->elasticache->getServerList()) > 0) {
						$row->cache_time = time();
						$this->elasticache->set("s_{$sid}", $row, time() + ($row->is_business ? $this->STAFF_SESSION_TIME : $this->SESSION_TIME));
					}
				}else{
					$query = true;
				}

				if(isset($row)){
					$trow = clone $row;
				}
			}
			if (isset($query) && ($row || !empty($query->result())) && time() < $row->expire_date) {
				// Checking if account is verified
				$time = time() - ($row->is_business ? $this->B_USER_VALIDATION : $this->P_USER_VALIDATION);
				if (!$row->verified && $row->creation_date <= $time && $row->managed_id == NULL) {
					return $this->stop("You must verify your email in order to continue <br>(Check your spam folders!) <a href='/account/resend_verification_email/{$row->user_id}'>Resend</a>");
				}
				$this->expire_date = time() + ($row->is_business ? $this->STAFF_SESSION_TIME : $this->SESSION_TIME);

				if ($this->elasticache != NULL && count($this->elasticache->getServerList()) > 0) {
					$trow->expire_date = $this->expire_date;

					if (!isset($trow->cache_time)) {
						$trow->cache_time = time();
					}

					$this->elasticache->set("s_{$sid}", $trow, $this->expire_date);
				}
				
				$this->id = $row->token;
				$this->user_id = $row->user_id;
				$this->is_business = $row->is_business;
				$this->staff_id = $row->staff_id;
				$this->managing_id = $row->managing_id;
				$this->active = TRUE;
				$this->verified = $row->verified;
				$this->show_verify = $this->verified;
				$this->creation_date = $row->creation_date;

				$this->user_name = $row->user_name;

				if (!isset($row->cache_time) || $row->cache_time > time() - 300) {
					$this->insert_into_db();
				}
			} else if(isset($rem)) {
				$q = $this->db->select('user_id, expire_date')->where('token', $rem)->get('remember_me');
				$row = $q->row();
				if(!empty($q->result()) && time() < $row->expire_date){
					$this->create_new_session($row->user_id, TRUE);
				}else{
					return $this->stop("You have been timed out");
				}
			} else
				return $this->stop("You have been timed out");
		}
		else { // creating	
			$this->create_new_session($user_id, $remember_me);
		}

		return $this;
	}

	private function create_new_session($user_id, $remember_me) {
		$this->manage_new_sid();
		$this->user_id = $user_id;
		$this->expire_date = time() + $this->SESSION_TIME;
		$this->set_is_business($user_id);
		$this->managing_id = $this->is_business ? -1 : $user_id;
		$this->staff_id = -1;

		if (!$this->is_business && isset($remember_me) && !$remember_me)
			$this->is_business = 0;
		else if (!$this->is_business)
			$this->is_business = NULL;

		$time = time() - ($this->is_business ? $this->B_USER_VALIDATION : $this->P_USER_VALIDATION);
		$query = $this->db
			->select('u.verified, u.creation_date, u.user_name, mu.managed_id, u.email')
			->where('u.id', $user_id)
			->join('manageable_users mu', 'mu.managed_id = u.id', 'left')
			->get('users u');
		$row = $query->row();

		$this->verified = $row->verified;
		$this->creation_date = $row->creation_date;

		if (!$row->verified && $row->creation_date <= $time && $row->managed_id == NULL) {
			if (empty($row->email)) {
				return $this->stop("You must set and verify your email in order to continue <a href='/account/setup_email/{$user_id}'>Click here to set up</a>");
			}
			return $this->stop("You must verify your email in order to continue <a href='/account/resend_verification_email/{$user_id}'>Resend</a>");
		}

		// if ($this->is_business) {
		// 	$d_auth_key = get_cookie('d', TRUE);
		// 	if (!isset($d_auth_key)) {
		// 		$d_auth_key = random_string('alnum', 64); // DUP LINES
		// 		set_cookie('d',$d_auth_key,0,$this->input->server('HTTP_POST'),'/','',FALSE,TRUE); // DUP LINES
		// 	}

		// 	$query = $this->db->select('*')->where('auth_key', $d_auth_key)->get('business_devices');
		// 	if (empty($query->result())) {
		// 		$d_auth_key = random_string('alnum', 64); // DUP LINES
		// 		set_cookie('d',$d_auth_key,0,$this->input->server('HTTP_POST'),'/','',FALSE,TRUE); // DUP LINES
		// 		$verification_key = random_string('alnum', 5);
		// 		$query = $this->db->insert('business_devices', [ 'business_id' => $user_id, 'auth_key' => $d_auth_key, 'verification_key' => $verification_key, 'key_expire_date' => time() + 1800 ]);

		// 		return $this->stop("This is the first time you log in from this device! Please enter a verification key sent to the business email address.");
		// 	}

		// 	$row = $query->row();
		// 	if ($row->verification_key != NULL) {
		// 		return $this->stop("This is the first time you log in from this device! Please enter a verification key sent to the business email address.");
		// 	}
		// }
		
		$this->insert_into_db();
		
		$this->active = TRUE;
		$rem = get_cookie('rem', TRUE);

		// Tutorial showing cookie
		$qr = $this->db->select('show_tutorial')->where('user_id', $user_id)->limit(1)->get('user_data')->result_array();

		if(isset($qr['show_tutorial'])){
			$show_tutorial = $qr['show_tutorial'] ? 1 : 0;
		}else{
			$show_tutorial = 0;
		}
		set_cookie('u_tut', $show_tutorial, 0, $this->input->server('HTTP_POST'));
		$this->input->set_cookie('pp', 1, $this->COOKIE_TIME, $this->input->server('HTTP_POST'));

		if ($remember_me) { // remember me on login
			$key = random_string('alnum', 64);
			$time = time() + ($this->is_business() ? $this->BUSINESS_REMEMBER_ME_TIME : $this->REMEMBER_ME_TIME);
			if (isset($rem) && !$this->is_business()) {
				$query = $this->db
					->where('token', $rem)
					->update('remember_me', [ 'token' => $key, 'expire_date' => $time ]);
			} else {
				$query = $this->db
					->where('token', $rem)
					->insert('remember_me', [ 'token' => $key, 'user_id' => $user_id, 'expire_date' => $time ]);
			}

			set_cookie('rem',$key, $this->COOKIE_TIME,$this->input->server('HTTP_POST'),'/','',FALSE,TRUE);
		}
		else if (!isset($this->is_business)) {
			if (isset($rem)) {
				$query = $this->db
					->where('token', $rem)
					->delete('remember_me');
			}
			delete_cookie('rem', $this->input->server('HTTP_POST'),'/','');
			delete_cookie('rem', $this->input->server('HTTP_HOST'),'/','');
		}
	}

	public function start_business_verification($device_id, $verification_key) {
		$d = get_cookie('d', TRUE);
		if (!isset($d)) {
			return [ 'success' => FALSE,
					 'message' => "Your device is not logged in" ];
		}

		if (empty($device_id))
			return [ 'success' => FALSE,
					 'message' => "You must set a name for his device" ];

		if (empty($verification_key))
			return [ 'success' => FALSE,
					 'message' => "You must enter the verification key" ];

		$query = $this->db->select('*')->where('auth_key', $d)->get('business_devices');

		if (empty($query->result()))
			return [ 'success' => FALSE,
					 'message' => "Your device was not recognised" ];

		$row = $query->row();

		if ($row->key_expire_date < time()) {
			$query = $this->db->where('auth_key', $d)->delete('business_devices');
			delete_cookie('d', $this->input->server('HTTP_POST'),'/','');
			delete_cookie('d', $this->input->server('HTTP_HOST'),'/','');
			return [ 'success' => FALSE,
					 'message' => "Verification key has already expired, please try to log in again" ];
		}
		if ($row->verification_key != $verification_key)
			return [ 'success' => FALSE,
					 'message' => "Verification key provided does not match. Please try again" ];

		$query = $this->db
			->where('business_id', $row->business_id)
			->where('id', $device_id)
			->get('business_devices', 1);

		if (!empty($query->result()))
			return [ 'success' => FALSE,
					 'message' => "This device name is already occupied" ];

		$query = $this->db->where('auth_key', $d)->update('business_devices', [ 'id' => $device_id, 'verification_key' => NULL ]);

		return $this->start($row->business_id, TRUE);
	}

	public function set_managing_id($id) {
		if (!ctype_digit($id)) {
			$id = $this->is_business() ? -1 : $this->user_id;
		}

		if ($this->managing_id != $id) {
			// $this->manage_new_sid();
			$this->managing_id = $id;
			$this->insert_into_db();
		}
	}

	public function get_managing_id() {
		return $this->managing_id;
	}

	public function is_managing() {
		return $this->managing_id > 0; //$this->managing_id != $this->user_id && $this->managing_id > 0;
	}

	private function set_is_business($user_id) {
		$query = $this->db->select('id')
			->where('id',$user_id)
			->get('businesses');
		$this->is_business = sizeof($query->result()) == 1;
	}

	private function clear_session_from_db() {
		$sid = get_cookie('sid', TRUE);
		if (isset($sid))
			$this->db->where('token', $sid)->delete('sessions');
	}

	private function clear_remember_me_from_db() {
		$rem = get_cookie('rem', TRUE);
		if (isset($rem))
			$this->db->where('token', $rem)->delete('remember_me');
	}
	
	private function stop($msg) {
		$this->logout();
		$this->err_msg = $msg;
		return $this;
	}
	
	private function manage_new_sid() {
		$this->clear_session_from_db();
		$sid = random_string('alnum', 32);
		$this->id = $sid;
		$this->sid_changed = TRUE;
		set_cookie('sid', $this->id, $this->COOKIE_TIME, $this->input->server('HTTP_POST'),'/','',FALSE,TRUE);
	}
	
	private function insert_into_db() {
		$data = [ 'token' => $this->id,
				  'user_id' => $this->user_id,
				  'expire_date' => $this->expire_date,
				  'is_business' => $this->is_business,
				  'staff_id' => $this->staff_id,
				  'managing_id' => $this->managing_id ];
		if ($this->sid_changed)
			$this->db->insert('sessions', $data);
		else $this->db->where('token', $this->id)->update('sessions', $data);

		$this->sid_changed = FALSE;
	}
	
	public function logout(){
		$this->clear_session_from_db();
		delete_cookie('sid', $this->input->server('HTTP_POST'),'/','');
		delete_cookie('sid', $this->input->server('HTTP_HOST'),'/','');

		if (!isset($this->is_business) || $this->is_business) {
			$this->clear_remember_me_from_db();
			delete_cookie('rem', $this->input->server('HTTP_POST'),'/','');
			delete_cookie('rem', $this->input->server('HTTP_HOST'),'/','');
		}

		$this->err_msg = "Logged out";
		$this->active = FALSE;
	}
	
	public function is_active() {
		return $this->active;
	}

	public function get_id() {
		return $this->id;
	}
	
	public function get_user_id() {
		return $this->user_id;
	}

	public function get_expire_date() {
		return $this->expire_date;
	}

	public function is_business() {
		return $this->is_business;
	}
	
	public function is_staff() {
		return $this->staff_id != -1;
	}

	public function get_staff_id() {
		return $this->staff_id;
	}

	public function get_staff_level() {
		if($this->is_staff()){
			if($this->staff_level == -1){
				$query = $this->db
					->select('level')
					->where('id', $this->staff_id)
					->get('staff');
				$res = $query->result_array();

				if(count($res) > 0){
					$this->staff_level = $res[0]['level'];
				}
			}
		} else{
			return -1;
		}
		return $this->staff_level;
	}
	
	public function set_staff_id($id) {
		if ($this->staff_id != $id) {
			// $this->manage_new_sid();
			$this->staff_id = $id;
			$this->insert_into_db();
		}
	}
	
	public function get_err() {
		return $this->err_msg;
	}

	public function get_user_name() {
		return $this->user_name;
	}

	public function is_verified() {
		return $this->verified;
	}

	public function show_verify(){
		if ($this->show_verify) {
			$this->show_verify = FALSE;
			return $this->show_verify;	
		}
		return $this->show_verify;
	}

	public function get_staff_session_time(){
		return $this->STAFF_SESSION_TIME;
	}

	public function get_user_creation_date() {
		return $this->creation_date;
	}
}

?>