<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_music extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function search_database(){
		return [
			'success' => TRUE,
			'message' => '',
			'data' => [
				[
					'id' => 0,
					'name' => 'Dance to all',
					'src' => '/assets/2-1.mp3',
					'cover_photo' => 'http://www.designformusic.com/wp-content/uploads/2015/10/insurgency-digital-album-cover-design.jpg',
					'time_length' => 82,
					'description' => "It's amazing!! I swear!! Check out my other shit",
					'tags' => ['EDM','Music','Stuff'],
					'owner' => [
						'username' => 'test',
						'display_name' => 'Test',
						'profile_picture' => null
					]
				],[
					'id' => 1,
					'name' => 'A moment in space',
					'src' => '/assets/2-2.mp3',
					'cover_photo' => '',
					'time_length' => 48,
					'owner' => [
						'username' => 'test',
						'display_name' => 'Test',
						'profile_picture' => null
					]
				],[
					'id' => 2,
					'name' => 'Dark Smash',
					'src' => '/assets/2-3.mp3',
					'cover_photo' => '',
					'time_length' => 55,
					'owner' => [
						'username' => 'test',
						'display_name' => 'Test',
						'profile_picture' => null
					]
				]
			]
		];
	}
}

/* End of file M_music.php */
/* Location: ./application/models/M_music.php */