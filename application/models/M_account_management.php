<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_account_management extends CI_Model {
	private $facebook_app;

	public function __construct(){
		parent::__construct();

		$this->facebook_app = new Facebook\Facebook([
			'app_id' => '661263320699947',
			'app_secret' => '111e07bc96b60ae28b97cd35fc1dab22',
			'default_graph_version' => 'v2.2',
		]);
	}

	public function facebook_login($access_token = NULL){
		if (!$access_token || empty($access_token)) {
			return ['success' => FALSE,
					'message' => "No access token received"];
		}

		try {
			// Returns a `Facebook\FacebookResponse` object
			$response = $this->facebook_app->get('me?fields=id,name,email,picture{url,width,height}', $access_token);
			// $response = $response->execute();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			return ['success' => FALSE,
					'message' => "Facebook Response: {$e->getMessage()}"];
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			return ['success' => FALSE,
					'message' => "Facebook SDK: {$e->getMessage()}"];
		}

		$graphObject = $response->getGraphObject()->asArray();
		$facebook_id = $graphObject['id'];

		$user = $this->db->select()
			->where('facebook_user_id', $facebook_id)
			->get('facebook_tokens',1)
			->row();

		if ($user) {
			$this->db
				->where('facebook_user_id', $facebook_id)
				->update('facebook_tokens', ['token' => $access_token]);
		}else{
			$this->db->insert('facebook_tokens', 
				[
					'facebook_user_id'	=> $facebook_id,
					'token'				=> $access_token
				]);
		}

		return ['success' => TRUE,
				'message' => ''];
	}

	public function create_event($list= []){
		if ($this->facebook_session->is_session_valid()) {
			return ['success' => FALSE,
					'message' => $this->facebook_session->get_error_message()];
		}

		return ['success' => TRUE,
				'message' => ''];
	}

}

/* End of file M_account_management.php */
/* Location: ./application/models/M_account_management.php */