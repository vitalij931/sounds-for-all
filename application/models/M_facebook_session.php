<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_facebook_session extends CI_Model {
	private $access_token;
	private $user_id;
	private $facebook_object;
	private $facebook_app;

	private $session_error_message;

	public function __construct(){
		parent::__construct();
		$this->facebook_object = new Facebook\Facebook([
			'app_id' => '661263320699947',
			'app_secret' => '111e07bc96b60ae28b97cd35fc1dab22',
			'default_graph_version' => 'v2.2',
		]);
		$this->facebook_app = $this->facebook_object->getApp();

		$access_token = $this->input->post('token');

		if (!$access_token) {
			$json_body = json_decode($this->input->raw_input_stream, true);

			if ($json_body) {
				if (!isset($json_body['token'])) {
					echo "No token ";
					die(var_dump($json_body));
				}
				$access_token = $json_body['token'];
			}
		}

		if (!$access_token) {
			$this->session_error_message = "Access token was not provided";
			return;
		}

		$session_row = $this->db->select()
			->where('token', $access_token)
			->get('facebook_tokens',1)
			->row();

		if (!$session_row) {
			$request = new Facebook\FacebookRequest(
				$this->facebook_app,
				$access_token,
				'GET',
				'/me'
			);

			try{
				$response = $this->facebook_object->getClient()->sendRequest($request);
			}catch(Facebook\Exceptions\FacebookSDKException $e){
				$this->session_error_message = $e->getMessage();
				return;
			}
			$graphObject = $response->getGraphObject();
			$facebook_user_id = $graphObject->getProperty('id');

			$user_id;
			$session_row = $this->db->select('id')
				->where('facebook_user_id',$facebook_user_id)
				->get('facebook_tokens')
				->row();

			if ($session_row) {
				$this->db->where('facebook_user_id',$facebook_user_id)->update('facebook_tokens', ['token' => $access_token]);
				$user_id = $session_row->id;
			}else{
				$this->db->insert('facebook_tokens', ['token' => $access_token, 'facebook_user_id' => $facebook_user_id]);
				$user_id = $this->db->insert_id();
			}

			$this->user_id = $user_id;
			$this->access_token = $access_token;
		}else{
			$this->user_id = $session_row->id;
			$this->access_token = $access_token;
		}
	}

	public function is_session_valid(){
		return !$this->user_id;
	}

	public function get_error_message(){
		return $this->session_error_message;
	}
}

/* End of file m_facebook_session.php */
/* Location: ./application/models/m_facebook_session.php */