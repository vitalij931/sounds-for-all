<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_lm_event extends CI_Model {
	private $current_event;

	public function __construct(){
		parent::__construct();
	}

	public function update($lat=NULL, $lng=NULL){
		if(_sna()) SE_R;

		$is_event_active = $this->is_event_active();

		if (!$is_event_active['success'] || !$is_event_active['success']) {
			return $is_event_active;
		}

		$active_event = $this->get_event();

		$data = [
			'user_id' => _s()->get_user_id(),
			'event_id' => $active_event->id,
			'lat' => $lat,
			'lng' => $lng,
			'time' => time()
		];

		$this->db->insert('event_tracking_location', $data);

		return ['success' => TRUE]
	}

	private function get_event(){
		if (!$this->current_event) {
			$this->current_event = $this->db->select('*')
				->where('user_id', _s()->get_user_id())
				->where('active', 1)
				->get('active_events', 1)
				->row();
		}

		return $this->current_event;
	}

	public function is_event_active(){
		if(_sna()) SE_R;

		$query_row = $this->get_event();

		return ['success' => TRUE,
				'active' => $query_row != NULL,
				'message' => ''];
	}
}

/* End of file M_lm_event.php */
/* Location: ./application/models/M_lm_event.php */