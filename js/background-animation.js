window.bacground_canvas = new (function BackgroundCanvas() {
	var canvas = window.bg_animation.canvas || document.getElementById('background');
	var context= canvas.getContext("2d");
	var dot_count = 100; // 150
	var dots = [];
	var lines = [];
	var tick = 0;
	var draw_debug = false;
	var is_running = true;
	var no_return_distance = 100;
	var background_time_length = 1000;

	var last_loop_time = Date.now();

	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;

	var createNewDot = function() {
		var velocity = 0.25 + 0.01 * Math.random();
		var angle = Math.random();
		var velocity_angle = 360*angle;

		return {
			x: (Math.random() * (canvas.width - 20)) + 10,
			y: (Math.random() * (canvas.height - 20)) + 10,
			dx: Math.cos(velocity_angle) * velocity,
			dy: Math.sin(velocity_angle) * velocity,
			opacity: Math.random(),
			velocity: velocity,
			velocity_angle: velocity_angle
		}
	}

	for (var i = 0; i < dot_count; i++) {
		dots.push(createNewDot())
	}

	var shadeColor = function(col, amt) {
		var usePound = false;

		if (col[0] == "#") {
			col = col.slice(1);
			usePound = true;
		}

		var num = parseInt(col,16);
		var r = (num >> 16) + amt;

		if (r > 255) r = 255;
		else if  (r < 0) r = 0;

		var b = ((num >> 8) & 0x00FF) + amt;

		if (b > 255) b = 255;
		else if  (b < 0) b = 0;

		var g = (num & 0x0000FF) + amt;

		if (g > 255) g = 255;
		else if (g < 0) g = 0;

		return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);

	}

	var draw = function() {
		context.clearRect(0, 0, canvas.width, canvas.height);

		for (var i = 0; i < lines.length; i++) {
			var line = lines[i];

			context.beginPath();

			context.lineWidth= 1 + 2 * line.opacity;
			context.moveTo(line.x1,line.y1);
			context.lineTo(line.x2, line.y2);
			context.globalAlpha = line.opacity;
			context.strokeStyle = '#eaeaea';
			context.stroke();
		}

		context.globalAlpha = 1;
		for (var i = 0; i < dots.length; i++) {
			var dot = dots[i];

			context.beginPath();
			context.arc(dot.x, dot.y, dot.opacity * 4 + 1, 0, 2 * Math.PI, false);
			context.fillStyle = shadeColor('#eaeaea', -(dot.opacity*100));
			context.fill();

			if (draw_debug) {
				context.beginPath();
				context.lineWidth=10;
				context.moveTo(dot.x,dot.y);
				context.lineTo(dot.x + 5*(dot.x - dot._x), dot.y + 5*(dot.y - dot._y));
				context.strokeStyle = 'red';
				context.stroke();
			}
		}

		// Look into dithered linear gradient (http://rectangleworld.com/blog/archives/713)
		var grd = context.createLinearGradient(0, canvas.width, canvas.height,0);
		grd.addColorStop(0, shadeColor("#eaeaea", Math.sin((Date.now() % 6000)/6000 * Math.PI) * -30 ));
		grd.addColorStop(1, shadeColor("#252a44", Math.sin((Date.now() % 5000)/5000 * Math.PI) * 30 ));

		context.globalAlpha = 0.3;
		context.fillStyle = grd;
		context.fillRect(0, 0, canvas.width, canvas.height);
	}

	var loop = function() {
		var now = Date.now();
		var deltaTime = now - last_loop_time;
		var dv = 1000/deltaTime;
		lines = [];

		for (var i = 0; i < dots.length; i++) {
			var el = dots[i];
			var dx = el.dx || Math.cos(el.velocity_angle * Math.PI / 180) * el.velocity * dv;
			var dy = el.dy || Math.sin(el.velocity_angle * Math.PI / 180) * el.velocity * dv;

			el.dx = dx;
			el.dy = dy;

			el._x = el.x
			el._y = el.y

			el.x += dx;
			el.y -= dy;

			// Collisions
			if (el.x < 0 || el.x > canvas.width){
				// console.log('Hit! 3',el.velocity_angle);
				el.dx = -el.dx
				// console.log('Now: ',el.velocity_angle);
			}

			if(el.y < 0 || el.y > canvas.height){
				// console.log('Hit! 4',el.velocity_angle);
				el.dy=-el.dy
				// console.log('Now: ',el.velocity_angle);
			}

			// Check if dot escaped
			if (el.x < -no_return_distance || el.x > canvas.width + no_return_distance
				||
				el.y < -no_return_distance || el.y > canvas.height + no_return_distance){
				el.x = Math.random() * canvas.width;
				el.y = Math.random() * canvas.height;
			}

			for (var j = 0; j < dots.length; j++) {
				var ranged_dot = dots[j];

				if (i == j) {
					continue;
				}

				var distanceX = Math.abs(ranged_dot.x - el.x);
				var distanceY = Math.abs(ranged_dot.y - el.y);
				var distance = Math.sqrt(Math.pow(distanceX,2) + Math.pow(distanceY,2));

				if (distance < 100) {
					var line = {
						x1: el.x, y1: el.y,
						x2: ranged_dot.x, y2: ranged_dot.y,
						opacity: 1-(distance/100)
					};

					if (!isLineCreated(line)) {
						lines.push(line)
					}
				}
			}
		}

		draw();
		last_loop_time = now;
		tick++;

		// if (tick < 50)
		if (is_running)
			requestAnimationFrame(loop);
	}

	requestAnimationFrame(loop);

	var isLineCreated = function(l2) {
		if (!l2){
			return false;
		}

		var reverse_l2 = {
			x1: l2.x2,
			x2: l2.x1,
			y1: l2.y2,
			y2: l2.y1,
		}

		for (var i = 0; i < lines.length; i++) {
			var l1 = lines[i];

			if (areLinesEqualPosition(l1,l2)) {
				return true
			}

			if (areLinesEqualPosition(l1, reverse_l2)) {
				return true
			}
		}

		return false;
	}

	var areLinesEqualPosition = function(l1,l2) {
		return Math.abs(l1.x1 - l2.x1) < 2 && Math.abs(l1.x2 - l2.x2) < 2 && Math.abs(l1.y1 - l2.y1) < 2 && Math.abs(l1.y2 - l2.y2) < 2;
	}

	function resizeCanvas() {
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;

		var new_dot_count = Math.floor((canvas.width * canvas.height) / 6000)

		if (new_dot_count >= dot_count) {
			var dc = new_dot_count - dot_count;

			for (var i = 0; i < dc; i++) {
				dots.push(createNewDot());
			}
		}else{
			dots.length = new_dot_count;
		}

		dot_count = new_dot_count;
	}

	this.getLines = function(){
		return lines;
	}

	this.getDotCount = function(){
		return dot_count;
	}

	this.getDots = function(){
		return dots;
	}

	window.addEventListener('resize', resizeCanvas, false);
	resizeCanvas();
})()

console.log(window.bacground_canvas);